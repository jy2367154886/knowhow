# Kirkja Writing Style Guide _(en)_

In an attempt to stay consistent early on...


## Technical / Instructional Writing Style

Since game designers will be in here...

The Markdown file will be our starting point. Markdown gives us some limitations to work within. Use the design restriction to _**generate a flow that is easy to:**_

- Understand.
- Skim.

### Markdown Patterns
###### TODO: How well does this perform on screen readers?

#### Instructional Notation

- _**Go to this location:**_
  - To perform some task, _**do this:**_ `menu`


To _**open a new file:**_
-  Either, _**click the menu:**_ `File`
  - Then _**click the selection:**_ `New File`
- Or, _**press the keys:**_


#### Coding

```gdscript
```



### Use the _inline_ `code` markdown to denote:


#### Keyboard shortcuts:
Blender:  `Viewport` > `Shift` + `A` to open the Add Object.
Godot:

- Nodes Object classes:
`Village`
`House`
`Resident`

- Variable & Function names:
``

- Open an app to a certain UI location.

- Godot: `SceneName` > `NodeName`
- Parameter Name: `value`
- Text Field: `"An awesome string!"`
- Position.x: `1.5`


## How best to be understood on different platforms.
### Discord
Once we have our text selected:
- Bold: `Ctrl` + `B`
- Italic: `Ctrl` + `I`
- Underline: `Ctrl` + `U`
- Strikethrough: `?`
- Quote: `?`
- Spoiler: `?`
- Codeblock: `?`

### GitLab.com / GitHub.com
- Bold:  `Ctrl` + `B`
- Italic: `Ctrl` + `I`
- Codeword: `Ctrl` + `E`
