# Bombs Away: 1. Project Setup

![The end is the beginning.](.docs/end.png)


## Objectives

- Review the **Bombs Away GDD**.
- Setup the work space to **begin the Bombs Away** project.
- Block out the game's main _**object classes**_ in Godot.
- **Add a falling behavior** to the **"Bomb.gd"** script.


## What are Game Design Documents? _(GDD)_

> _**"A good Game Design Document should be a 'living document' that is edited and maintained alongside the rest of the project's production. If a GDD becomes obsolete, it needs to be updated!"** - unblinky_
>
> **Game designers are tasked with editing and maintaining the GDD.** When version controlled, **Game design documents become instructions, plans, and developer log**.
>
> ### GDD Prompts:
> - What kind of game are we trying to make?
> - **What should the player feel** when they play the game?
> - What **input controls** does the game support?
> - What is the **target platform**, both hardware and software?
> - What are the **steps needed** to complete the game?
> - How should we **organize our work**?
>
>
> ### What's on the back of the box?
> The game's final description will usually be written last. It's too hard to predict the final form of the media we are generating. Game development shifts and changes course rapidly. A general description is a fine placeholder until the project becomes playable. Yet, another reason it's beneficial to become comfortable with rapid prototyping and iteration techniques.


## Bombs Away GDD

The Bombs Away GDD should include:
- A list of **all the pieces** needed to play.
- **The rules** for the game, and **how the game mechanics work**.
- A list of possible **endgame conditions**.
- The intended **player experience**.


### Pieces of Bombs Away:

- Main
- Bomb
- Explosion
- Missile
- Target
- Silo
- Building
- UI
  - Player Score
  - Hi Score
  - Game Menu


### Rules

- **Bombs drop at random** from the sky _(above the top of the screen)_ and **fall towards the player's base**, located on the ground at the bottom of the screen.
- The player tries to **protect the base's Building and Silos**.
- The game is over **when the player has lost all their Buildings and Silos**.
- The player may **launch Missiles from ground Silos** to destroy the falling Bombs before they reach the ground base.
- Anytime a Bomb is destroyed **the player's score is increased** in the Score UI.
- The **hi-score is stored as _persistent data_** and is **loaded upon opining** the game.
- The game menu should handle **pause, quit, and reset hi-score**.


![Bombs Away GDD.](.docs/bombsaway_gdd.png)

_(Notice how the GDD format is similar to the way board game instructions are laid-out.)_


## Clone the Project Repo

- **BombsAway Repo - https://classroom.github.com/a/USYK1oOf**


### GitHub Classroom

The link above should open our default web browser to a **GitHub Classroom** page with an invitation to accept the **Bombs Away** assignment.
- Click the `Accept this assignment` button.
- _we stare at the page waiting for something to happen... the white light; starting to produce a headache. neither tapping fingers, nor anxiously bouncing knee, seem to progress the wait. the mind wanders to thoughts of `dark mode`... hoping for something to happen... but it never will..._

...not until we read the instructions indicting that we need to: **refresh the page**.
- After the refresh, **click the link to your repository:**
  - _example link:_ `https://github.com/PushingPolygons/bombsaway-username`


### GitHub.com

- GitHub created a unique online repository with the _naming convention_: _**reponame-username**_.
- The link above should **deposit us on our GitHub.com** repo webpage; displaying our **newly constructed project repo**.
- If we look to the top of the page, we should see that **the repository is marked `Private`**
- **Only the two of us have access to the content of this repo**.
- From our repo page: **click the green** `Code` button, and select the `Open with GitHub Desktop` option.


### GitHub Desktop

- **GitHub Desktop** should launch; opening the dialog box: `Clone a repository` > **`URL`**
  - _**Repository URL or GitHub username and repository:** (auto-filled)_
  _example:_ `https://github.com/PushingPolygons/bombsaway-username.git`
  - Pick a favorite hard drive location for game dev projects. _**Local path:**_ `Choose...`
  _example:_ `~/games/bombsaway-username`
  - Click the `Clone` button.

![Clone the project repository.](.docs/clone_bombsaway.png)


- Once cloning is complete, we can **click the "show" button to navigate to our local project** via the OS file browser:
  - _**Windows button:**_ `Show in Explorer`
  - _**Mac button:**_ `Reveal in Finder`
  - _**Linux button:**_ `Show in your File Browser`


### File Browser

Our operating system should launch its file browser, opening to the repository project folder.
- Open the **"Code"** folder.
- **Double click the "project.godot"** file.


## Object Oriented Design (OOD)

> ## Object Oriented Design (OOD)
> There is a style of programming that works very well with games called **object oriented programming (OOP)**. The object oriented flow centralizes the concept of:
> - **Plans & Constructs.**
>
> Picture a vehicle designer who _**drafts up plans**_ for a new tricycle. Its form-factor is all curvy, and swooshy, and awesome! Those _**plans**_ then are sent to a manufacturer to _**construct**_ the trikes. Every trike the factory pumps out is a _**constructed instance**_ built from those original _**blueprints**_.
>
> ### Objects & Instances
> The games industry uses multiple terms for the **Plans & Constructs** model of organization. The most generic and common terms are:
> - Object & Instance.
>
> #### A **plan** may be called:
> - **object** (generic design)
> - **class** (any OOP capable language)
> - **scene** (Godot)(scene + script)
> - **blueprint** (Unreal)
> - **prefab** (Unity)
> - **symbol** (Adobe)
> - **lawform** (game design / board game design)
>
>
> #### A **construct** made from a plan may be called:
> - **instance** (C++, C#, python, Godot, Unreal, Unity, Adobe)
> - **Node** (Godot uses Scenes to generate Node instances.)
> - **Actor** (Unreal)
> - **gameObject** (Unity)


## Godot Game Scenes

The main file that Godot uses to **maintain its game data is the Scene**. By default Godot Scenes use the file extension ".tscn" _(text scene)_. **Scenes are constructed around sets of node hierarchies called _Trees_**. Each scene has a _node tree_ that may be viewed in the `Scene` dock. By dragging the position of nodes around in the node tree, we have the ability to create _parent_ and _child_ connections between nodes. We will begin with the following nodes:

- `Node`
- `Node3D`
- `MeshInstance3D`
- `Area3D`
- `CollisionShape3D`


Splitting each major piece of our game into distinct object classes will allow us to focus on one component at a time. Let's start with the big one...


## The `Bomb` Scene: "Bomb.tscn"

Each of our game objects will need to combine _**assets**_ and _**logic**_ into a single construct that the game engine will understand how to use. Each game object typically has a **folder**, **scene**, and a **script**. Let's make those first!

Look to the `FileSystem` dock, and **select the Resources folder:** `FileSystem` > `res://`

- **res://:** `RMB` > `New` > `Folder...` > **`Create Folder`**
  - _**Name:**_ `"Bomb"`
  - _**Click:**_ `OK`
- **res://Bomb/:** `RMB` > `New` > `Scene` > **`Create New Scene`**
  - _**Root Type:**_ `3D Scene`
  - _**Name:**_ `"Bomb.tscn"`
  - _**Click:**_ `OK`


### Graphics with: `Node3D` + `MeshInstance3D`

If we add a `MeshInstance3D` node to our scene tree....

> **_Pro tip:_ Dummy Node3D**
> Grab all the `MeshInstance3D` nodes that make up the graphics, then parent them to a `Node3D`. This keeps all of our graphical nodes together under one _**dummy node**_, out of the way of our collision shapes. Transforming the `Node3D` will adjust the position, rotation, and scale, independently of the `Area3D` at our _scene root_.

![The `Bomb` scene hierarchy.](.docs/bomb_graphics.png)


### Collisions with: `Area3D` + `CollisionShape3D`

In order for our `Bomb` to interact with other objects in our game, we need to set up _**collision shapes**_ within our scene's hierarchy. Sometimes called a _**hit box**_ or _**collider**_, the `CollisionShape3D` node will **emit a signal** that we can _**listen**_ for whenever it overlaps with another object with a collider.


#### Usage

`CollisionShape3D` nodes cannot provide the collision logic by themselves. `CollisionShape3D` requires that it be parented to Godot nodes that contain the words: "Area" or "Body"; we will be using `Area3D`
- `RMB` click on the `Bomb` root node, then select: `Change Type`

![Change the type of the `Bomb` root node.](.docs/change_type.png)

- From the `Change Type of "Bomb"` dialog box: `Node` > `Node3D` > `CollisionObject3D` > `Area3D` + `LMB` double-click.

![Select `Area3D` from the node list.](.docs/change_type_to_area3d.png)


### Attach Script: "Bomb.gd"

Our `Bomb` **won't do anything** _(beyond the default node behaviors)_ without attaching a script. The primary way Godot organizes game logic is through **text files, written in GDScript**. If we want our `Bomb` class to drop and fall downwards, we'll want to program that behavior within the body of the script.

- From the `Scene` hierarchy:
  - _**Select the root node:**_ `Bomb`
  - _**Click the button:**_ `Attach Script`

![`Attach Script Button`](.docs/attach_script_button.png)

- Within the  `Attach Node Script` dialog box:
  - Most of the fields should auto-fill, _**but double check:**_
    - _**Inherits:**_ `Area3D`
    - _**Path:**_ "res://Bomb/Bomb.gd"
  - _**Click the button:**_ `Create`

![By default the `Attach Node Script` will `Create`, or `Load`, a ".gd" script with the same name as the selected node.](.docs/attach_node_script.png)

_(notice over in the `FileSystem` dock we now have a new text file named "Bomb.gd" in the `Bomb` folder. if we loose track of the script in the editor, we can always find it in the FileSystem.)_


We'll just **double-check that the extended _super class_ is of type:** `Area3D`, and **declare our _class name_ as: Bomb**.


###### "Bomb.gd"

```gdscript
extends Area3D
class_name Bomb

var speed: float = 10 # m / sec.
var direction: Vector3 = Vector3.DOWN # Normalized direction.

func _process(delta):
	position += speed * direction * delta
```

![The "Bomb.tscn" scene.](.docs/bomb_scene.png)


## Add a `Bomb` instance to the `Main` scene.

- To open the `Main` Scene from the `FileSystem` dock, _**double-click:**_ `res://` > `Main` > `Main.tscn`
  - To **add a new** `Bomb` **instance** to the `Main` scene:
    - _**Drag the scene:**_ `res://` > `Bomb` > `Bomb.tscn` over to the `Scene` hierarchy dock.
    - _**Drop the scene on top of the root node:**_ `Main`
    - Our `Bomb` should now be visible in the `3D` viewport. _(located at the `Main` node's origin)_
  - If we were to drag and drop another `Bomb.tscn` onto the the `Main` node, the names of each node will automatically increment: "Bomb2", "Bomb3", etc.

![`Main` scene with one `Bomb` instance.](.docs/main_scene.png)


### Play the Project: `F5`

- `▶` **Play** the game and make sure it works!
- We should see a `Bomb` **falling out of view in our (Debug) game window**.

![Playing Bomb!](.docs/dark_bomb.png)


## Why is it so dark?

### Sun + Environment

In our `Main` scene we may quickly change the _lighting_, _shadows_, _sky color_, and more from the `Edit Sun and Environment Settings` drop-down tool.
- Within the `3D` viewport toolbar:
  - _**Click the 3 vertical dots:**_  `Edit Sun and Environment Settings`
  - _**Set the Azimuth:**_ 106
  - _**Click the button:**_ `Add Sun to Scene`
  - _**Click the button:**_ `Add Environment to Scene`

![Edit the lighting in the properties.](.docs/edit_sun_and_environment.png)

We now have **two new nodes** in our `Scene` hierarchy:
- The `WorldEnvironment` manipulates the sky and ground colors, the glow and bloom of our scene, and a ton of other parameters.
- Use the `DirectionalLight3D` node to manipulate the sun's direction, color, intensity. etc.

![](.docs/world_environment.png)



### Tap `F5`

- `▶` **Play** the game and make sure it works!
- We should see a `Bomb` **falling out of view in our (Debug) game window**.


![Playing Bomb!](.docs/end.png)


## Commit & Push!

When satisfied with the results:
- Use **GitHub Desktop** to _**commit**_ and _**push**_ the project changes.
- **If any issues arise, use Discord** to send a message and we'll sort out any hurdles together!


### Continued in Part 2

[Bombs Away: 2. Game Scale Lineup](https://gitlab.com/kirkja-leikjahonnunar/knowhow/-/blob/main/Tinker/Drills/Bombs%20Away/2.%20Game%20Scale%20Lineup/README.md)
