# Bombs Away: 6. UI Score and Menus


Now that our game systems are in place, we can add the final touches with UI scoring and menus.

Set up a scenario where we can test the "Game Over" systems.
Create a UI singleton scene that controls the in-game score and menus.
Create a Game Over Menu with 2 buttons:
[Play Again]
[Quit]
Dooms Day

Position a Bomb over each Building and Silo in the Main 3D scene so we can trigger the Game Over systems quickly.

Autoload the UI Global Variable

Create a new scene for the UI and store it in the Main folder.

Use the menu [Project] &gt; [Project Settings...] &gt; [Audoload]
Click the [Folder Icon] and select the "Main/UI.tscn" scene.
Click the [Add] button.
To allow us to call the UI scene from any script, however by default Godot names the global variable "Ui" (mixed caps); we need to rename the [Name] field to "UI" (all caps).

UI Layout

Don't forget to select Ignore form the UI &gt; Mouse &gt; Filter: Ignore (or the mouse clicks will be intercepted by the UI). We can add a Label node for the score.

UI.gd

extends Control

var score: int = 0

func _ready():
&nbsp; &nbsp; $Label.text = str(score)
&nbsp; &nbsp; $Menu.hide()
&nbsp; &nbsp;&nbsp;
func _unhandled_key_input(event):
&nbsp; &nbsp; if event.scancode == KEY_ESCAPE:
&nbsp; &nbsp; &nbsp; &nbsp; PauseMenu()
&nbsp; &nbsp; &nbsp; &nbsp;&nbsp;
func ResetScore():
&nbsp; &nbsp; score = 0
&nbsp; &nbsp; $Label.text = str(score)

func UpdateScore(delta_score: int):
&nbsp; &nbsp; score += delta_score
&nbsp; &nbsp; $Label.text = str(score)

func PauseMenu():
&nbsp; &nbsp; $Menu.show()

func _on_PlayAgain_pressed():
&nbsp; &nbsp; $Menu.hide()
&nbsp; &nbsp; ResetScore()
&nbsp; &nbsp; get_tree().reload_current_scene()

func _on_Quit_pressed():
&nbsp; &nbsp; get_tree().quit()

Test the results by clicking [F5].

Commit &amp; Submit!
Use GitHub Desktop to commit and push&nbsp;your project's changes.
Mark this D2L Assignment complete.
If you have any issues, please use Discord to ask questions and we'll solve it over there!
Sky Bombs: Quest Chain Complete!
Submit your GitHub repo name (example: "skybombs-unblinky") in the D2L text submission field to let me know when the project is ready for review.
