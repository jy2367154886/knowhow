# Bombs Away: 2. Game Scale Lineup
Before detailing-out *one specific class* too far, it's good practice to _**block out all the game pieces into scenes of their own**_. Once we have them blocked out we can _**compare their scale to one another**_ in the `Main` scene, then we'll be able to _**make adjustments until we achieve a believable scale**_ for the game. _(believable v. realistic)_

![The end is the beginning.](.docs/end.png)


## Objectives
- Create new scenes with for **each our remaining _object classes:_**
  - `Explosion`
  - `Missile`
  - `Silo`
  - `Building`
  - `Target`
- After populating scenes with _**graphics**_, _**colliders**_, and _**scripts**_, add each of the scenes to the `3D` view of the `Main` scene, so we can get a **more accurate view of the how our game pieces look next to each other**.


## The `Explosion` Scene: "Explosion.tscn"
![Explosion scene.](.docs/explosion_scene.png)


### Graphics: `Node3D` + `MeshInstance3D`
- Use the `Node3D` and `MeshInstance3D` nodes to build out the 3D visuals. Our `Explosion` will be a simple sphere shape: `Inspector` > `MeshInstance3D` > `Mesh` > `New SphereMesh`
- _**Add and edit the nodes**_ in the `Scene` hierarchy to create the visuals for the `Missile` class.
  - Add a `Node3D`, then rename it "Graphics"


### Colliders: `Area3D` + `CollisionShape3D`

- _**Change root node type to:**_ `Area3D`
  - _**`RMB` click on the root node:**_ `Explosion`
  - _**Select:**_ `Change Type`
  - Within the `Change Type of "Explosion"` dialog box, _**double-click:**_ `Node` > `Node3D` > `CollisionObject3D` > `Area3D`

---

- _**Add a `CollisionShape3D` to the root node:**_ `Explosion`
  - There will be errors on the `CollisionShape3D` node, until we _**select a shape from:**_ `Inspector` > `CollisionShape3D` > `Shape` > `New ShereShape3D`
  - _**Adjust the little red and white handles**_ in the `3D` viewport to fit around the mesh.


### Brains: "Explosion.gd"

We'll just _**add the most basic of scripts**_ to _register_ our `Explosion` class with the _Editor_. The GDScript below registers the word Explosion as a class Hello my name is: `Explosion`

- _**From the**_ `Scene` hierarchy:
  - _**Select the root node:**_ `Explosion`
  - _**Click the button:**_ `Attach Script`


###### "Explosion.gd"

```gdscript
extends Area3D
class_name Explosion
```

> ## Automation of Flow
> _"One and done, only works if there is a system in place first." - unblinky, currently inhabiting the plane beyond the academically abstract._
>
> ### What's in a system?
> Once we have built 2 objects we are well on our way to designing a _"system of object creation"_. If we proceed to build a 3rd object, we can begin to think about how to automate any repetitive behavior.
>
> A general rule-of-thumb for developers would be to reserve the word "system" for a **process that has been executed at least 2 times**.
>
> ### Workflows and Standards of Organization
>
> So far, we have built two game objects, the `Bomb` and the `Explosion`. Before continuing with our 3rd _game object_, we should stop and consider how our previous steps have been working for us.
> - Is there anything we can do to help us **cruse through the remaining game pieces?**
> - Our **work pattern systems** _(work**flow**)_ will solidify as we progress. How do we intend to **_retain flexibility_ for new ideas that could improve the _system of flow_?**
>
> Let's see if we can find any!


## The `Missile` Scene: "Missile.tscn"
![The "Missile.tscn" scene.](.docs/missile_scene.png)

### Graphics...
- Add a `Node3D`, then rename it "Graphics".
- Parent any `MeshInstance3D` nodes to: `Graphics`

### Colliders...
- **None.** The `Missile` _root node_ type may remain a `Node3D`, since we don't care if it hits anything other than its `Target` and `Explode()`.

### Brains...
###### "Missile.gd"
```gdscript
extends Area3D
class_name Missile
```


## The `Silo` Scene: "Silo.tscn"
![The Silo scene.](.docs/silo_scene.png)

### Graphics...
- Add a `Node3D`, then rename it "Graphics".
- Parent any `MeshInstance3D` nodes to: `Graphics`

### Colliders...
- Change _root node_ type to: `Area3D`
- Add and `CollisionShape3D` nodes to the _root node_: `Silo`

### Brains...
###### "Silo.gd"
```gdscript
extends Area3D
class_name Silo
```


## The `Building` Scene: "Building.tscn"
![The Building scene.](.docs/building_scene.png)

### Graphics...
- Add a `Node3D`, then rename it "Graphics".
- Parent any `MeshInstance3D` nodes to: `Graphics`

### Colliders...
- Change _root node_ type to: `Area3D`
- Add and `CollisionShape3D` nodes to the _root node_: `Building`

### Brains...
###### "Building.gd"
```gdscript
extends Area3D
class_name Building
```


## The `Target` Scene: "Target.tscn"
![The "Target.tscn" scene.](.docs/target_scene.png)

### Graphics...
- Add a `Node3D`, then rename it "Graphics".
- Parent any `MeshInstance3D` nodes to: `Graphics`

### Colliders...
- **None.**

### Brains...
###### "Target.gd"
```gdscript
extends Area3D
class_name Target
```


## Scale Adjustments and Details
Now that we have a starting point with our scenes, we can _**keep adjusting the 3D object transforms (position, rotation, scale) and material of the mesh graphics**_, until we are satisfied with the overall **game scale**.

 **Colliders should not be scaled!** the `CollisionShape3D` nodes may act unpredictably if Transform Scaled. Instead, _**use the red & white resize dots**_ in the 3D viewport, or _**set the properties of:**_ `Inspector` > `CollisionShape3D` > `Shape` > `Shape3D`
![](.docs/adjust_to_taste.png)

### Saving
We can always **save our progress with the keys:**
- _**Save scene:**_ `Ctrl` + `S`
- _**Save all scenes:**_ `Ctrl` + `Shift` + `Alt` + `S`


![All of our game pieces to scale in the `Main` scene.](.docs/end.png)


## Commit & Push!
When satisfied with the results:
- Use **GitHub Desktop** to _**commit**_ and _**push**_ the project changes.
- **If any issues arise, use Discord** to send a message and we'll sort out any hurdles together!
