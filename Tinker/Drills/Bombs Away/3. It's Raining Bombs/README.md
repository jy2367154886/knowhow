# Bombs Away: 3. It's Raining Bombs
![The end is the beginning.](.docs/end.png)


## Objectives
- **Use a `Timer` node to _spawn multiple random instances_** of the `Bomb` class into the `3D` viewport.
- Spawn a new `Bomb` instance **every 0.3 seconds**.
- Spawn random `Bomb` instances **along the top of the camera frame**.
- Despawn `Bomb` instances **once they reach the floor of the playing field**. _(position.y < 0)_


## The Opening Dev Ritual

The following process keeps our dev team _**informed**_ and _**content synced**_. As long as we _**perform the "opening dev ritual" every day**_ of development, we will **never lose more than one day's worth of progress!**


### GitHub Desktop

Open **GitHub Desktop** and check the **3 main tool buttons** across the top:

1. Is the `Current repository` button **set to our "bombsaway-username" project?**
1. Is the `Current branch` button **set to "main"?**
1. Click the `Fetch origin` button for good luck!
1. _**Click the:**_ `"show"` **button** to navigate to our _local project repo_ via the OS file browser:
   - _**Windows button:**_ `Show in Explorer`
   - _**Mac button:**_ `Show / Reveal in Finder`
   - _**Linux button:**_ `Show in your File Browser`

![Open the project repository with GitHub Desktop.](.docs/dev_ritual_opener.png)


### File Browser

Our operating system should launch its default _file browser_, opening to the project repository folder.

  - _**Open the folder:**_ "Code"
  - _**Double-click:**_ "project.godot"

![Double-click: "project.godot"](.docs/project_godot.png)


## The `Main` Scene: "Main.tscn"

We now have **enough pieces to begin adding _gameplay_ to our** `Main` scene. To open the `Main` scene in the `2D` viewport:

- From the `FileSystem` dock, _**double-click:**_ `res://` > `Main` > `Main.tscn`
  - _**Click the viewport button:**_ `2D`
  - Notice that the `Scene` hierarchy updates, **showing us a list of items within the** `2D` viewport.



### Adding a Countdown: `Timer`

Godot provides a `Timer` node that allow us to trigger our functions with **specific timing**. Timers are perfect for **repetitive actions, like spawning bombs!**

- From the `Scene` hierarchy:
  - _**Select the root node:**_ `Main`
  - _**Open the context menu:**_ `Main` + `RMB`
  - _**Choose the selection:**_ `+ Add Child Node`
- From the `Create New Node` dialog box:
  - _**Locate and select:**_ `Timer`
  - _**Click the button:**_ `Create`

![Add child `Timer` to `Main` root node.](.docs/create_timer_node.png)



### Timer Parameters

Now that we have a `Timer` in the scene, let's set it up to _**emit a signal**_ every `0.3` seconds. We will also need to enable its **Autostart** parameter so `Timer` will begin counting down as soon as the `Main` scene is run: `▶️`

- From the `Scene` hierarchy, _**select:**_ `Main` > `Timer`
- In the `Inspector` panel, _**set its properties:**_
  - _**Wait Time:**_ `0.3` seconds.
  - _**Autostart:**_ `true`

![Set the Timer's Wait time to 0.3. Set the Autostart to true.](.docs/timer_parameters.png)


### Main Brain: "Main.gd"

Let's _**add a new script**_ to the `Main` scene to handle our _main gameplay_ logic. This will be the core _game manager_ for all of our _game objects_. To create and attach a new script to the `Main` scene's _root node_:


- Look to the `Scene` hierarchy:
  - _**Select the root node:**_ `Main`
  - _**Open the context menu:**_ `Main` + `RMB`
  - _**Click the selection:**_ `Attach Script`

![](.docs/attach_script.png)


- From the `Attach Node Script` dialog box:
  - Leave the defaults for _**Inherits:**_ `Node`
  - Leave the auto-filled defaults for, _**Path:**_ `res://` `Main/` `Main.gd`
  - _**Click:**_ `Create`

![`Main` node > `Attach Node Script` > Path: "res://Main/Main.gd".](.docs/attach_node_script.png)


### How does one spawn `Bomb` instances with `Timer` ?

#### The Signal: `timeout`
The `Timer` starts at a _specific time,_ then begins to count down. Every time its countdown reaches `0`, **the node emits a signal.** We can _listen_ for this signal in our "Main.gd" script.

Our `Timer` will _**emit a signal**_ called `timeout` once it has **finished its countdown** from the "Wait Time" parameter. In order for us to _**use the**_ `timeout` _signal_, we need to _**add 2 things to our "Main.gd" script first:**_

- _**Declare a function**_ that we want to run when the `Timer` _times out:_ `func OnTimedOut():`
- _**Connect the signal**_ to that new function:  `timer.timeout.connect(OnTimedOut)`


###### _"Main.gd"_

```gdscript
extends Node
class_name Main

const BOMB: PackedScene = preload("res://Bomb/Bomb.tscn")
@onready var timer = $Timer

func _ready():
	timer.timeout.connect(OnTimedOut)

func OnTimedOut():
	var bomb = BOMB.instantiate()
	bomb.position.x = randf_range(-6, 6)
	bomb.position.y = 10
	add_child(bomb)
```


### Play the Project: `F5`

Click the `▶` **play** button and make sure it works! What are our results? It's good practice to **test the results of our project as often as possible**. It will help us detect any bugs we accidentally introduce. If we clean them up as we go, we will have a more stable code base.

- To **run the project** either:
  - _**Click the Play button:**_ `▶`
  -or _**tap the key:**_ `F5`


### Editing "Bomb.gd"
The only thing we have to fix in "Bomb.gd", is to _**despawn bombs once they reach the ground.**_ The best place to check a condition every frame is the `Bomb` `_process()` function.


###### _"Bomb.gd"_

```gdscript
func _process(delta):
    position += speed * direciton * delta
    if position.y < 0:
        queue_free()
```

### Tap `F5`
- `▶` **Play** the game and make sure it works!
- We should see **multiple bombs falling** toward the ground, then despawning when they move below `0.0` on the `Y` axis.

![It's raining bombs!](.docs/end.png)


## Commit & Push!

Use GitHub Desktop to _commit_ and _push_ your project's changes.
If you have any issues, please use Discord to ask questions and we'll solve it over there!
