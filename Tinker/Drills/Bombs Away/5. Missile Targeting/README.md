
###### _"Main.gd"_

```gdscript
extends Node

# Scenes to spawn.
const BOMB: PackedScene = preload("res://Bomb/Bomb.tscn")
const MISSILE: PackedScene = preload("res://Missile/Missile.tscn")
const TARGET: PackedScene = preload("res://Target/Target.tscn")

@onready var timer = $Timer
@onready var cursor_wall = $CursorWall
@onready var silo = $Silo


func _ready():
    timer.timeout.connect(SpawnBomb) # Events.
    cursor_wall.input_event.connect(OnWallClicked)


func SpawnBomb():
    var bomb = BOMB.instantiate()
    bomb.position.x = randf_range(-25, 25)
    bomb.position.y = 20
    add_child(bomb)


func SpawnMissile(target):
    var missile = MISSILE.instantiate()
    missile.target = target
    missile.position = silo.position
    add_child(missile)


func OnWallClicked(_camera, event, position, _normal, _shape_idx):
    if event is InputEventMouseButton:
        if event.button_index == MOUSE_BUTTON_LEFT:
            if event.pressed:
                var target = TARGET.instantiate()
                target.position = position
                add_child(target)

                SpawnMissile(target)
```


###### _"Missile.gd"_

```gdscript
extends Node3D
class_name Missile

const EXPLOSION = preload("res://Explosion/Explosion.tscn")

var target: Target
var speed: float = 4.0 # m/s
var direction: Vector3 = Vector3(1, 0, 0)


func _ready():
    direction = position.direction_to(target.position)


func _process(delta):
    position += direction * speed * delta
    var distance = position.distance_to(target.position)
    if distance < 1.5:
        Explode()


func Explode():
    var explosion = EXPLOSION.instantiate()
    explosion.position = target.position
    get_parent().add_child(explosion)
    target.queue_free()
    queue_free()
```


###### _"Bomb.gd"_

```gdscript
extends Area3D
class_name Bomb

var speed: float = 5 # m / sec.
var direciton: Vector3 = Vector3.DOWN # Normalized direction.

func _ready():
    area_entered.connect(OnEntered)

func _process(delta):
    position += speed * direciton * delta
    if position.y < 0:
        queue_free()


func OnEntered(other_area):
    if other_area is Explosion:
        queue_free()
```


```gdscript

```

---
---
# 3.4 to 4.0; Broken below!
---
---

# Bombs Away: 5. Missile Targeting

Now that we have built out a level with `Building` and `Silo` instances, and we can have theme the player needs to be able to click "through the screen" onto a 3D space collision surface in order to defend them.

![The end is the beginning.](.docs/end.png)


## Objectives
Once the player clicks on the playing field:

- Spawn a `Target` instance whenever we click on the "MouseHitBox".
- A `Silo` should spawn a Missile.
- Once instantiated, the Missile should fly directly towards the Crosshair.
- Upon reaching the Crosshair, the Crosshair should be freed from memory, and the Missile should explode.
Crosshair


## The `Main` Scene
Before we can really do anything with our new level, we need to let the Main game script know about the items within. The following code to prepare the scene's logic. We now have a Crosshair const that gets preloaded, and Arrays to store the Buildings and Silos. We will add each of our scene objects to the appropriate Array with the $PathName structure.


###### "Main.gd"
```gdscript
extends Node



const BOMB: PackedScene = preload("res://Bomb/Bomb.tscn")
const TARGET: PackedScene = preload("res://Crosshair/Crosshair.tscn")

var buildings: Array
var silos: Array
var current_silo: Silo
var crosshair: Node3D

func _ready():
  # Silos.
  $HomeBase/SiloCenter.main = self
  $HomeBase/SiloLeft.main = self
  $HomeBase/SiloRight.main = self
  silos.append($HomeBase/SiloCenter)
  silos.append($HomeBase/SiloLeft)
  silos.append($HomeBase/SiloRight)

  # Buildings.
  $HomeBase/Building_0.main = self
  $HomeBase/Building_1.main = self
  $HomeBase/Building_2.main = self
  $HomeBase/Building_3.main = self
  $HomeBase/Building_4.main = self
  $HomeBase/Building_5.main = self
  buildings.append($HomeBase/Building_0)
  buildings.append($HomeBase/Building_1)
  buildings.append($HomeBase/Building_2)
  buildings.append($HomeBase/Building_3)
  buildings.append($HomeBase/Building_4)
  buildings.append($HomeBase/Building_5)

```


We are going to group all the actions that all Buildings can do into one _base class_.

###### "Building.gd"
```gdscript
extends Area3D
class_name Building

var main: Node # Reference to the Main scene.

func Destroy():
  main.RemoveBuilding(self)
  queue_free()
```


###### "Silo.gd"

Now we can use the Building class as the base of Silo. The Silo's main purpose is to spawn a Missile, if the Cooldown is over.

```gdscript
extends Building # A Silo is a Building.
class_name Silo

const MISSILE: PackedScene = preload("res://Missile/Missile.tscn")

var is_ready_to_fire: bool

func _ready():
  is_ready_to_fire = false
  $Cooldown.scale.y = 0

func _process(delta):
  $Cooldown.scale.y += 4.0 * delta
  if $Cooldown.scale.y &gt; 4.0:
    $Cooldown.scale.y = 4.0
    is_ready_to_fire = true

func FireMissile(crosshair):
  if is_ready_to_fire:
    var missile = MISSILE_PS.instance()
    missile.translation = translation
    missile.crosshair = crosshair
    get_parent().get_parent().add_child(missile) # Should add to Main.
    is_ready_to_fire = false
    $Cooldown.scale.y = 0
  else:
    target.queue_free()
```


###### "Main.gd"

Let's add a few more functions to the Main script. These functions manage our Buildings and Silos as they cooldown, or get blown up.

```gdscript
func GameOver():
  print("GAME OVER")

func RemoveBuilding(building: Building):
  print("Destroyed: ", building.name)

  if building is Silo:
    silos.erase(building)
  else:
    buildings.erase(building)

    if buildings.size() <= 0:
      GameOver()


func FindSilo():
  for silo in silos:
    if silo.is_ready_to_fire:
      print(silo.name)
      return silo

  print("No ammo!")
  return null
```


### Main 3D Scene

Back within the "Main.tscn" we can finally hook up the mouse click event and use some of the assets we've been building.

We want the Crosshair to spawn when, and where, the player clicks on Main scene's HitPlane Area node. For this we'll need to detect the mouse click and get the 3D position of where the mouse cursor intersects with our HitPlane. We'll use the Area node's input_event() Signal. When we double click the Signal input_event(), a dialog box opens asking us to pick a script to connect a new Signal Function. In this case _on_HitPlan_Input_event on the "Main.gd" script.

  - _**Click:**_ `Connect`

Main.gd
func OnMouseClick(camera, event, position, normal, shape_idx):
  if event is InputEventMouseButton:
    if event.button_index == BUTTON_LEFT:
      if event.pressed:
        current_silo = FindSilo()
        if current_silo:
          target = TARGET.instantiate()
          target.position = position
          add_child(target)
          current_silo.FireMissile(crosshair)

## Tap `F5`
Test the results by clicking the Play button (or [F5]) at the top right of Godot. The Bombs should drop and a Missile should appear but it just sits there...



###### "Missile.gd"

Let's make a smart Missile that bolts towards the spawned Crosshair. When the Missile reaches it's target, it explodes and we delete the Crosshair. We use a `lerp()` function to determine the Missile's distance along a linear path.


```gdscript
extends Explosive
class_name Missile

var target: Nod3D
var from_point: Vector2
var to_point: Vector2
var alpha: float
var speed: float = 40.0 # m/s
var interval: float

func _ready():
  from_pos = Vector2(translation.x, translation.y)
  to_pos = Vector2(crosshair.translation.x, crosshair.translation.y)

  var angle = from_pos.angle_to_point(to_pos)
  rotate_z(angle)

  # 2D Vector2 function.
  var distance = from_pos.distance_to(to_pos)
  interval = speed / distance

func _process(delta):
  if alpha < 1.0:
    alpha += interval * delta
    position = Vector3(lerp(from_pos.x, to_pos.x, alpha), lerp(from_pos.y, to_pos.y, alpha), 0) #lerp(from_pos, to_pos, alpha)
  else:
    Explode(Color.AQUA)
    crosshair.queue_free()
```

Test the results by clicking [F5]. Now the Missiles should target the Crosshair and explode.

## Commit & Push!
- Use GitHub Desktop to commit and push&nbsp;your project's changes.
- If you have any issues, please use Discord to ask questions and we'll solve it over there!
