# Bombs Away: 4 Level Design


Now that we are successfully bombing the sky let's build out the rest of the Main scene. Use the Godot navigation and transformation tools to block-out our first prototype level. A prototype level is usually one where all the game systems can be tested and verified.

In Godot, populate the Main scene with:

Bombs
Silos
Buildings
Background Hitplane
Level Design

For this first level we will primarily be using the X axis (left and right) and Y axis (up and down) to arrange our level. We will be keeping our game objects near 0 on the Z axis.

We can start by dragging the Building and Silo scenes from the FileSystem tab into the Main scene's viewport or Scene tab hierarchy. Every time we drop a scene from FileSystem into the Main scene, we are creating an instance of that scene. It's the same as spawning the scenes from code.

It's common to use empty nodes to categorize scene instances. Let's make a new node to hold all our buildings called "HomeBase". We'll also need to add an Area node called "HitPlane" for the player to click later on. The HitPlane should have 2 children nodes: a MeshInstance for looks, and a CollisionShape &gt; BoxShape that covers the camera's view.

Commit &amp; Submit!
Use GitHub Desktop to commit and push&nbsp;your project's changes.
If you have any issues, please use Discord to ask questions and we'll solve it over there!
