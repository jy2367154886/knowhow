# Space Rocks: 3. Ship Controls

![The end is the beginning.](.docs/end.png)


## Objectives

- Iterate the `Ship` scene:
  - Graphics:
    - Use a paint program to _**draw an image of a spaceship**_.
    - _**Export the images**_ for use in a `Sprite2D` node.
  - Code:
    - _**Create a player control scheme**_  to control our ship.
    - _**Add the logic for the ship to rotate and thrust forward**_ to the "Ship.gd" script.


## Perform the Opening Dev Ritual

Once again, we'll make sure we _**stay informed and content synced,**_ so we never lose more than one day's worth of progress!


###  ![](../../.docs/github.png) GitHub Desktop

Open **GitHub Desktop** and check the **3 main tool buttons** across the top:

1. Is the `Current repository` button **set to our "spacerocks-username" project?**
1. Is the `Current branch` button **set to "main"?**
1. Click the `Fetch origin` button for good luck!
1. _**Click the:**_ `"show"` **button** to navigate to our _local project repo_ via the OS file manager:
   - _**Windows button:**_ `Show in Explorer`
   - _**Mac button:**_ `Show / Reveal in Finder`
   - _**Linux button:**_ `Show in your File Manager`

![Open the project repository with GitHub Desktop.](.docs/dev_ritual_opener.png)


###  ![](../../.docs/folder.svg) File Manager

Our operating system should launch its default _file manager_, opening to the project repository folder.



## ![Krita logo.](../../.docs/krita.svg) Krita - "Ship.kra"

Let's make a pixel art asset for our `Ship` class. Krita has a mirror tool that is helpful when designing symmetrical objects.

- _**Launch Krita:**_ _(or your favorite pixel paint program)_
  - _**Create a new pixel canvas:**_
    - _**Width:**_ `32`
    - _**Height:**_ `32`
  - _**Rename a transparent pixel layer:**_ "Ship"
  - _**Draw a spaceship in a transparent pixel layer.**_

![The Ship layer.](.docs/ship_layer.png)

- _**Save the working file:**_ `File` > `Save As...`
- From the `Saving As` dialog box:
  - _**Folder Path / Look in:**_ "spacerocks-username/Art/"
  - _**File name:**_ "Ship.kra"
  - _**Files of type:**_ `Krita document`
  - _**Click the button:**_ `Save`

![Ship save.](.docs/ship_save.png)


Godot can't read Krita files directly so **we'll need to export our layer as a ".png" file.**

- _**Export the ship layer:**_ `File` > `Export...`
- From the `Exporting` dialog box:
  - _**Folder Path / Look in:**_ "spacerocks-username/Code/Ship/assets/"
  - _**File name:**_ "ship.png"
  - _**Files of type:**_ `PNG Image`
  - _**Click the button:**_ `Save`

![Ship export.](.docs/ship_export.png)


| "Ship/assets/" | Sprite Texture |
| :-: | :--: |
| "ship.png" | ![ship.png](.docs/ship.png) |

Let's try out our new graphics within Godot!


## ![](../../.docs/folder.svg) File Manager

- Launch Godot:
  - _**Open the folder:**_ "Code"
  - _**Double-click:**_ "project.godot"


## ![](../../.docs/godot.svg) Godot - The `Ship` Scene: "Ship.tscn"

- Open the `Ship` scene from the `FileSystem` dock:
  - _**Double-click:**_ `res://` > `Ship` > `Ship.tscn`


- From the `Scene` tree:
  - _**Select the root node:**_ `Ship`
  - _**Click the button:**_ `Add a new child node`


- From the `Create New Node` dialog box:
  - _**Search:**_ "Sprite2D"
  - _**Click the button:**_ `Create`

![New `Sprite2D` node.](.docs/new_sprite2d.png)


- From the `FileSystem` dock:
  - _**Drag:**_ `res://` > `Ship` > `assets` > `"ship.png"`
  - _**Drop on:**_ `Inspector` > `Sprite2D` > Texture: `<empty>` field.


- In the `2D` viewport:
  - _**Adjust the size of the**_ `Sprite2D` _**node graphics**_ to fit within the bounds of our `CollisionShape2D` nodes.

![Scale the `Sprite2D` to fit the existing `CollisionShape2D` nodes.](.docs/sprite_scale.png)


## ![](../../.docs/godot.svg) Player Control / Input Mapping

Godot has a built-in user interface that we can use to **set the _input maps_ for our game**. _Input maps_ are the way we bind the _player's input_ to our _in-game actions_. Godot supports _mapping_ actions to the player's: keyboard, mouse, game-pad, VR headset, etc.


### Declaring Actions

- _**Select the menu option:**_ `Project` > `Project Settings...`
- Within the `Project Settings` dialog box:
  - _**Click the tab:**_ `Input Map`
  - _**Type new action names into the text field:**_ `Add New Action`
  -  _**Then press:**_ `Enter`
    - _**Action:**_ "rotate_cw"
    - _**Action:**_ "rotate_ccw"
    - _**Action:**_ "thrust"
    - _**Action:**_ "fire"

![Declaring our action inputs.](.docs/action_declaring.png)


### Binding Actions

Once added to the action list, we can **bind the actions to different player inputs:**

- _**Click plus button, at the end of each action row:**_ `+`

![Full actions list.](.docs/action_list.png)


- From the `Event Configuration` dialog box:
  - _**The first text field should read:**_ "Listening of input..."
  - _**Press:**_ `A` (or any key, mouse button, gamepad input)
  - _**Click:**_ `Ok`

![Event Configuration dialob box.](.docs/event_configuration.png)


Feel free to assign the buttons that you like! In this web drill we bound:

| Input Action | Key Binding |
| :-- | :-: |
| "rotate_cw" | `D` |
| "rotate_ccw" | `A` |
| "thrust" | `RMB` |
| "fire" | `LMB` |

![Full binding list.](.docs/action_bindings.png)


### Scripting Movement: "Ship.gd"

Now that we have our actions registered as _input maps_, we need to edit the script of our `Ship` class to perform behaviors when the player presses buttons.

 _**Let's add the ability to rotate and thrust forward!**_


###### _"Ship.gd"_

```gdscript
extends ScreenWrapper
class_name Ship

var rotation_rate: float = 180.0 # degrees / sec.
var velocity: Vector2 = Vector2.ZERO
var thrust_power: float = 10.0


func _process(delta):
	# Call the base class `_process()` function.
	super._process(delta)

	# Ship Rotation.
	if Input.is_action_pressed("rotate_ccw"):
		rotation_degrees -= rotation_rate * delta

	if Input.is_action_pressed("rotate_cw"):
		rotation_degrees += rotation_rate * delta

	# Thrust.
	if Input.is_action_pressed("thrust"):

		# Discover the ship's angle in radians from the sin & cos functions.
		var direction = Vector2(cos(rotation), -sin(rotation))

		# Invert the y-axis to convert to screen coordinates.
		direction.y *= -1

		# Update the velocity.
		velocity += direction * thrust_power

	# Update the ship's position.
	position += velocity * delta
```


### Tap `F5`

- `▶` _**Play the game and make sure it works!**_
  - Does the ship rotate?
  - Does the ship move when thrust is held?


![Ship movement.](.docs/end.png)


## Commit & Push!

When satisfied with the results:
- Use **GitHub Desktop** to _**commit**_ and _**push**_ the project changes.
- **Should any issues arise, use Discord** to message me, and we'll sort it out together!


### Continued in Part 4
- [Space Rocks: 4. Bullet Time](https://gitlab.com/kirkja-leikjahonnunar/knowhow/-/blob/main/Tinker/Drills/Space%20Rocks/4.%20Bullet%20Time/README.md)
