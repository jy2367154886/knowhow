# Space Rocks: 6. Collision Mission

![The end is the beginning.](.docs/end.png)


## Objectives

- Object References:
  - Refactor the `Ship` scene by **splitting off the UI nodes into a new `Player` scene**.
  - **Declare and set all the _object reference_ variables** in each script that needs to communicate with another game object:
    - "Player.gd": `var main: Main`
    - "Ship.gd": `var player: Player`
    - "Rock.gd": `var player: Player`
    - "Bullet.gd": `var player: Player`
- Collision Logic:
  - Iterate the "Rock.gd" script:
    - Declare a _signal connection_ and _callback function_ for `area_entered` and `OnAreaEntered()`.
    - Create a `Split()` function that will despawn the current `Rock` and spawn 2 smaller `Rock` instances at the same location.



## Perform the Opening Dev Ritual

Once again, we'll make sure we _**stay informed and content synced,**_ so we never lose more than one day's worth of progress!


### GitHub Desktop

![](../assets/github.png) Open **GitHub Desktop** and check the **3 main tool buttons** across the top:

1. Is the `Current repository` button **set to our "spacerocks-username" project?**
1. Is the `Current branch` button **set to "main"?**
1. Click the `Fetch origin` button for good luck!
1. _**Click the:**_ `"show"` **button** to navigate to our _local project repo_ via the OS file manager:
   - _**Windows button:**_ `Show in Explorer`
   - _**Mac button:**_ `Show / Reveal in Finder`
   - _**Linux button:**_ `Show in your File Manager`


![Open the project repository with GitHub Desktop.](../assets/opening_dev_ritual.png)


### ![Godot logo.](../assets/folder.svg) File Manager

Our operating system should launch its default _file manager_, opening to the project repository folder.

  - _**Open the folder:**_ "Code"
  - _**Launch Godot, double-click:**_ "project.godot"


![Double-click: "Code/project.godot".](.docs/project_godot.png)



## ![Godot logo.](../assets/godot.svg) Godot - The `Ship` Scene: "Ship.tscn"

Let's make a new `Player` class saving part of our scene tree as a new scene that will represent the player and the player stats. We'll design the `Player` scene in with the functionality to:

- Track a player's score.
- Track a player's life count.
- Spawn a `Ship`. _(if the player has any ships remaining)_


### Node Work

- From the `Scene` tree:
  - Select the node: `UI????`
  - From it's context menu, select: `UI???` + `RMB` > `Save Branch as Scene`


![Save branch as Scene](.docs)

- Within the `Save New Sceen As` dialog box:
 - _**Create a new folder:**_ "Player"
 - _**Path:**_ "res://Player/"
 - _**Filename:**_ "Player.tscn"


### Parsing from the "Ship.gd" Script

- _**Open the script:**_ "Ship.gd"
- _**Select the functions:**_
  - OnWhatever()
  - OnThisToo()
- _**Cut the functions using:**_ `Ctrl` + `X`
- _**Open the script:**_ "Player.gd"
- _**Paste the functions using:**_ `Ctrl` + `V`


###### _"Ship.gd"_

```gdscript
extends ScreenWrapper
class_name Ship

const BULLET = preload("res://Bullet/Bullet.tscn")

var player: Player

var rotation_rate: float = 180.0 # degrees / sec.
var velocity: Vector2 = Vector2.ZERO
var thrust_power: float = 10.0


func _process(delta):
	super._process(delta)

	# Continuous hold.
	if Input.is_action_pressed("rotate_ccw"):
		rotation_degrees -= rotation_rate * delta

	if Input.is_action_pressed("rotate_cw"):
		rotation_degrees += rotation_rate * delta

	if Input.is_action_pressed("thrust"):
		var direction: Vector2 = Vector2(cos(rotation), -sin(rotation))
		direction.y *= -1 # Screen coords.
		velocity += direction * thrust_power

	position += velocity * delta


	# Clicks.
	if Input.is_action_just_pressed("fire"):
		var bullet: Bullet = BULLET.instantiate()
		bullet.player = player
		bullet.position = position
		bullet.rotation = rotation
		get_parent().add_child(bullet)

```
---
---

### 2D Graphics

- Open the `Bullet` scene from the `FileSystem` dock:
  - _**Double-click:**_ `res://` > `Bullet` > `Bullet.tscn`


- From the `Scene` tree:
  - _**Select the root node:**_ `Bullet`
  - _**Click the button:**_ `Add a new child node`


- From the `Create New Node` dialog box:
  - _**Search:**_ "Sprite2D"
  - _**Click the button:**_ `Create`

![New `Sprite2D` node.](.docs/new_sprite2d.png)


- From the `FileSystem` dock:
  - _**Drag:**_ `res://` > `Bullet` > `assets` > `"bullet.png"`
  - _**Drop on:**_ `Inspector` > `Sprite2D` > Texture: `<empty>` field.


- In the `2D` viewport:
  - _**Adjust the size of the**_ `Sprite2D` _**node graphics**_ to fit within the bounds of our `CollisionShape2D` nodes.
  - **Rotate the `Sprite2D` node to face right**. _(aligning it with the 0° angle)_


  ![The Bullet scene.](.docs/bullet_godot.png)


### Bullet Brain

###### _"Bullet.gd"_

```gdscript
extends ScreenWrapper
class_name Bullet

@onready var timer = $Timer

var speed: float = 300 # px / sec.

func _ready():
    timer.timeout.connect(OnTimedOut)

func _process(delta):
    super._process(delta)
    var direction = Vector2(cos(rotation), -sin(rotation))
    direction.y *= -1
    position += direction * speed * delta

func OnTimedOut():
    queue_free()
```

Next, we'll need to _**update our "Ship.gd"**_ script so the `Ship` will actually fire something.




### Tap `F5`

- `▶` **Play** the game and make sure it works!
  - The `Bullet` instances should fire from the ship:
    - Move forward at the same rotational angle as the ship.
    - Wrap around the screen.
    - Dispose of itself when the `Timer` triggers its `timeout` signal.


![Bullet time!](.docs/end.png)


## Commit & Push!

When satisfied with the results:
- Use **GitHub Desktop** to _**commit**_ and _**push**_ the project changes.
- **Should any issues arise, use Discord** to message me, and we'll sort it out together!

### Continued in Part 7
- "I want to believe." :alien:
