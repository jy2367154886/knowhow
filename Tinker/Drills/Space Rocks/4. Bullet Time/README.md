# Space Rocks: 4. Bullet Time

![The end is the beginning.](.docs/end.png)


## Objectives

- Iterate the `Bullet` scene.
  - Graphics:
    - Use a pixel paint program to **draw an image of a projectile**.
    - **Export the image** for use in a `Sprite2D` node.
  - Code:
    - **Spawn a `Bullet` instance** to the same location and rotation as the `Ship` instance.
    - **Update the `Bullet` position forward along its flight path**, base on its angle.
    - **Use a `Timer` node to destroy the bullet**, if it doesn't hit anything after a set length of time.
- Iterate the `Ship` scene.
  - **Spawn** a `Bullet` instance **every time** the player presses the **"fire" button.**


## Perform the Opening Dev Ritual

Once again, we'll make sure we _**stay informed and content synced,**_ so we never lose more than one day's worth of progress!


### ![](../assets/github.png) GitHub Desktop

Open **GitHub Desktop** and check the **3 main tool buttons** across the top:

1. Is the `Current repository` button **set to our "spacerocks-username" project?**
1. Is the `Current branch` button **set to "main"?**
1. Click the `Fetch origin` button for good luck!
1. _**Click the:**_ `"show"` **button** to navigate to our _local project repo_ via the OS file manager:
   - _**Windows button:**_ `Show in Explorer`
   - _**Mac button:**_ `Show / Reveal in Finder`
   - _**Linux button:**_ `Show in your File Manager`


![Open the project repository with GitHub Desktop.](../.docs/dev_ritual_opener.png)


### ![](../../.docs/folder.svg) File Manager

Our operating system should launch its default _file manager_, opening to the project repository folder.


## ![](../../.docs/krita.svg) Krita - "Bullet.kra"

- _**Launch Krita:**_ _(or your favorite pixel paint program)_
  - _**Create a new pixel canvas:**_
    - _**Width:**_ `32`
    - _**Height:**_ `32`
  - _**Draw a projectile in a transparent pixel layer.**_


![The Bullet layer.](.docs/bullet_krita.png)



- _**Export the ship layer:**_ `File` > `Export...`

- From the `Exporting` dialog box:
  - _**Look in:**_ "spacerocks-username**/Code/Bullet/assets/**"
  - _**File name:**_ "bullet.png"
  - _**Files of type:**_ `PNG Image`
  - _**Click the button:**_ `Save`


![](.docs/bullet_export.png)



| "Bullet/assets/" | Sprite Texture |
| :-: | :-: |
| "bullet.png" | ![bullet.png](.docs/bullet.png) |


## ![](../../.docs/folder.svg) File Manager

- Launch Godot:
  - _**Open the folder:**_ "Code"
  - _**Double-click:**_ "project.godot"


## ![](../../.docs/godot.svg) Godot - The `Bullet` Scene: "Bullet.tscn"


### 2D Graphics

- Open the `Bullet` scene from the `FileSystem` dock:
  - _**Double-click:**_ `res://` > `Bullet` > `Bullet.tscn`


- From the `Scene` tree:
  - _**Select the root node:**_ `Bullet`
  - _**Click the button:**_ `Add a new child node`


- From the `Create New Node` dialog box:
  - _**Search:**_ "Sprite2D"
  - _**Click the button:**_ `Create`

![New `Sprite2D` node.](../../.docs/create_new_node/sprite_2d.png)


- From the `FileSystem` dock:
  - _**Drag:**_ `res://` > `Bullet` > `assets` : `"bullet.png"`
  - _**Drop on field:**_ `Inspector` > `Sprite2D` > `Texture` : `<empty>`


- In the `2D` viewport:
  - _**Adjust the size of the**_ `Sprite2D` _**node graphics**_ to fit within the bounds of our `CollisionShape2D` nodes.
  - **Rotate the `Sprite2D` node to face right**. _(aligning it with the 0° angle)_


  ![The Bullet scene.](.docs/bullet_godot.png)


### Bullet Brain

###### _"Bullet.gd"_

```gdscript
extends ScreenWrapper
class_name Bullet

@onready var timer = $Timer

var speed: float = 300 # px / sec.

func _ready():
	timer.timeout.connect(OnTimedOut)

func _process(delta):
	super._process(delta)
	var direction = Vector2(cos(rotation), -sin(rotation))
	direction.y *= -1
	position += direction * speed * delta

func OnTimedOut():
	queue_free()
```

Next, we'll need to _**update our "Ship.gd"**_ script so the `Ship` will actually fire something.


###### _"Ship.gd"_

```gdscript
extends ScreenWrapper
class_name Ship

const BULLET = preload("res://Bullet/Bullet.tscn")

var rotation_rate: float = 180.0 # degrees / sec.
var velocity: Vector2 = Vector2.ZERO
var thrust_power: float = 10.0 # ??

func _process(delta):
	super._process(delta)

	# Continuous hold.
	if Input.is_action_pressed("rotate_ccw"):
		rotation_degrees -= rotation_rate * delta

	if Input.is_action_pressed("rotate_cw"):
		rotation_degrees += rotation_rate * delta

	if Input.is_action_pressed("thrust"):
		var direction = Vector2(cos(rotation), -sin(rotation))
		direction.y *= -1
		velocity += direction * thrust_power

	position += velocity * delta

	# Clicks or taps.
	if Input.is_action_just_pressed("fire"):
		var bullet = BULLET.instantiate()
		get_parent().add_child(bullet)
		bullet.position = position
		bullet.rotation = rotation
```


### Tap `F5`

- `▶` **Play** the game and make sure it works!
  - The `Bullet` instances should fire from the ship:
    - Move forward at the same rotational angle as the ship.
    - Wrap around the screen.
    - Dispose of itself when the `Timer` triggers its `timeout` signal.


![Bullet time!](.docs/end.png)


## Commit & Push!

When satisfied with the results:

- Use **GitHub Desktop** to _**commit**_ and _**push**_ the project changes.
- **Should any issues arise**, use **Discord** to send a message, and we'll sort it out together!

### Continued in Part 5

- [Space Rocks: 5. Hi UI](https://gitlab.com/kirkja-leikjahonnunar/knowhow/-/blob/main/Tinker/Drills/Space%20Rocks/5.%20Hi%20UI/README.md)
