# Space Rocks: 5. Hi UI

![The end is the beginning.](.docs/end.png)


## Objectives

- Create a player user interface to **display the player's score and lives remaining**.
- Use `Node` & `Control` nodes within the `Scene` tree to **create a functional layout**.
- **Edit the "Ship.gd" script** to add functionality.
- **Test** the functionality.


## ![](../../.docs/github.png) GitHub Desktop

### Perform the Opening Dev Ritual

Once again, we'll make sure we _**stay informed and content synced,**_ so we never lose more than one day's worth of progress!

Open **GitHub Desktop** and check the **3 main tool buttons** across the top:

1. Is the `Current repository` button **set to our "spacerocks-username" project?**
1. Is the `Current branch` button **set to "main"?**
1. Click the `Fetch origin` button for good luck!
1. _**Click the:**_ `"show"` **button** to navigate to our _local project repo_ via the OS file manager:
   - _**Windows button:**_ `Show in Explorer`
   - _**Mac button:**_ `Show / Reveal in Finder`
   - _**Linux button:**_ `Show in your File Manager`


![Open the project repository with GitHub Desktop.](../.docs/dev_ritual_opener.png)


## ![](../../.docs/folder.svg) File Manager

Our operating system should launch its default _file manager_, opening to the project repository folder.

- _**Open the folder:**_ "Code"
- _**Launch Godot, double-click:**_ "project.godot"


## ![](../../.docs/godot.svg) Godot - The `Ship` Scene: "Ship.tscn"

We'll create a user interface and data variables to store, calculate, and update the graphics of our player's game state. Functionally, the UI should track the player's `score` and `life_count`.

Since we are building the UI system to store the player's stats, it makes OOP sense to store the data in the `Ship` scene.

- Open the `Ship` scene from the `FileSystem` dock:
  - _**Double-click:**_ `res://` > `Ship` > `Ship.tscn`


### Screen Space Layout

One of the great features of Godot is how one can arrange `Scene` tree nodes into specific parenting patterns to access different parts of the graphics engine. Meaning that if we arrange the nodes in the right order, we'll be able to use 2D, 3D, or UI assets at any time. This gives us a way to keep UI elements as individual parts of one larger scene. Let's add several new nodes!


#### 1. Add and rename a `Node` to "ScreenUI".

- From the `Scene` tree:
  - _**Select the root node:**_ `Ship`
  - _**Click the button:**_ `+` _(Add Add Child Node)_
- Within the `Create New Node` dialog box:
  - _**Search:**_ "Node" _(hot tip: it's always the top one)_
  - _**Click the button:**_ `Create`
- Back in the `Scene` tree:
  - _**Rename the**_ `Node` _**to:**_ "ScreenUI"


![Create New Node: `Node`](../../.docs/create_new_node/node.png)


#### 2. Add and rename a `PanelContainer` to "PlayerUI".

- In the `Scene` tree:
  - _**Select the node:**_ `ScreenUI`
  - _**Click the button:**_ `+` _(Add Add Child Node)_
- Within the `Create New Node` dialog box:
  - _**Search:**_ "PanelContainer"
  - _**Click the button:**_ `Create`
- `Scene` tree:
  - _**Rename the**_ `PanelContainer` _**node to:**_ "PlayerUI"


![Create New Node: `PanelContainer`](../../.docs/create_new_node/panel_container.png)


#### 3. Add and rename a `VBoxContainer` to "VBox".

- `Scene` tree:
  - _**Select the node:**_ `PlayerUI`
  - _**Click the button:**_ `+`
- `Create New Node` dialog box:
  - _**Search:**_ "VBoxContainer"
  - _**Click the button:**_ `Create`
- `Scene` tree:
  - _**Shorten the**_ `VBoxContainer` _**name to:**_ "VBox"


![Create New Node: `VBoxContainer`](../../.docs/create_new_node/v_box_container.png)


#### 4. Add and rename a `Label` to "ScoreUI".

- `Scene` tree:
  - _**Select the node:**_ `VBox`
  - _**Click the button:**_ `+` _(Add Add Child Node)_
- `Create New Node` dialog box:
  - _**Search:**_ "Label"
  - _**Click the button:**_ `Create`
- `Scene` tree:
  - _**Rename the**_ `Label` _**node to:**_ "ScoreUI"


![Create New Node: `Label`](../../.docs/create_new_node/label.png)


#### 5. Add and rename an `HBoxContainer` to "LivesUI".

Since we'd like to **display the player's life images in a row under the player's score**, we'll attach an `HBoxContainer` node to the `VBoxContainer` node called "VBox". Any elements we add as children to the `HBoxContainer` will align horizontally.

- `Scene` tree:
  - _**Select the node:**_ `VBox`
  - _**Click the button:**_ `+` _(Add Add Child Node)_
- `Create New Node` dialog box:
  - _**Search:**_ "HBoxContainer"
  - _**Click the button:**_ `Create`
- `Scene` tree:
  - _**Rename the**_ `HBoxContainer` _**node to:**_ "LivesUI"


![Create New Node: `HBoxContainer`](../../.docs/create_new_node/h_box_container.png)


#### 6. Add and rename a `TextureRect` to "Life".

`TextureRect` is the `Control` version of the `Sprite2D` node. Meaning, it has a **Texture:** field within the `Inspector` dock that we will fill with an image.

- `Scene` tree:
  - _**Select the node:**_ `LivesUI`
  - _**Click the button:**_ `+`
- `Create New Node` dialog box:
  - _**Search:**_ "TextureRect"
  - _**Click the button:**_ `Create`
- `Scene` tree:
  - _**Rename the**_ `TextureRect` _**node to:**_ "Life"


![Create New Node: `TextureRect`](../../.docs/create_new_node/texture_rect.png)


- From the `FileSystem` dock:
  - _**Drag:**_ `res://` > `Ship` > `assets` : `"ship.png"`
  - _**Drop on:**_ `Inspector` > `TextureRect` > `Texture` : `<empty>`


#### Scene Tree Results

Our `Scene` tree hierarchy should now look something like this:

![](.docs/ui_pieces.png)


### The `Life` Scene: "life.tscn"

Since the "LivesUI" `HBoxContainer` node will align our elements for us, all we need to do is attach and detach instances of the `Life` node. If we turn `Life` into a scene of its own, we'll be able to spawn them easier.



- From the `Scene` tree:
  - _**Context menu, select:**_ `Life` + `RMB` > `Save Branch as Scene`
- In the `Save New Scene As...` dialog box:
  - _**Path:**_ "res://Ship/assets"
  - _**Click:**_ `Save`


![Save New Scene As... "life.tscn"](.docs/save_as_life.png)
_(Note: We use all lower case for the filename this time because it doesn't have a script. It's a part of something else.)_


### Scripting the Brains: "Ship.gd"

Now that we have the pieces, let's put them to use. We'll update our "Ship.gd" script to include new functionally for the user interfaces for player score and lives.


#### Updating the Score UI with: `Ship.UpdateScore()`


###### _"Ship.gd"_

```gdscript
@onready var score_ui = $ScreenUI/UI/VBox/ScoreUI

var score: int = 0

func UpdateScore(delta_score: int):
    score += delta_score
    score_ui.text = str(score)
```


#### Updating the Lives UI with: `Ship.UpdateLives()`


###### _"Ship.gd"_

```gdscript
const LIFE = preload("res://Ship/assets/life.tscn")

@onready var lives_ui = $ScreenUI/UI/VBox/LivesUI

var life_count: int = 3

func UpdateLives(delta_lives: int):
	life_count += delta_lives

	if life_count >= 0:
		# Delete all children in LivesUI.
		for child in lives_ui.get_children():
			lives_ui.remove_child(child)
			child.queue_free()

		# Refresh the life graphics.
		for life in life_count:
			lives_ui.add_child(LIFE.instantiate())
```


### Testing the Functionally

Now that we've implemented our logic, we need to test it out. But we'd like to do it quickly without making further assets, so we'll test out the UI by hooking up test code to the "fire" section of our `Ship._process()` function.

#### Just a Test

- Within the "Ship.gd" script:
  - _**Navigate to the function declaration:**_ `_process(delta):`
  - _**Script test logic inside the condition block:**_ `Input.is_action_just_pressed("fire"):`
    - _**Test the positive direction with:**_ `UpdateScore(2)`
    - _**Test the positive direction with:**_ `UpdateLives(1)`


```gdscript
func _process(delta):

    # Single click.
    if Input.is_action_just_pressed("fire"):
        var bullet = BULLET.instantiate()
        get_parent().add_child(bullet)
        bullet.position = position
        bullet.rotation = rotation

        # Testing
        UpdateScore(2)
        UpdateLives(1)
```


##### Tap `F5`

- `▶` _**When "fire" is pressed:**_
  - Does the ship score increase by 2?
  - Do the ship icons appear in a row, 1 at a time?



#### Subtractive Test

- Within the "Ship.gd" script:
  - _**Navigate to the function declaration:**_ `_process(delta):`
  - _**Script test logic inside the condition block:**_ `Input.is_action_just_pressed("fire"):`
    - _**Test the positive direction with:**_ `UpdateScore(-3)`
    - _**Test the positive direction with:**_ `UpdateLives(-1)`


```gdscript
func _process(delta):

    # Single click.
    if Input.is_action_just_pressed("fire"):
        var bullet = BULLET.instantiate()
        get_parent().add_child(bullet)
        bullet.position = position
        bullet.rotation = rotation

        # Testing
        UpdateScore(-3)
        UpdateLives(-1)
```


##### Tap `F5`

- `▶` _**When "fire" is pressed:**_
  - Does the ship score decrease by 3?
  - Can we attain a negative score?
  - Do the ship icons deplete 1 at a time?
  - Can we attain negative lives? Probably, but the UI won't display negative images so were good!


### Finishing Up

With each successful test our code gets stronger, **but let's not forget to remove the test code before we commit!**

- Within the "Ship.gd" script:
  - _**Navigate to the function declaration:**_ `_process(delta):`
  - _**Script test logic inside the condition block:**_ `Input.is_action_just_pressed("fire"):`
    - _**Remove the test line:**_ `UpdateScore(-3)`
    - _**Remove the test line:**_ `UpdateLives(-1)`


#### Tap `F5`

- `▶` _**Play the game and make sure it still runs.**_
  - Neither UI should be affected when the "fire" action is pressed.


![Ship movement.](.docs/end.png)


## Commit & Push!

When satisfied with the results:

- Use **GitHub Desktop** to _**commit**_ and _**push**_ the project changes.
- **Should any issues arise**, use **Discord** to send a message, and we'll sort it out together!


### Continued in Part 6

- [Space Rocks: 6. Under Construction](./)

---

Here is the complete and combined code for the "Ship.gd" script.


###### _"Ship.gd"_

```gdscript
extends ScreenWrapper
class_name Ship

const BULLET = preload("res://Bullet/Bullet.tscn")
const LIFE = preload("res://Ship/assets/life.tscn")

@onready var lives_ui = $ScreenUI/UI/VBox/LivesUI
@onready var score_ui = $ScreenUI/UI/VBox/ScoreUI

var score: int = 0
var life_count: int = 3

var rotation_rate: float = 180.0 # degrees / sec.
var velocity: Vector2 = Vector2.ZERO
var thrust_power: float = 10.0

func _ready():
	UpdateLives(0)

func _process(delta):
	super._process(delta)

	# Continuous hold.
	if Input.is_action_pressed("rotate_ccw"):
		rotation_degrees -= rotation_rate * delta

	if Input.is_action_pressed("rotate_cw"):
		rotation_degrees += rotation_rate * delta

	if Input.is_action_pressed("thrust"):
		var direction = Vector2(cos(rotation), -sin(rotation))
		direction.y *= -1 # Screen coords.
		velocity += direction * thrust_power

	position += velocity * delta

	# Single click.
	if Input.is_action_just_pressed("fire"):
		var bullet = BULLET.instantiate()
		get_parent().add_child(bullet)
		bullet.position = position
		bullet.rotation = rotation

func UpdateScore(delta_score: int):
	score += delta_score
	score_ui.text = str(score)

func UpdateLives(delta_lives: int):
	life_count += delta_lives

	if life_count >= 0:
		# Delete all children in LivesUI.
		for child in lives_ui.get_children():
			lives_ui.remove_child(child)
			child.queue_free()

		# Refresh the life graphics.
		for life in life_count:
			lives_ui.add_child(LIFE.instantiate())
```
