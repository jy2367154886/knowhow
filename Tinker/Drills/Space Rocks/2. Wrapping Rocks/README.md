# Space Rocks: 2. Wrapping Rocks

![The end is the beginning.](.docs/end.png)


## Objectives

- Graphics:
  - Use a paint program to _**draw 3 -5 rock images**_.
  - _**Export the images**_ for use in an `AnimatedSprite2D` node.
- Code:
  - _**Randomize the Rock's sprite texture**_ from the 3 - 5 imported images.
  - _**Add a random velocity** (direction + speed)_ to the "Rock.gd" script.
  - _**Add some rotational spin**_ to the `Rock` behavior.
  - _**Create a super class with the ability to screen wrap**_ to the opposite side of the game window.


## Perform the Opening Dev Ritual

Let's make sure we _**stay informed and content synced,**_ so we never lose more than one day's worth of progress!


### ![](../../.docs/github.png) GitHub Desktop

Open **GitHub Desktop** and check the **3 main tool buttons** across the top:

1. Is the `Current repository` button **set to our "spacerocks-username" project?**
1. Is the `Current branch` button **set to "main"?**
1. Click the `Fetch origin` button for good luck!
1. _**Click the:**_ `"show"` **button** to navigate to our _local project repo_ via the OS file manager:
   - _**Windows button:**_ `Show in Explorer`
   - _**Mac button:**_ `Show / Reveal in Finder`
   - _**Linux button:**_ `Show in your File Manager`

![Open the project repository with GitHub Desktop.](../.docs/dev_ritual_opener.png)


### ![](../../.docs/folder.svg) File Manager

Our operating system should launch its default _file manager_, opening to the project repository folder.

  - _**Open the folder:**_ "Code"
  - _**Launch Godot, double-click:**_ "project.godot"


![Double-click: "Code/project.godot".](.docs/project_godot.png)


## Pixel Painting

"Pixel Pushers" are a class of digital painting tools that color a grid of very small squares different colors. Each pixel contains data. The most common data that pixels contain are RGBA _(red, green, blue, alpha)_ data.


 There are many different pixel painting applications for desktops, tablets, and mobile devices. The most well-known of these is Photoshop, but these drills use Krita.


## ![](../../.docs/krita.svg) Krita

Krita is a great open-source pixel pusher - [Official Krita Website](https://krita.org)

- _**Launch Krita:**_ _(or your favorite pixel paint program)_
  - _**Create a new pixel canvas:**_
    - _**Width:**_ `32`
    - _**Height:**_ `32`
  - _**Draw on different layers:**_ 3 - 5 randomly shaped space rocks.

![Rocks in pixel layers.](.docs/rock_layers.png)


- We'll export each layer as an individual ".png" file:
  - _**Hide all the layers except one rock layer, click the small eyeball button:**_ `👁`
  - _**Click the option:**_ `File` > `Export...`


- From the `Exporting` dialog box:
  - _**Look in:**_ "spacerocks-username**/Code/Rock/assets/**"
  - _**File name:**_ "rock_a.png"
  - _**Files of type:**_ `PNG Image`
  - _**Click the button:**_ `Save`


- _**Repeat the export process for the remaining layers:**_
  - "rock_b.png"
  - "rock_c.png"
  - "rock_d.png"
  - "rock_e.png"

![Exporting individual rock layers.](.docs/export_rocks.png)


## ![](../../.docs/godot.svg) Godot

If we head over to Godot, we'll be able to see the newly exported ".png" files. Look over at the `FileSystem` dock, then _**open the folder:**_ `res://` > `Rock` > `assets`

![](.docs/rock_assets.png)

| "Rock/assets/" | Sprite Texture    |
| :-----------------------: | :---------------: |
| "rock_a.png" | ![rock_a.png](.docs/rock_a.png) |
| "rock_b.png" | ![rock_b.png](.docs/rock_b.png) |
| "rock_c.png" | ![rock_c.png](.docs/rock_c.png) |
| "rock_d.png" | ![rock_d.png](.docs/rock_d.png) |
| "rock_e.png" | ![rock_d.png](.docs/rock_e.png) |



## ![](../../.docs/godot.svg) The `ScreenWapper` super class: "ScreenWapper.gd"

Before diving into the `Rock` scene too quickly, **we should examine the pieces of our game to determine if any of them have similar behaviors**. Our `Rock`, `Ship`, and `Bullet` scenes all need the ability to _"wrap"_ to the opposite side of the screen, if they fly off the edge the game window.

Since we have multiple classes that perform the same behavior, we should consolidate our reusable code into a single script by creating a simple _super class_. A _super class_ is a regular _class_ that has been _inherited from_.

Let's create a super class to hold the screen wrapping logic called: `ScreenWrapper`. Afterwards, `Rock`, `Ship`, and `Bullet` will inherit the screen wrapping powers by updating their scripts to: `extends ScreenWrapper`

- From the `FileSystem` tab:
  - _**Create a new folder:**_ "ScreenWrapper"
  - _**Create a new script:**_ `RMB` + `res://` > `Create New` > `Script...`

![](.docs/create_screen_wrapper_script.png)


- From the `Create Script` dialog box:
  - _**Name the script:**_  "ScreenWapper.gd"
  - _**Click the button:**_ `Create`

![](.docs/name_script.png)

###### _"ScreenWrapper.gd"_

```gdscript
extends Node2D
class_name ScreenWrapper

func _process(delta):
	if position.x < 0:
		position.x = get_viewport().size.x
	if position.x > get_viewport().size.x:
		position.x = 0
	if position.y < 0:
		position.y = get_viewport().size.y
	if position.y > get_viewport().size.y:
		position.y = 0
```


## ![](../../.docs/godot.svg) The `Rock` Scene: "Rock.tscn"

- From the `FileSystem` dock:
  - Open the `Rock` scene, _**double-click:**_ `res://` > `Rock` > `Rock.tscn`


- Within the `Scene` tree:
  - _**Select the root node:**_ `Rock`
  - _**Add a new node:**_ `AnimatedSprite2D`


- Over in the `Inspector` dock:
  - _**Select the option, Sprite Frames:**_ `New SpriteFrames`

![Create a new SpriteFrames resource.](.docs/new_sprite_frames.png)


Notice that a new tab called `SpriteFrames` appeared along the bottom of the editor, once a SpriteFrames resource is loaded.

- Open the `SpriteFrames` editor:
  - _**Click the tab:**_ `SpriteFrames`
  _Note: If you can't find the `Sprite Frames` tab, make sure that `AnimatedSprite2D` is selected in the `Scene` tree dock._


### SpriteFrames

`SpriteFrames` are like animation flipbooks. When `AnimatedSprite2D.playing = true`: the 2D game engine swaps out one texture for another, at the frame rate listed in the `SpriteFrames.speed` parameter. We will hold off on playing the animation until we tackle the Ship and Bullet classes. **Instead of playing the rock sprites as an animation, we will use the sprite list to pick a random starting frame.**

- **Drag each rock sprite** from: the `FileSystem` > "res://Rock/Rock/assets/" folder **to the `SpriteFrames` tab**.
- Next **add a `CollisionShape2D`** node.
- Set the parameter **Shape: `CircleShape2D`**.
- **Adjust the _red handles_ in the `2D` viewport** to fit around the general shape of our artwork.

![The Rock scene.](.docs/rock_scene.png)


### Setting up a test in the _"Main.tscn"_ scene.
Now that we have the Rock's graphical assets game ready, **let's drag a few Rocks to the `2D` viewport**, so we can see their behavior when we play the game.

![Setup a rock test in the "Main.tscn" scene.](.docs/rocking_main.png)


### Rock Behaviors

**Q:** How do we **describe "what a Rock is"** to the game engine? **A Rock:**

- has an initial random **velocity**.
- has a random **rotation speed**.
- can **wrap around the screen**.
- can be damaged by Bullets.
- can damage Ship.

We'll put the `Rock` behaviors within the `_ready()` and `_process()` functions.

###### _"Rock.gd"_

```gdscript
extends ScreenWrapper
class_name Rock

@onready var animated_sprite_2d = $AnimatedSprite2D

var speed: float = 150 # px / sec
var rotation_speed: float = 180 # degrees / sec.
var direction: Vector2 = Vector2(0, -1)


func _ready():
	speed = randf_range(150, 300)
	direction.x = randf_range(-1, 1)
	direction.y = randf_range(-1, 1)
	rotation_speed = randf_range(-180, 180)
	animated_sprite_2d.frame = randi_range(0, 4)


func _process(delta):
	position += direction * speed * delta
	rotation_degrees += rotation_speed * delta
	super._process(delta)
```


### Tap `F5`

- `▶` **Play** the game and make sure it works!
- The Rocks on the stage should fly different directions at different speeds.
- They should also rotate at different speeds.
- Upon reaching one edge of the screen Rocks should teleport to the opposite side of the screen.

![Wrapping Rocks!](.docs/end.png)


## Commit & Push!

When satisfied with the results:

- Use **GitHub Desktop** to _**commit**_ and _**push**_ the project changes.
- **Should any issues arise**, use **Discord** to send a message, and we'll sort it out together!


### Continued in Part 3

- [Space Rocks: 3. Ship Controls](https://gitlab.com/kirkja-leikjahonnunar/knowhow/-/blob/main/Tinker/Drills/Space%20Rocks/3.%20Ship%20Controls/README.md)
