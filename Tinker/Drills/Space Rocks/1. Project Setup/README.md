# Space Rocks: 1. Project Setup

_Last refreshed using: [Godot 4.1.2.stable](https://godotengine.org)._

![The end is the beginning.](.docs/end.png)


## Objectives

- Review the **Space Rocks GDD**.
- Setup the work space to **begin the Space Rocks** project.
- Block out the _**object classes**_ in Godot.



## Space Rocks GDD

Remember a good GDD should be a _**"living document"**_ that is **edited and maintained** alongside the rest of the project's production. If it becomes obsolete, **it needs to be updated**.

The Space Rocks GDD should include:
- A list of all the pieces needed to play.
- The rules for the game and how the game mechanics work.
- Any endgame conditions.
- The intended player experience.


### Pieces of Space Rocks _(Classes)_:

- Main
- Rock
- Ship
- Bullet
- UFO
- UI
  - Player Score
  - Player Lives
  - Hi Score
  - Game Menu


![Space Rocks GDD.](.docs/space_rocks_gdd.png)

_(Try to keep the GDD simple and easy to read.)_


### Rules

- Numerous `Rock` instances fly around the screen at different velocities.
- When game objects move beyond the edge of the game window, they _screen wrap,_ to reappear at the opposite edge of the viewport.
- The player maneuvers the `Ship` trying to avoid the `Rock` instances.
  - Rotate ⭯ _(counter clockwise)_: `A`
  - Rotate ⭮ _(clockwise)_: `D`
  - Thrust forward: `RMB`
  - Fire a `Bullet` instance: `LMB`
- If one of the player's `Bullet` instances collides with a `Rock`, the `Rock` will break into smaller rocks.
- The smaller the rock the more points are added to the player score UI.
- If the `Ship` collides with anything, it explodes and the player loses a life from the UI.
- A game menu should handle the pause and quit options if the player hits `Esc` key.


## Clone the Project Repo

- **SpaceRocks** - https://classroom.github.com/a/8qi-Oj40


### GitHub Classroom

The link above should open our default web browser to a **GitHub Classroom** page with an invitation to accept the **SpaceRocks** assignment.

- Click the `Accept this assignment` button.
- This creates a unique online repository with the format _**reponame-username**_.
- **Only you and I have access to the content of this repo**.
- Then **refresh the page**.
- After refreshing, **click the link to your repository**. _(example: https://github.com/PushingPolygons/spacerocks-username)_


### GitHub.com

- Once on the repo page:
  - **Click the green `Code`** button.
  - Then **select the `Open with GitHub Desktop`** option.


###  ![](../../.docs/github.png) GitHub Desktop

- GitHub Desktop should launch, opening up to the `URL` tab, and auto-fill the **Repository URL**.
- We'll set our **Local path** to our favorite hard drive location game projects.
- **Click the `Clone`** button.

![Clone the project repository.](.docs/clone_spacerocks.png)


- Once cloning is complete, we can click the "show" button to navigate to our local project via the OS file manager:
  - _**Windows button:**_ `Show in Explorer`
  - _**Mac button:**_ `Show / Reveal in Finder`
  - _**Linux button:**_ `Show in your File Manager`

![GitHub Desktop](../.docs/github_desktop.png)


###  ![](../../.docs/folder.svg) File Manager

Our operating system should launch its default _file manager_, opening to the project repository folder.

  - _**Open the folder:**_ "Code"
  - _**Launch Godot, double-click:**_ "project.godot"


![Double-click: "Code/project.godot".](.docs/project_godot.png)



## ![](../../.docs/godot.svg) Godot

### Space Rocks: Object Classes

By splitting each _**major component**_ of our _**game system**_ into distinct _**object classes**_ (object classifications) will allow us to focus on one component at a time.

- Each class should have their own **Folder, Scene, and Script**.
  - Rock
  - Ship
  - Bullet
  - UFO


### New Folders...

- From the `FileSystem` dock:
  - _**Open the context menu, then select:**_ `res://` + `RMB` > `Create New` > `Folder...`

![`RMB` > `New Folder...`.](.docs/filesystem_new_folder.png)

- Within the `Create Folder` dialog box:
  - _**Create new folder in res://:**_ "Rock"
  - _**Click:**_ `OK`

![`Create Folder` popup.](.docs/create_folder.png)


### New Scenes...

Since we would like our game pieces to interact with one another, we'll be creating our scenes with an `Area2D` as the _root node_.

- From the `FileSystem` dock:
  - _**Select the folder:**_ "Rock"
  - _**Open the context menu, then select:**_ `res://` > `Rock` + `RMB` > `Create New` > `Scene...`


![`RMB` > `New Scene...`.](.docs/filesystem_new_scene.png)


- Within the `Create New Scene` dialog box:
  - _**Click the small button:**_ `Node tree` _(just to the right of the bottom radio button)_


![](.docs/scene_tree.png)


- From the `Pick Root Node Type` dialog box:
  - _**Search:**_ "Area2D"
  - _**Click:**_ `Pick`


![](.docs/pick_area2d.png)


- Then back in the `Create New Scene` dialog box:
  - _**Root Type:**_ `Area2D`
  - _**Scene Name:**_ "Rock" `.tscn`
  - _**Click:**_ `OK`


![Create a new scene called "Rock".](.docs/create_new_scene.png)


### Add Colliders...

Notice the warning displayed next to the `Rock` node. The `Area2D` node requires the addition of _at least one_ `CollisionShape2D` node before it can detect anything with its "hitbox".


![](.docs/area2d_warning.png)


- In the `Scene` tree:
  - _**Select the root node:**_ `Rock`
  - _**Click the button:**_ `+` _(just above, and to the left, of the root node)_


![](.docs/click_plus.png)


- From the `Create New Node` dialog box:
  - _**Search:**_ "CollisionShape2D"
  - _**Tap:**_ `Enter`


![](.docs/add_collision.png)


Now there is a warning on `CollisionShape2D` node... but that's okay! We haven't finished setting up its parameters yet.

- From the `Scene` tree:
  - _**Select:**_ `CollisionShape2D`
- From the `Inspector` dock:
  - _**Select the Shape:**_ `New CircleShape2D`


![](.docs/new_circelshape.png)



### Attach Scripts...

GDScripts will act as the "brains" of our _game objects_, making them great locations to store our desired behaviors and logic.

- From the `Scene` tree:
  - _**Select the root node:**_ `Rock`
  - _**Click the small button:**_ `white script with green plus` _(Attach new or existing script to selected node.)_


![](.docs/add_script_button.png)


- Within the Attach Node Script dialog box:
  - _**Leave the auto-filled defaults for Path:**_ "res://Rock/Rock.gd"
  - _**Click:**_ `Create`.


![Attach a new script to the root node.](.docs/attach_script.png)


##  ![](../../.docs/godot.svg) Repeat the Workflow

- **Repeat the Folder, Scene, and Script creation steps** listed above, for the 3 remaining _game object classes_:
  - **Ship**
  - **Bullet**
  - **UFO**


## ![](../../.docs/godot.svg) Game Window Size

For this project let's make a classic arcade playing field. We'll display our game with _**portrait**_ proportions, instead of the default _**landscape**_ layout.

- From the `Project` menu:
  - _**Select:**_ `Project Settings...`
- From the Project Settings dialog box:
  - _**Navigate to:**_ `General` > `Display` > `Window` > `Size` :
    - _**Viewport Width:**_ `500`
    - _**Viewport Height:**_ `800`
    - _**Mode:**_ `viewport`
    - _**Aspect:**_ `keep`


![`Project` > `Project Settings...` > `General` > `Display` > `Window` > `Size`.](.docs/window_size.png)



### Play the Project: `F5`

Click the `▶` **play** button and make sure it works! What are our results? It's good practice to **test the results of our project as often as possible**. It will help us detect bugs we accidentally introduce. If we clean them up as we go, we will have a more stable code base.

- To run the project either:
  - _**Click the Play button:**_ `▶`
  -or _**tap the key:**_ `F5`
- There isn't anything going on yet, but the `(DEBUG)` window starts with a tall aspect ratio.


![It runs!](.docs/f5.png)


##  ![](../../.docs//godot.svg) Game Scale Lineup
Before going too far down the development flow, _**we should drag all of our game pieces to the "Main.tscn"**_. We'd like to make sure everything the pieces are scaled appropriately in relation to one other. It is often better to figure out the proportions of our screen space "real estate" early on in the project.


- We should now have **a clean project with our classes all set up**  ready to jam.


![All set up!](.docs/main_scene.png)


### Play the Project: `F5`

Since at this point we are only using `CollisionShape2D` nodes for our graphics; when we run the game as is, **we won't be able to see anything.** Game engines don't usually display colliders at runtime. Thus, we must tell Godot to show them to us first!

- From the `Debug` menu:
  - _**Enable:**_ `Visible Collision Shapes`


![Turn on `Visible Collision Shapes`](.docs/visible_collision_shapes.png)


- _**Tap:**_ `F5`
 - We should now see our colliders being displayed, and our game pieces all lined up.
 - Continue making adjustments to the `CollisionShape2D` parameters for each piece until achieving the desired shapes and sizes.


![All set up!](.docs/end.png)


## Commit & Push!

When satisfied with the results:

- Use **GitHub Desktop** to _**commit**_ and _**push**_ the project changes.
- **Should any issues arise**, use **Discord** to send a message, and we'll sort it out together!


### Continued in Part 2

[Space Rocks: 2. Wrapping Rocks](https://gitlab.com/kirkja-leikjahonnunar/knowhow/-/blob/main/Tinker/Drills/Space%20Rocks/2.%20Wrapping%20Rocks/README.md)
