# Tinker Drills
The **Drills** folder should hold game dev assignments and activities that demonstrate the principal flows of game development.

Look to the "~/Pushing Polygons/Tinker Templates/" folder for the project templates that generate the GitHib Classroom links.
