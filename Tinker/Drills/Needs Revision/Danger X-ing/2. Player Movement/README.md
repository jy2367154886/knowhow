# Danger X-ing: Player Movement
- Create player input map actions using `Project` > `Project Settings...` > `Input Maps`.
- Hook up the actions to functions in the Player script.
- Play audio when the player moves.


## GitHub Desktop
Let's make sure our project is up-to-date by opening Github Desktop and make sure there haven't been any changes since we last opened the project.

1. Check that the Repository is set to **"dangerxing-username"**.
1. Check that the Branch is set to **"main"**.
1. _If there are any changes that we forgot to commit_, **let's commit them now**.
1. Depending on the OS, click the **`Show in your File Browser`**, **`Show in Explorer`**, or **`Show in Finder`**.

## Godot
In order to keep player input organized, Godot has an interface to allow us to add new actions and call them from script. Navigate to the `Project` > `Project Settings...` > `Input Maps` tab.

1. Let's create a new action by typing **"move_forward"** into the **Action:** text field.
1. Click the **`Add`** button to submit "move_forward" to the action list.
1. Click the **`+`** on the "move_forward" line and select **`Physical Key`** from the dropdown menu.
1. Press the **`W`** key _(or whatever we prefer instead)_ to assign the key to the "move_forward" action.

![Input Map Actions](docs/input_map_actions.png)

We follow the same steps for our remaining actions:
- **"move_back"**
- **"move_left"**
- **"move_right"**

_(Quick Tip: We can add our actions quickly by typing the name into the **Action:** text field, hitting `Enter` then typing the next action name, and hitting `Enter` again.)_

![Player input map setup.](docs/input_map_complete.png)

### Player.tscn
Now that we have our actions registered with the game engine, we can hook them up to functions within the "Player.gd" script. Remember that our player is moving around the **X/Z plane** so we'll use "translation**.x**" & "translation**.z**" plus the **"speed"** variable to move the player around the playing field.

#### Position
- The Player should only move one space at a time. We can achieve this with the **"Input.is_action_just_pressed()"** function. It tells us if the player just pressed the input on this frame.

#### Rotation
- The Player character should also turn the direction just pressed. We will use the **"rotation_degrees.y"** property to orient the Player.

#### Audio
1. We would also like to play an audio clip when the player moves. So we'll need to add an **AudioStreamPlayer** node to our Player scene.
1. Rename the node **"MovementAudio"**.
1. Locate the audio clip in the **FileSystem** tab. Drag it to the **Inspector > Stream** field. We will access this node from our script and tell it to **play()** when input is detected.

![Player audio setup.](docs/player_audio.png)

- We would like to slightly adjust the **pitch_scale** of our audio. Otherwise the player will quickly encounter audio fatigue since we play the move sound a lot!
- We will make a new function called **PlaySound()** that will handle most of this for us.

#### Player.gd
Now let's put it all together inside the Player's brain so we can run round the field, turning, and making noise.

```gdscript
extends Area
class_name Player

var speed = 2.0

func _process(delta):
	if Input.is_action_just_pressed("move_forward"):
		translation.z += -speed
		rotation_degrees.y = 0
		PlaySound()

	if Input.is_action_just_pressed("move_back"):
		translation.z += speed
		rotation_degrees.y = 180
		PlaySound()

	if Input.is_action_just_pressed("move_left"):
		translation.x += -speed
		rotation_degrees.y = 90
		PlaySound()

	if Input.is_action_just_pressed("move_right"):
		translation.x += speed
		rotation_degrees.y = -90
		PlaySound()


func PlaySound():
	$MovementAudio.pitch_scale = rand_range(0.95, 1.05)
	$MovementAudio.play()
```
#### Test it out!
 Does the Player character move, rotate, and play sound when the player presses input keys?

## Commit & Submit!
- Use GitHub Desktop to commit and push your project's changes.
- Mark this D2L Assignment complete.
- If you have any issues, please use Discord to ask questions and we'll solve it over there!
