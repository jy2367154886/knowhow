# Danger X-ing: Vehicles & Vessels
- Have Vehicles spawn and de-spawn in the Road.
- Have Vessels spawn and de-spawn in the River.
- Reorganize the Road and River classes so that they inherit functionality from Lane.


## Reorganizing Lane
The Road and River have a lot of features in common. They both have modes of conveyance that travel down their Lane at a certain speed limit. So we can eliminate code duplication by **combining the common functionality for both River and Road into the Lane class**. Likewise since Vehicles and Vessels behave in a similar manner we can _*sub-class*_ them under Conveyance.

|||||||||

| Lane   | ⮪    | Conveyance  |        | ⮪      |        |     |           |
| ------ | ----- | ----------  | ------ | ------ | ------ | --- | --------- |
| **Road ** | **River** | Vehicle ⮭   | ⮪      | Vessel | ⮪     | ⮪   | ⮪         |
|        |       | Car ⮭       | Truck  | Boat ⮭  | Turtle | Log | Crocodile |

Each Lane will have a spawning point and a de-spawning point that we'll be able to move around.

### Lane.gd
```gdscript
extends Area
class_name Lane
```


### Road.gd
```gdscript
extends Lane
Road
```

### River.gd
```gdscript

class_name River

```


Conveyance
