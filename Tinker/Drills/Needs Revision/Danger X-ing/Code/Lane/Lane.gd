extends Spatial
class_name Lane

onready var spawn_point := $SpawnPoint
onready var despawn_point := $DespawnPoint
onready var craft_scene : PackedScene = preload("res://Craft/Craft.tscn")

var speed_limit : float = 2.0 # m/s.


func SpawnCraft(packed_scene):
	var craft = packed_scene.instance()
	craft.Initialize(spawn_point.translation, despawn_point.translation, speed_limit)
	add_child(craft)


func _on_Timer_timeout():
	SpawnCraft(craft_scene)
