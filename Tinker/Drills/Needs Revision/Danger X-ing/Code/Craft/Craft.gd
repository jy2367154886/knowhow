extends Spatial
class_name Craft

var speed : float = 2.0 # m/s.
var spawning_point : Vector3
var destroy_radius : float


func _process(delta):
	translation.z += -speed * delta
	if translation.distance_to(spawning_point) > destroy_radius:
		queue_free()


func Initialize(new_spawning_point, despawn_point, speed_limit):
	spawning_point = new_spawning_point
	destroy_radius = spawning_point.distance_to(despawn_point)
	speed = speed_limit
	translation = spawning_point
