extends Area
class_name Player

var speed = 2.0

func _process(delta):
	if Input.is_action_just_pressed("move_forward"):
		translation.z += -speed
		rotation_degrees.y = 0
		PlaySound()

	if Input.is_action_just_pressed("move_back"):
		translation.z += speed
		rotation_degrees.y = 180
		PlaySound()

	if Input.is_action_just_pressed("move_left"):
		translation.x += -speed
		rotation_degrees.y = 90
		PlaySound()

	if Input.is_action_just_pressed("move_right"):
		translation.x += speed
		rotation_degrees.y = -90
		PlaySound()


func PlaySound():
	$MovementAudio.pitch_scale = rand_range(0.95, 1.05)
	$MovementAudio.play()
