extends Craft
class_name Vessel

enum vessel_types {
	boat,
	turtle,
	river_log,
	crocodile,
	toaster
}


func _ready():
	match vessel_type:
		vessel_type.boat:
			$Log.hide()


func Initialize(new_vessel_type):
	vessel_type = new_vessel_type
