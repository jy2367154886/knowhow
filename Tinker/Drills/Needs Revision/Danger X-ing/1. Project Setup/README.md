# Danger X-ing: Project Setup
- Setup your work space to **begin the Danger X-ing** project.
- Review the **Danger X-ing GDD**.
- Block out your first few **Object Scenes** in Godot.

## GitHub Classroom
Accept this GitHub Classroom invite: **DangerXing** - https://classroom.github.com/a/Xxzsfpfa

- This creates a unique online repository with the format "dangerxing-username". Only you and I have access to the content of this repo.
- Refresh the page to get the link to your personal repo.
- Click the green [Code] button and select [Open with GitHub Desktop].

## GitHub Desktop
- GitHub Desktop should open to the clone by URL tab.
- Set your Local path: "C: > CS 233G > DangerXing" (i recommend making a simple class folder like "C:\CS 233G\" to keep all local project repos, but you are in-charge of your own hard drive locations)
- Once cloned, click the [Show in Explorer] button to open the Danger X-ing project folder in your file browser.

## Danger X-ing Game Design Document
Danger X-ing is a clone of the classic arcade game Frogger - https://www.youtube.com/watch?v=l9fO-YuWPSk


We can break down the various parts of the game into basic game object classifications:

- Player
- Lane
- Vehicle
- River
- Vessel
- Goal

[ Insert GDD Pic. ]

## Godot

Open the Danger Crossing project from your file browser at **"DangerXing > Code > project.godot"**. The Main scene should appear, but if it doesn't, look at the [FileBrowser] tab and double-click **"res://Main/Main.tscn"** to open it up. This will be our main *entry point* into our game and its *node tree*.

According to the GDD above we'll need several classes:

## Player
### New Scene / New Folder

Make a new Scene in Godot by clicking the **[+]** icon at the end of the **Tabs Row** in the Viewport. This will create a new Scene Tab. Notice that the scene's name states **[empty]** on the new tab. We'll want to find a place to store the scene in our project folder and save it as soon as possible. However, Godot cannot save the scene until it has **at least one node in the scene's hierarchy**. Looking over to Godot's Scene docker you'll notice 3 default options for the basic nodes *[2D Scene]*, *[3D Scene]*, and *[User Interface]*. For our game we will only be using [3D Scene].

Click the **[3D Scene]** button, then Double click the **Spatial** node and rename it: **Player**. Notice that the scene name changed to `[unsaved](*)` in the viewport tab. Right clicking on the tab opens a dropdown where we can **Save Scene** (`Ctrl` + `S` also works).

Create a folder at **"res://Player/"**, and save the **"Player.tscn"** scene within.

![Save the Player scene.](docs/save_player.png)

### Build up the Player
Build out the Player's node hierarchy with **Area**, **MeshInstance**, and **CollisionShape** nodes. The look of the asset can look like anything, the important part for the game engine is the **collision detection**. We have to use **area type** or **body type** nodes in order to tell Godot to use collision detection. So the first thing we need to do is change the Player node's type from **Spatial** to **Area**.

![Save the Player scene.](docs/change_type.png)

Next we will **build up our 3D graphics with MeshInstance** node primitives; and finish it off with a Capsule **CollisionShape**.

In the examples, we build a Chicken for our Player, but if you are feeling inspired, please build what inspires you! Hamsters?

This time we have placed all our graphics in a Spatial node called **"Chicken"**. Next we add **MeshInstance** nodes to the "Chicken" node then: pick a shape, rotate, scale, and position.

![Save the Player scene.](docs/player_nodes.png)

### Attach Script to Node
Select the **Player** node. Click the small **[Attach Script]** button in the Scene tab. The **Attach Node Script** dialog box should autofill most of the fields. This action will create a new text file named **"Player.gd"** in the same folder as the **"Player.tscn"** scene.

This file will act as the **"brain"** of our object, so that's where we'll be placing all the logic for our Player object.

![Add a "Player.gd" script.](docs/player_script.png)

In the next assignment we'll be adding the Player logic for movement based on player input into the **"Player.gd"** script.

## Perform this same setup for the remaining game objects:

### Vehicle
The graphics of our objects are up to us to imagine. Here we have a Car built with standard Godot MeshInstance nodes, parented to a single Spatial node. What sort of interesting other forms can we create with **MeshInstance** nodes? 

![Vehicle scene nodes.](docs/vehicle_nodes.png)

### Lane
Since the player won't collide with the roadway, we can keep Lane set to the Spatial node type.

![Lane scene nodes.](docs/lane_nodes.png)

### River
The Player will be colliding with the water; so the River object should be an Area node type with a child **CollisionShape** node.

![River scene nodes.](docs/river_nodes.png)

### Vessel
Here we have designed our Vessel like a log. Since it will float in the River, we keep the Vessel centered on the scene origin.

![Vessel scene nodes.](docs/vessel_nodes.png)

### Goal
The Goal here is a nice pleasant roost. A nest of eggs for our brave chicken!

![Goal scene nodes.](docs/goal_nodes.png)

## Commit & Submit!
- Use GitHub Desktop to commit and push your project's changes.
- Mark this D2L Assignment complete.
- If you have any issues, please use Discord to ask questions and we'll solve it over there!
