# Cyclone Cycler: Project Setup
## Objectives
- Review the **Cyclone Cycler GDD**.
- Deconstruct the rules of our game. What are the **game objects**? What are **their functions**?
- Setup the work space to **begin the Cyclone Cycler** project.
- Block out the main **game objects** in Godot by:
  - **Creating a Scene** for each object.
  - **Creating a Script** for each object.
  - **Creating a visual** to see the results.


## Cyclone Cycler GDD
The Cyclone Cycler GDD should include:
- A list of all the pieces needed to play.
- The rules for the game and how the game mechanics work.
- Any endgame conditions.
- The intended player experience.


### Pieces of Cyclone Cycler _(Classes)_:
- Main
  - UI: Hi Score
  - Game Menu
- Level
- Enemies
- Player
  - Controller
  - Middle Mouse Wheel `MMW`
  - `Left Arrow` + `Right Arrow`
  - UI: Score


### Rules
- _In progress..._
- A game menu should handle the pause and quit options.

_(Notice how this format is similar to the way board game instructions are laid-out.)_


## Clone the Project Repo
- **CycloneCycler** - https://classroom.github.com/a/SEUgPH7b.


### GitHub Classroom
The link above should open our default web browser to a **GitHub Classroom** page with an invitation to accept the **Cyclone Cycler** assignment.
- Click the `Accept this assignment` button.
- This creates a unique online repository with the format _**reponame-username**_.
- **Only you and I have access to the content of this repo**.
- Then **refresh the page**.
- After refreshing, **click the link to your repository**. _(example: https://github.com/PushingPolygons/cyclonecycler-username)_


### GitHub.com
- Once on the repo page: **click the green** `Code` button, and select the `Open with GitHub Desktop` option.


### GitHub Desktop
- GitHub Desktop should launch, opening up to the `URL` tab, and auto-fill the **Repository URL**.
- We'll set our **Local path** to our favorite hard drive location game projects.
- Click the `Clone` button.
- Once cloning is complete, we can click the "show" button to navigate to our local project via the OS file browser:
  - Windows: `Show in Explorer` button.
  - Mac: `Show in Finder` button.
  - Linux: `Show in your File Browser` button.


### File Browser
Our operating system should launch its file browser, opening to the repository project folder.
- Open the **"Code"** folder.
- **Double click the "project.godot"** file.



## Godot Game Objects
Splitting each major piece of our game into distinct object classes will allow us to focus on one component at a time.


### Cyclone Cycler Classes _(object classifications)_
- _In progress..._




### Tap `F5`
- `▶` **Play** the game and make sure it works!
- _What should we see?_



## Commit & Push!
When satisfied with the results:
- Use **GitHub Desktop** to _**commit**_ and _**push**_ the project changes.
- If you have **any issues**, use Discord to message me and we'll sort it out together!
