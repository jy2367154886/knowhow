# Bombs Away: 7. Inheritance
One of the cornerstones of OOP is _inheritance_.

## Objectives
- Eliminate code duplication with Explosive > Explosion
- Eliminate code dup with Building < Silo


If the Bombs hit any Silos or Buildings they will explode. Since both Bombs and Missiles explode let's make a new class to help automate our explosions:

Create a Folder, Scene and Script for the Explosion class.
Create a Folder, Scene and Script for the Explosive class.
Extend Bomb and Missile from the Explosive class.
Explosion

First let's make an Explosion scene:

# Explosion.gd

extends Area
class_name Explosion

var max_radius: float = 4.0
var expansion_rate: float = 4.0 # / s.

func SetColor(color):
&nbsp; &nbsp; $MeshInstance.get_active_material(0).emission = color
&nbsp; &nbsp; $MeshInstance.get_active_material(0).albedo_color = color
&nbsp; &nbsp;&nbsp;
func _process(delta):
&nbsp; &nbsp; scale.x += expansion_rate * delta
&nbsp; &nbsp; scale.y += expansion_rate * delta
&nbsp; &nbsp; scale.z += expansion_rate * delta
&nbsp; &nbsp;&nbsp;
&nbsp; &nbsp; if scale.x &gt; max_radius:
&nbsp; &nbsp; &nbsp; &nbsp; queue_free()

Explosive

We are designing Bomb and Missile to be child classes of Explosive so we can store all the logic for our explosions. Explosive will be the super class of Bomb and Missile. this is where we will define the Explode() function.


# Explosive.gd

extends Spatial
class_name Explosive

const EXPLOSION_PS: PackedScene = preload("res://Explosion/Explosion.tscn")

func Explode(color: Color):
&nbsp; &nbsp; var explosion = EXPLOSION_PS.instance()
&nbsp; &nbsp; explosion.SetColor(color)
&nbsp; &nbsp; explosion.translation = translation
&nbsp; &nbsp; get_parent().add_child(explosion)
&nbsp; &nbsp; queue_free()

Bomb &amp; Missile

Now hook up the Bomb and Missile classes by extending the Explosive class. Change the top line in both the "Bomb.gd" and "Missile.gd" scripts.

# Bomb.gd
extends Explosive
# Missile.gd
extends Explosive

Test the results by clicking the Play button (or [F5]) at the top right of Godot. Nothing should have really changed. But there shouldn't be any errors either. :)

Commit &amp; Submit!
Use GitHub Desktop to commit and push&nbsp;your project's changes.
Mark this D2L Assignment complete.
If you have any issues, please use Discord to ask questions and we'll solve it over there!
