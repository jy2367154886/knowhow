# Booting: 0. Communicator

## Objectives

- Use the previously provided invite link to **join the Discord server**: `Pushing Polygons`
- As you are granted new _member roles_, look for new project channels and updates to appear in the channels column.
- **Submit your Discord username.**


## Communication App

Remote game devs require a way to **present works-in-progress (wip) to receive feedback** from other team members. There are many different messaging applications designed to make collaboration easier, some have web browser interfaces, and some are desktop applications. Popular messaging apps for game dev include: _Discord, Slack, MS Teams, Gmail, etc._

**We'll be using Discord.** Discord allows us to organize dev members within _Discord servers_, post work and issues to different _server channels_, paste screenshots, display code, send files back and forth, all in one _mostly-easy-to-use_ interface with cute animated emojis.


## ![](../../.docs/discord.svg) Discord (required)

_**Grab the desktop version from the - [Official Discord website](https://discord.com/).**_  When sorting out more complex issues, it will be easier to solve if we can stream from the desktop and application windows in real-time.

_**Use the previously provided Pushing Polygons invite link**_ to:

1. _**Download, install, and launch**_ the [Discord desktop app](https://discord.com/).
1. _**Login to (or create)**_ a **Discord account**.
1. As you are granted new _member roles_, _**look for new project channels to appear**_ in the Pushing Polygon channels column.
1. Feel free to _**rename your nickname**_ on the server to the name you'd like to be called. This won't change your username anywhere else.

Now you should have a clean line of communication with me, and our fellow contributors of the upcoming projects!

