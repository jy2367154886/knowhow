# Booting: 6. Nav Quest 3D

![The end is the beginning.](.docs/journal_folder.png)


## Objectives

- Use **GitHub Desktop** to clone the "NavQuest3D" repository from **GitHub Classroom**.
- Use Godot's viewport navigation controls to **line-up and take screenshots** of "Main.tscn".
- Try to **recreate the 10 screenshots provided below** as closely as possible.
- Use a file browser to locate the new shots and **move them into a specific repo folder**.


## Clone the Repo

- **NavQuest3D Repo** - https://classroom.github.com/a/4eQuaKDP.


### GitHub Classroom

The link above should open our default web browser to a **GitHub Classroom** page with an invitation to accept the **NavQuest3D** assignment.
- Click the `Accept this assignment` button.
- This creates a unique online repository with the format _**reponame-username**_.
- **Only you and I have access to the content of this repo**.
- **Refresh the page** when it asks us to.
- After refreshing, **click the link to your repository**. _(example: https://github.com/PushingPolygons/navquest3d-username)_


### GitHub.com

- Once on the repo page: **click the green** `Code` button, and select the `Open with GitHub Desktop` option.


### ![](../../.docs/github.png) GitHub Desktop

- GitHub Desktop should launch, opening up to the `URL` tab and auto-fill the **Repository URL**.
- We'll set our **Local path** to our favorite hard drive location game projects.
- Click the `Clone` button.

![Clone the project repository.](.docs/clone_navquest3d.png)


- Once cloning is complete, we can click the "show" button to navigate to our local project via the OS file browser:
  - Windows: `Show in Explorer` button.
  - Mac: `Reveal in Finder` button.
  - Linux: `Show in your File Browser` button.


### ![](../../.docs/folder.svg) File Browser

Our operating system should launch its file browser, opening to the repository project folder.
- Open the **"Code"** folder.
- **Double click the "project.godot"** file.


## ![](../../../.docs/godot.svg) Godot

We'll start by maximizing the `Viewport`, so we can see as much of the scene as possible.

![The Nav Quest 3D scene.](.docs/default_layout.png)
- Click the `Distraction Free Mode` button, located at the top-right corner of the Main Viewport.

![The `Distraction Free Mode` button.](.docs/maximize_button.png)

![Our maximized viewport.](.docs/main_scene.png)


### 3D Scene Navigation

#### Creative mode controls: `RMB` _(right mouse button)_

To fly around the scene like a first-person video game, try holding down the `RMB` _(right mouse button)_ in the `3D` viewport. The mouse cursor will disappear and enter _**mouse-look mode**_.

- **Mouse-look:** `RMB` + drag
- **Fly forward:** `RMB` + `W`
- **Fly back:** `RMB` + `S`
- **Strafe left:** `RMB` + `A`
- **Strafe right:** `RMB` + `D`
- **Fly up:** `RMB` + `E`
- **Fly down:** `RMB` + `Q`
- **Speed up movement:** `RMB` + `MMW` + up
- **Slow down movement:** `RMB` + `MMW` + down


#### Orbiting controls: `MMB` _(middle mouse button)_

##### _(Dev Tip)_ Swap Pan & Orbit Hotkeys

If one wishes for a more balanced feel between the `2D` viewport and `3D` viewport controls, and other user interfaces within Godot: take a quick detour to the `Editor Settings` UI for a more consistent input experience.

- Click the `Editor` menu > `Editor Settings...` > `Editors` > `3D` > `Navigation`
- Change the **Orbit Modifier** to: `Shift`
- Change the **Pan Modifier** to: `None`

![3D Input Settings](.docs/input_settings.png)

Now pan and zoom will behave the same way in both the `2D` and `3D` viewports. Plus the `3D` viewport gains a workflow efficiency. By holding `Shift` to orbit a selection, the key is already pressed to multi-select more viewport objects.

- **Pan the scene:** `MMB` + drag
- **Orbit view:** `Shift` + `MMB` + drag
- **Zoom in:** `MMW` + up
- **Zoom out:** `MMW` + down


## Taking Screenshots

It's a good idea to take daily screenshots to add to **one's dev journal**. Human memory is just not that great; it's nice to have screenshots and a few mementos to look back-on when trying to justify asking the boss for a raise!


### Windows Screenshots

- Press the `Windows` + `Print Screen` buttons on the keyboard to take a picture of the entire screen.
- By default the images will be stored in the user's **"Pictures > Screenshots"** folder.


### Mac Screenshots

- `Shift` + `Command` + `3` will capture a screenshot of the entire screen.
- By default the images will be stored on the **"Desktop"**.


## Let's Play!

### How closely can you match the following 10 screenshots?

![Screenshot 01](.docs/screenshot_01.png)

![Screenshot 02](.docs/screenshot_02.png)

![Screenshot 03](.docs/screenshot_03.png)

![Screenshot 04](.docs/screenshot_04.png)

![Screenshot 05](.docs/screenshot_05.png)

![Screenshot 06](.docs/screenshot_06.png)

![Screenshot 07](.docs/screenshot_07.png)

![Screenshot 08](.docs/screenshot_08.png)

![Screenshot 09](.docs/screenshot_09.png)

![Screenshot 10](.docs/screenshot_10.png)


### ![](../../.docs/folder.svg) File Browser

- Let's **create new a folder called "Journal"** at the top level of our repo. It will sit right next to our repo's main "Code" folder.
- Locate and **move the screenshots into the "Journal" folder**.

!["Journal" folder with screenshots.](.docs/journal_folder.png)


## ![](../../.docs/github.png) Commit & Push!

When satisfied with the results:
- Use **GitHub Desktop** to _**commit**_ and _**push**_ the project changes.
- **Should any issues arise**, please use **Discord** to send a message, and we'll sort it out together!
