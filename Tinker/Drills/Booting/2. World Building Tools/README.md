# Booting: 2. World Building Tools

In order to bcome an effective game dev, one should at least have a passing understanding of the following types of developer tools.


## Objectives

- Download and install our world building tools:
  - _(Required)_ **Godot is required.**
  - _(Recommended)_ The rest are optional, but recommended.


## The Mighty Game Engine

A _game engine_ is an appplication that helps us take all the pieces of the game and put them together. It will be our primary app as we explore how games are bilt. Popular industry games engines include: _Unreal, Unity, GameMaker Studio, and more..._ **We will be using Godot.**


### ![](../../.docs/godot.svg) Godot 4 _(required)_

![The Godot editor.](.docs/godot_interface.png)  

_**Visit the Official Godot Website to click the `Download Latest` button.**_

- The download page should open to the link for your OS _(operating system):_
    - Downloading Godot 4 for Windows - [https://godotengine.org/download/windows/](https://godotengine.org/download/windows/)
    - Downloading Godot 4 for macOS - [https://godotengine.org/download/macos/](https://godotengine.org/download/macos/)
    - Downloading Godot 4 for Linux - [https://godotengine.org/download/linux/](https://godotengine.org/download/linux/)
- On the download page, _**click the button:**_ `Godot Engine | 4.x.x`
- _**Extract the contents**_ of the downloaded **ZIP file** to a new folder on your hard drive.
- There is no application installer.
- There are two main ways to launch Godot from the File Manager:
    - Double-clicking the Godot executable. _(from the earlier extraction folder)_
    - Double-clicking a "project.godot" file.


## Asset Creation Tools _(optional)_

One should train with one's prefered instraments as often as possible. If you are already proficient in other audio or visual asset creation tools, please feel free to use your favorite application for the task at hand. However, the upcomming dev drills are documented using the following 2D, 3D, and audio applications.



### ![](../../.docs/krita.svg) Krita (2D Pixel Paint)

Digital painting programs are plentiful and available for almost any device. Some try to emulate the feel of physical mediums like paint or chalk and some embrace their digital roots. Industry standards include: Photoshop, Painter, Clip Studio Paint, Procreate, etc... **We will be using Krita as our primary pixel pusher.**

![Krita pixel paint.](.docs/krita_interface.png)

- _**Download and install Krita**_ - [https://krita.org/](https://krita.org/)



### ![](../../.docs/blender.svg) Blender (3D Modeling)

There are many 3D modeling applications within different industries. Some are used for achitecture, some for product development, some are for making movies, some are for making games. Examples of popular 3D modeling programs include: _3DSmax, Maya, Revit, Cinema 4D, and more..._ **We will be using Blender!**

Blender is quite feature rich. It aims to cover the everyday tasks of an animation studio. From storyboarding with 2D vectors, to blocking out shapes and sculpting in 3D, then rigging bones, or unwrapping UVs, to texture painting, video editing, rendering, etc.

![Blender is awesome! I can't wait until it's an entire OS!](.docs/blender_interface.png)

- _**Download and install Blender**_ - [https://www.blender.org/](https://www.blender.org/)



### ![](../../.docs/bespoke.png) Bespoke (Node based DAW)

Audio is always last and minimally covered. Today is no exception!



- _**Download and install Bespoke**_ - [https://www.bespokesynth.com/](https://www.bespokesynth.com/)


## Install the Tools

- Just install. (no submission)
- **Should any issues arise**, please use **Discord** to send a message, and we'll sort it out together!
