# Booting: 1. Top 10

## Objectives

- Use **Markdown** formatting to **create a list of your top 10 favorite games** of all time.
- Board, card, and other tabletop games count too!
- Post the list into the #in Discord.
- Why are they your favorite? **TLDR! No more than 3 sentences per game!**


## ![](../../.docs/md.svg) Markdown Language

**Markdown** is a simple way to format one's _text documentation_. Project management websites like _Trello, GitHub, GitLab,_ and messaging applications like _Discord_ all support MD formatting.

The most common markdown file we'll run across is the **"README.md"** file. Markdown files are just regular _text files_ with the **".md"** _file extension_, and can be opened in any text editor. The "README.md" file is a common way to document the contents of one's folder. Git hosting websites _(like GitHub.com or GitLab.com)_ parse ".md" files within our _online git repositories_.

In fact you are reading one now!


### ![](../../.docs/discord.svg) Discord Markdown Cheatsheet

When typing a multi-line message into a Discord post, use: `Shift` + `Enter`

- _**Text Formatting**_
  - Place _italic text_ between two `_` symbols: `_italic text_`
  - Place **bold text** between two `**` symbols: `**bold text**`
  - Place **spoiler text** between two `||` symbols: `||spoiler text||`
- _**Lists**_
  - Each item in a list must start on a **new line**.
  - For a _numbered list:_
    - Place a `1. ` before each list item. _(one, period, space)_
    - The numbers will auto increment.
  - For a _bullet list:_
    - Place a `- ` before each list item. _(dash, space)_
- _**Headings**_
  - Use the `# ` symbol at the start of a **new line** to create a heading.
  - Discord supports 3 heading sizes: `# Heading of size: 1`, `## Heading of size: 2`, `### Heading of size: 3`


In the code example below, I used markdown to format my top 10 list with `1.` at the beginning of each line item.


###### _Discord Markdown Example_

```
# Top 10

1. Best 2D animation, best platformer: **|| Earthworm Jim (1994) ||**
1. Best use of 3D spatial gameplay: **|| Homeworld (1999) ||** _(I read the user manual all the way through while waiting at the doctor office.)_
1. Best RTS: **|| StarCraft (1998) ||**
1. Both **Tower Defense** and **MOBA** genres owe their lineage to the gamer community using the map editor in new and creative ways: **|| Warcraft III (2002) ||**
1. Most addictive: **|| Diablo II (2000) ||** _(4+ years in a row of my life. Ties with **|| Bastion (2011) ||** for best original soundtrack.)_
1. Best FPS game mechanic: **|| Superhot (2016) ||**
1. Most tortuous decision making: **|| Frost Punk (2018) ||** _(Will you play as a military despot, or a religious zealot? It won't matter. Everyone will still hate you when it starts to freeze. Good thing players may press the `space bar` to pause while debating agonizing trade-offs.)_
1. Most frightening with the lights off: **|| Dead Space (2008) ||** _(I can only play about 30 minutes at a time before needing to switch to something less intense.)_
1. Best visual communication via minimalist pixel animation: **|| Zombie Night Terror (2016) ||**
1. Tricking players into learning how CPU instructions work: **|| Human Resource Machine (2015) ||**

## Best user manual fluff:

- [Fallout](http://cdn.akamai.steamstatic.com/steam/apps/38400/manuals/Fallout_manual_English.pdf)
- [Homeworld](http://sierrachest.com/gfx/games/Homeworld/box/manual.pdf)
- [Valve Employee Handbook](http://media.steampowered.com/apps/valve/Valve_Handbook_LowRes.pdf)

```


### Create a Post

- _**Head over to the Discord server:**_ `Pushing Polygons`
  - _**Look for the channel:**_ `#of-all-time`
  - _**Use the**_ `1. ` _**markdown formatting**_ to post a list of your top 10 favorite games of all time.
  - _**Don't worry about making mistakes.**_ you will have full editing privileges to revise any drafts.
- **Should any issues arise**, use **Discord** to send a message, and we'll sort it out together!



### Bonus Objective

- Read the [Valve Employee Handbook](http://media.steampowered.com/apps/valve/Valve_Handbook_LowRes.pdf).