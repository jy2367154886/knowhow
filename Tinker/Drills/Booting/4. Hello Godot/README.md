# Booting: 4. Hello Godot

![The end is the beginning.](.docs/hello_godot.png)


## Objectives

- _**Clone**_ a _**Git repository**_ using the various tools of **GitHub**.
- **Explore the Godot UI** enough to:
  - **Attach a script** to the **Main** game node.
  - Explore simple **GDScript commands**.
  - **Write a script** in Godot that prints the text **"Hello Godot!"** to the in-editor `Output` window.
- Use **GitHub Desktop** to _**commit**_ and _**push**_ the project changes.


Piece of cake, yes? Let's take it step... by step...


## Clone the Project Repo

- **HelloGodot Repo** - https://classroom.github.com/a/-Ifw5QYx.


### GitHub Classroom

The link above should open our default web browser to a **GitHub Classroom** page with an invitation to accept the **HelloGodot** assignment.

- Click the `Accept this assignment` button.
- This creates a unique online repository with the format _**reponame-username**_.
- **Only you and I have access to the content of this repo**.
- Then **refresh the page**.
- After refreshing, **click the link to your repository**. _(example: https://github.com/PushingPolygons/spacerocks-username)_


### GitHub.com

- Once on the repo page:
  - **Click the green `Code`** button.
  - Then **select the `Open with GitHub Desktop`** option.

![The green `Code` button.](.docs/code_button.png)


### ![](../../.docs/github.png) GitHub Desktop

- GitHub Desktop should launch, opening up to the `URL` tab, and auto-fill the **Repository URL**.
- We'll set our **Local path** to our favorite hard drive location game projects.
- **Click the `Clone`** button.

![Clone the project repository.](.docs/clone_hellogodot.png)


- Once cloning is complete, we can click the "show" button to navigate to our local project via the OS file manager:
  - _**Windows button:**_ `Show in Explorer`
  - _**Mac button:**_ `Show / Reveal in Finder`
  - _**Linux button:**_ `Show in your File Manager`

![GitHub Desktop](../.docs/github_desktop.png)


### ![](../../.docs/folder.svg) File Manager

Our operating system should launch its default _file manager_, opening to the project repository folder.

  - _**Open the folder:**_ "Code"
  - _**Launch Godot, double-click:**_ "project.godot"


![Double-click: "Code/project.godot".](.docs/project_godot.png)



## ![](../../.docs/godot.svg) Godot

![The main Godot interface.](.docs/main_interface.png)
The interfaces for game engines can seem daunting if you've never experienced one before. We'll be taking a look at each of these areas in more depth in future assignments, but for now let's just try to get oriented.


#### Common Areas _(yellow)_

- The `Viewport` > `2D` button displays the **2D engine** viewport.
- The `Viewport` > `3D` button displays the **3D engine** viewport.
- The `Viewport` > `Script` button displays the **text of the selected script** in its `Scripts List`.
- The `Scene` tab displays the scene's **node tree**.
- The `Inspector` tab displays the properties of the currently selected node(s).


#### Important Areas _(red)_

- The `FileSystem` tab shows our **project files and assets**; stored on our hard drive; ready to load.
- The `Node` > `Signals` tab displays the current node's **list of _Signals_ to hook up with code**.
- The `Output` window shows a lot of useful information **when debugging**.
- The `Project` > `Project Settings...` contains a ton of **parameters that affect how the game window operates**.
- Hitting the `▶` button **_compiles_ and _launches_ our game** in a new window called: `Remote (DEBUG)`.
- The editor's `▶`, `⏸`, `⏹` buttons controls the runtime of the _Remote (DEBUG)_ window.


### Opening the "Main" Scene

Godot should open the **"Main"** scene by default, but if it doesn't, we can always open it on our own:

- Let's navigate to the `FileSystem` tab.
- **Double click "res://Main/Main.tscn"** to open our `Main` viewport tab.

![`FileSystem` tab.](.docs/filesystem_tab.png)

- The ".tscn" stands for **text-scene**.
- In the `Scene` tree tab we should now see a **Node named "Main"**.
- The Main scene is incredibly basic; it simply exists. However it _is_ a functional app!


### Playing the Game

- Hit the `▶` **Play** button (or tap `F5`) to launch the game in debug mode.
- If we get the **Please Confirm...** dialog box, choose the `Select Current` button.

![Please confirm...](.docs/no_main_scene.png)

![Launched!](.docs/boring_game.png)

- It's boring. It doesn't really do anything. But it launches and acts like a window!
- To stop playing the game:
  - Hit the `⏹` **Stop** button.
  - Tap `F8`.
  - Close the Remote window.


### Adding Code

Anytime we want to **do** something in a game, we have to code it in a _**script**_. A script is a plane-text file with instructions written in a programming language. The main language Godot uses is called **GDScript**. GDScript was designed to be a simple, and legible, language that game devs could use without a ton of setup. Let's make a script!


- To create a script, let's head over to the `Scene` tab, and **select the "Main" node**.
- Then click the `Attach New Script` button to create a new script file.

![Attach new script.](.docs/attach_new_script.png)


- This will open the `Attach Node Script` dialog box.
- It should auto-fill the script's name **"Main.gd"** in the Path: field. The auto-fill is based on the name of the node selected.
- Click the `Create` button.

![Create new script.](.docs/create_script.png)


### Scripting

- Open the **"Main.gd"** script by double clicking it in the `FileSystem` tab.
- Replace the body of the "Main.gd" script with the following code:


#### Main.gd

```gdscript
extends Node

func _ready():
    print("Hello Godot!")
```


### Tap `F5`

- `▶` **Play** the game and make sure it works!
- Sure, it's still boring... but at least we've said "hello".

!["Hello Godot!"](.docs/hello_godot.png)


## ![](../../.docs/github.png) Commit & Push!

When satisfied with the results:
- Use **GitHub Desktop** to `commit` and `push` your project changes.
- **Should any issues arise**, please use **Discord** to send a message, and we'll sort it out together!

### Have fun!
