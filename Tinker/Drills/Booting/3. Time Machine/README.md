# Booting: 3. Time Machine


## Objectives

- Create a GitHub Account.
- Direct message me on Discord with your GitHub Account username.
- _(Recommended)_ New devs should: download, install, and login to: `GitHub Desktop`


## Version Control

A **version control** system is a way to **track changes made to our project files** as we work.

- Pros: By using a **version control** system we have a versatile way to **save** files, **track** changes, **restore** older versions, and **sync files** as we **collaborate** on a project.
- Cons: It can be complicated to set up, and has a higher learning curve for beginner users.

When onboarding at new dev studio, game jam team, or other collaborative project, it's common practice to just **give new recruits a weblink to the _online project repository_**. The new member can use the application they prefer to perform _repository commands_ as they participate at different times during the course of the projects lifespan.


### What's a repository?

A _**repository**_ is a **log** of all the **files** in our project, their **folder structure**, and the **history** of how the files change over time.

### ![](../../.docs/git.svg) Git

**Git** is an open source _version control system_ that will helps us manage large quantities of digital folders and files from a single _repository._ For those more curious about the origins of Git: [https://git-scm.com/ - Official Git Website](https://git-scm.com/).


## ![](../../.docs/github_services.svg) GitHub
 
 
**GitHub** is a suite of tools and services that help us control our _Git project repos_. For completeness, here is the list of GitHub services we'll be using _(but if this is your first time, this is super unimportant and you probably won't notice the different systems anyway):_

- **GitHub**: a company acquired by Microsoft in 2018.
- **GitHub.com**: a website where we `clone` projects from. It stores our online version, or **origin**, of our project.
- **GitHub Desktop**: a desktop application that devs use daily to `fetch`, `pull`, `commit`, and `push` project changes.
- **GitHub Classroom**: an auxiliary site we use to organize student repo links.

_**Just click the links as they come up!**_



#### ![](../../.docs/github.png) GitHub Username _(required)_

**We need a GitHub.com username** in order for the _repository invitations_ to work properly within all the systems above. It might be a pain to make another account, but it's super common these days and developers should get used to working with version control. _For example: the last 4 game project that I've been paid to work on used GitHub daily to sync repositories._ 



- _**Login to (or create) a GitHub Account at**_ - [https://github.com/](https://github.com/)




#### ![](../../.docs/github.png) GitHub Desktop _(recomended)_

If this is your first time using a version control system:

- _**Download and install GitHub Desktop**_ - [https://desktop.github.com/](https://desktop.github.com/)
- _**Launch GitHub Desktop.**_ It should guide us through the GitHub Account login _(or creation)_ process.


### Repo Time

A repository is a bit like a time machine for our project files. It allows us to:

- Save our progress we _commit_ to our project a bit like quick saves in-game.
- _Revert_ to a previous project state.
- Undo a specific change at a speific point in time while squashing bugs.
- We can split our project timeline into alternate dimenssions when we are trying something dangerous, encapsulating major disasters away from the root branch.
- Manage complex communications when no one is in the room.


### Git Repo Vocab

- **Git client:** a computer application that can perform `git` commands. We'll be using **GitHub Desktop**, but there is no one stopping us from using alternatives like: _SourceTree_, _GitKraken_, or running `git` commands directly from the _Terminal_.
- **Repository:** a project folder that Git tracks.
- **Common commands:**
  - `clone`: download a copy of all the project folders & files. Once cloned we have a new folder on our hard drive that we can edit _locally,_ without fear of messing up the _origin_ project files.
  - `pull`: sync our _local project files_ with the _online project files_.
  - `commit`: log our _local changes_ to our _local repository_.
  - `push`: upload our changes to the _online repository_.


### Direct Message Submission

- _**Head over to the Pushing Polygons Discord server and send me a DM (direct message) that includes your:**_ `Github username`.
- If **any issues** arise, use Discord to send a message and we'll sort it out together!


