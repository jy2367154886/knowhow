# Booting: 5. Nav Quest 2D

![The end is the beginning.](.docs/journal_folder.png)


## Objectives

- Use **GitHub Desktop** to clone the "NavQuest2D" repository from **GitHub Classroom**.
- Use Godot's viewport navigation controls to **line-up and take screenshots** of "Main.tscn".
- Try to **recreate the 10 screenshots provided below** as closely as possible.
- Use a file browser to locate the new shots and **move them into a specific repo folder**.


## Clone the Repo

- **NavQuest2D Repo** - https://classroom.github.com/a/gbOpvuO-.


### GitHub Classroom

The link above should open our default web browser to a **GitHub Classroom** page with an invitation to accept the **NavQuest2D** assignment.

- Click the `Accept this assignment` button.
- This creates a unique online repository with the format _**reponame-username**_.
- **Only you and I have access to the content of this repo**.
- **Refresh the page** when it wants us to.
- After refreshing, **click the link to your repository**. _(example: https://github.com/PushingPolygons/navquest2d-username)_


### GitHub.com

- Once on the repo page: **click the green** `Code` button, and select the `Open with GitHub Desktop` option.


### ![](../../.docs/github.png) GitHub Desktop

- GitHub Desktop should launch, opening up to the `URL` tab and auto-fill the **Repository URL**.
- We'll set our **Local path** to our favorite hard drive location game projects.
- Click the `Clone` button.

![Clone the project repository.](.docs/clone_navquest2d.png)


- Once cloning is complete, we can click the "show" button to navigate to our local project via the OS file browser:
  - Windows: `Show in Explorer` button.
  - Mac: `Reveal in Finder` button.
  - Linux: `Show in your File Browser` button.


### ![](../../.docs/folder.svg) File Manager

Our operating system should launch its file browser, opening to the repository project folder.
- Open the **"Code"** folder.
- **Double click the "project.godot"** file.


## ![](../../.docs/godot.svg) Godot

We'll start by maximizing the `Viewport`, so we can see as much of the scene as possible.

![The Nav Quest 2D scene.](.docs/default_layout.png)
- Click the `Distraction Free Mode` button, located at the top-right corner of the Main Viewport.

![The `Distraction Free Mode` button.](.docs/maximize_button.png)

![Our maximized viewport.](.docs/main_scene.png)


### 2D Scene Navigation

Navigate around the scene within the `2D` viewport.


#### Controls: `MMB` _(middle mouse button)_ & `MMW` _(middle mouse wheel)_

- **Pan the scene:** `MMB` + drag
- **Zoom in:** `MMW` + up
- **Zoom out:** `MMW` + down


## Taking Screenshots

It's a good idea to take daily screenshots to add to **one's dev journal**. Human memory is just not that great; it's nice to have screenshots and a few mementos to look back-on when trying to justify asking the boss for a raise!


### Windows Screenshots

- Press the `Windows` + `Print Screen` buttons on the keyboard to take a picture of the entire screen.
- By default the images will be stored in the user's **"Pictures > Screenshots"** folder.


### Mac Screenshots

- `Shift` + `Command` + `3` will capture a screenshot of the entire screen.
- By default the images will be stored on the **"Desktop"**.


## Let's Play!

### How closely can you match the following 10 screenshots?

![Screenshot 01](.docs/screenshot_01.png)

![Screenshot 02](.docs/screenshot_02.png)

![Screenshot 03](.docs/screenshot_03.png)

![Screenshot 04](.docs/screenshot_04.png)

![Screenshot 05](.docs/screenshot_05.png)

![Screenshot 06](.docs/screenshot_06.png)

![Screenshot 07](.docs/screenshot_07.png)

![Screenshot 08](.docs/screenshot_08.png)

![Screenshot 09](.docs/screenshot_09.png)

![Screenshot 10](.docs/screenshot_10.png)


### ![](../../.docs/folder.svg) File Browser

- Let's **create new a folder called "Journal"** at the top level of our repo. It will sit right next to our repo's main "Code" folder.
- Locate and **move the screenshots into the "Journal" folder**.


!["Journal" folder with screenshots.](.docs/journal_folder.png)


## ![](../../.docs/github.png) Commit & Push!

When satisfied with the results:

- Use **GitHub Desktop** to _**commit**_ and _**push**_ the project changes.
- **Should any issues arise**, please use **Discord** to send a message, and we'll sort it out together!
