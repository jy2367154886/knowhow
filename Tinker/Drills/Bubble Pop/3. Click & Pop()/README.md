# Bubble Pop: 3. Click & Pop()

![The end is the beginning.](.docs/end.png)


## Objectives

- _**Give the `Bubble` class the ability to**_ `Pop()`, thus _freeing_ itself from game memory.
- Use `CollisionShape2D` nodes to **_build out the "Bubble.tscn" scene,_ so** `Bubble` **instances may be clicked** by the player.
- Add an `AudioStreamPlayer2D` node to the `Main` scene, and _**load it up with a sound effect**_.
- When popped, _**play the audio.**_



## ![](../../.docs/github.png) GitHub Desktop
### Perform the Opening Dev Ritual

Once again, we'll make sure we _**stay informed and content synced,**_ so we never lose more than one day's worth of progress!

Open **GitHub Desktop** and check the **3 main tool buttons** across the top:

1. Is the `Current repository` button **set to our "spacerocks-username" project?**
1. Is the `Current branch` button **set to "main"?**
1. Click the `Fetch origin` button for good luck!
1. _**Click the:**_ `"show"` **button** to navigate to our _local project repo_ via the OS file manager:
   - _**Windows button:**_ `Show in Explorer`
   - _**Mac button:**_ `Show / Reveal in Finder`
   - _**Linux button:**_ `Show in your File Manager`


![Open the project repository with GitHub Desktop.](../.docs/opening_dev_ritual.png)


## ![](../../.docs/folder.svg) File Manager

Our operating system should launch its default _file manager_, opening to the project repository folder.

- _**Open the folder:**_ "Code"
- _**Launch Godot, double-click:**_ "project.godot"


![Double-click: "project.godot"](.docs/project_godot.png)


## ![](../../.docs/godot.svg) Godot

### Tap `F5`

- `▶` **Play** the game and _**take note of the current game state**_.


### Do we notice any Issues?

_**Let's make a quick list of issues**_ we can find, and possible solutions.

- We are **spawning a bunch of bubbles** into the game, but we **never delete them afterwards!**
- **We can't click** the `Bubble` instances yet.
- We have visuals, but **no sound effects** to accompany them.

#### Dev Plan:

- _**Create a**_ `Pop()` _**function to handle all these tasks.**_


## ![](../../.docs/godot.svg) The `Bubble` Scene: "Bubble.tscn"

### Memory Leaks

When we want to spawn a `Bubble` instance to our in-game scene, we **ask the computer for enough memory** to do what we want. If all we do is keep asking the computer to **allocate more memory without giving any back**, the computer will eventually run out of memory, resulting in a some sort of crash.

Godot provides us with a _built-in_ function called **`queue_free()`** to _**deallocate**_ the bubble's memory; _**freeing up the memory for future use.**_


### The `Pop()` Function

We'll begin scripting within the `Bubble` class, by _declaring_ the `Pop()` function. This will give us a location within our script to mess around with different tasks, like adding animation, sound effects, or to _**free the bubble's memory!**_

- _**Open the script: "Bubble.gd"**_

  - First, we'll _declare_ the `Pop()` function with the line: `func Pop():`
    - For now, all `Pop()` will do is _call_ `queue_free()`, thus **freeing the Bubble instance from memory**.
  - Next, within the _code block_ of the function: `_process(delta):`
    - We'll use the `Bubble.position.y` to determine **if the bubble moves upward beyond the top of the gaming window**.
    - Then we'll _call_ the `Pop()` function.


###### _"Bubble.gd"_

```gdscript
extends Node2D
class_name Bubble

var speed: float = 100.0

func _process(delta):
    position.y -= speed * delta
    if position.y < 0:
        Pop()

func Pop():
    queue_free()
```


### Tap `F5`

- _**Click the "Play" button:**_ `▶`
- The bubbles should now `Pop()` when they reach the top of the viewport.


## `Bubble` Clicking
The `Bubble` scene already contains a `Sprite2D` node called "Sprite", allowing us to see it. Now we need to use the `CollisionShape2D` + `Area2D` node combo to set up our `Bubble` _*before we can click it with our mouse cursor.*_


### How does the game know where the player can click?
#### Colliders!

- From the `Scene` tree:
  - _**Select the root node:**_  `Bubble`
  - _**Add a child node:**_ `CollisionShape2D`
- Godot presents us with a few warnings, indicating that **we aren't quite using the `CollisionShape2D` node correctly yet.**

![Multiple `CollisionShape2D` node warnings.](.docs/collision_warnings.png)


- Within the `Scene` tree:
  - _**Select the node:**_ `CollisionShape2D`
  - _**Click the option:**_ `Inspector` > `Shape` > `New CircleShape2D`

![Select the `New CircleShape2D` option.](.docs/new_circle_shape.png)


- Let's _**head to the**_ `2D` viewport and:
  - _**Use the small red and white handles**_ of the `CollisionShape2D` to _**scale the radius**_ of the collision area shape _**to fit the desired clickable area**_.

![Drag the small red handles of the `CollisionShape2D` node.](.docs/small_red_handles.png)


- There is still a warning on our `CollisionShape2D` that is basically complaining that **we aren't using the correct _root node_ type** for the new collider to work properly.

![Another `CollisionShape2D` node warning.](.docs/collision_warning.png)


### Changing the _root node_ type from a `Node2D`, to  an `Area2D`.

- From the `Scene` tree dock:
  - _**Select the root node:**_ `Bubble`
  - _**Open the context menu:**_ `Bubble` + `RMB`
    - _**Select the option:**_ `Change Type`

![Select the `Change Type` option from the node's context menu.](.docs/change_type_to_area2d.png)

- Within the `Change Type of "Bubble"` dialog box:
  - _**Search:**_ "Area2D"
  - _**Click the button:**_ `Change`

![`Change Type` popup.](.docs/change_type_popup.png)


- After changing the root node type to `Area2D` all the warnings should be resolved in the `Scene` tab.

![Final collision shape setup.](.docs/scaled_collision_shape_2d.png)



### How do we get the `Bubble` to recognize player clicks?
#### Signals!

The `Area2D` node can _**emit event signals**_ that are useful for the most common actions that nodes perform. Player input can be _**handled by the signal:**_ `Area2D.input_event`

With the `input_event` signal, we'll be able to _**detect the player's mouse input**_ when the cursor is within the bounds of any `CollisionShape2D` node that is a child of our `Area2D` node.

- To gain access to the powers that `Area2D` affords, we must first _**update the first line in the "Bubble.gd" script to read:**_ `extends Area2D`


### Scripting the `OnClicked()` function.

Since the player is able to trigger **multiple inputs** from keyboard, mouse, gamepad, or microphone, _**we'll need to narrow down what sort of event triggered the signal**_. We can accomplish this with some **_nested_ `if` statements!**


#### What sort of _event_ are we waiting for? A mouse button event.

- `if event is InputEventMouseButton:`
_(did the event that `Bubble` instance received, come from a mouse button?)_
- `if event.button_index == MOUSE_BUTTON_LEFT:`
_(is that button the left one?)_
-  `if event.pressed:` _(is it pressed down?)_
- `Pop()` _(we "heard" a `LMB` + click down event!)_

Let's add this logic inside of two new functions: `_ready()` and `OnClicked()`


###### _"Bubble.gd"_

```gdscript
extends Area2D
class_name Bubble

func _ready():
    input_event.connect(OnClicked)

func OnClicked(_viewport, event, _shape):
    if event is InputEventMouseButton:
        if event.button_index == MOUSE_BUTTON_LEFT:
            if event.pressed:
                Pop()
                print("Player clicked down.")
            else:
                print("Player clicked up.")
```


## ![](../../.docs/godot.svg) The `Main` Scene: "Main.tscn"


### Delete the Bubble.

Before adding the sound effects, _**let's delete the one Bubble instance**_ we drug to the "Main.tscn" earlier. **That way all of our Bubbles will be spawning programmatically!**

![Delete the Bubble node from the `Scene` tree.](.docs/delete_bubble.png)


### Adding SFX

- In the `Scene` tree:
  - _**Select the root node:**_ `Main`
  - _**Add a child node of type:**_ `AudioStreamPlayer2D`
  - _**Rename the node from:**_ "AudioStreamPlayer2D" to "Audio"

![The `Create New Node` dialog box: new `AudioStreamPlayer2D`.](../../.docs/create_new_node/audio_stream_player_2d.png)


- From the `FileSystem` dock:
  - _**Open the folder:**_ `Sfx`
  - _**Drag any ".wav" audio sample to the:**_ `Inspector` > `AudioStreamPlayer2D` > `Stream`
  - To reveal the _resource properties_, _**click on the resource preview window.**_ _(just to the left of the `Stream` parameter's drop-down selector.)_
  - _**Listen to the sound by clicking the the "Play" button:**_ `▶️`

![Drag the sfx file from the `FileSystem` dock to the `Stream` parameter field.](.docs/pop_sfx.png)


### Coding the Audio

To have the audio play with our scripts, _**we need to add a couple lines to both the "Main.gd" and "Bubble.gd" scripts:**_


###### _"Main.gd"_

```gdscript
@onready var audio = $Audio

func OnTimedOut():
    var rando: int = randi_range(0, get_viewport().size.x)
    var bubble_node = BUBBLE.instantiate()
    bubble_node.audio = audio
    bubble_node.position.x = rando
    bubble_node.position.y = get_viewport().size.y
    add_child(bubble_node)
```


###### _"Bubble.gd"_

```gdscript
var audio: AudioStreamPlayer2D

func Pop():
    audio.position = position
    audio.play()
    queue_free()
```


### Tap `F5`

- `▶` **Play** the game and make sure it works!
- Does the sound effect play when a bubble is popped?

![Click & Pop()!](.docs/end.png)


## ![](../../.docs/github.png) Commit & Push!

When satisfied with the results:

- Use **GitHub Desktop** to _**commit**_ and _**push**_ the project changes.
- **Should any issues arise**, please use **Discord** to send a message, and we'll sort it out together!


### Concluded in Part 4

[Bubble Pop: 4. Score + More](https://gitlab.com/kirkja-leikjahonnunar/knowhow/-/blob/main/Tinker/Drills/Bubble%20Pop/4.%20Score%20+%20More/README.md)
