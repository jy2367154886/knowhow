# Bubble Pop: 1. Project Setup

![The end is the beginning.](.docs/end.png)


## Objectives

- Setup the work space to **begin the Bubble Pop** project.
- Block out our first _**object class**_ in Godot.
- Create an _**instance**_ of the `Bubble` class in the `2D` viewport.
- **Add a floating behavior** to the **"Bubble.gd"** script.


## Clone the Project Repo

- **BubblePop Repo - https://classroom.github.com/a/k1ruZKHR**.


### GitHub Classroom

The link above should open our default web browser to a **GitHub Classroom** page with an invitation to accept the **Bubble Pop** assignment.

- Click the `Accept this assignment` button.
- **Refresh the page!**
- After the refresh, **click the link to your repository:**
  - _example link:_ `https://github.com/PushingPolygons/bubblepop-username`


### GitHub.com

- GitHub created a unique online repository with the _naming convention_: _**reponame-username**_.
- The link above should **deposit us on our GitHub.com** repo webpage; displaying our **newly constructed project repository**.
- If we look to the top of the page, we should see that **the repository is marked `Private`**
- **Only the two of us have access to the content of this repo**.
- From our repo page: **click the green** `Code` button, and select the `Open with GitHub Desktop` option.


### ![](../../.docs/github.png) GitHub Desktop

- **GitHub Desktop** should launch; opening the dialog box: `Clone a repository` > **`URL`**
  - _**Repository URL:**_ should auto-fill with our repo link.
  _(example):_ `https://github.com/PushingPolygons/bubblepop-username.git`
  - _**Local path:**_
  _(example):_ `~/games/bubblepop-username`
  - Pick a favorite **hard drive location** for game dev projects:
    - Or **create a new folder location** using the button: `Choose...`
  - Click the `Clone` button.

![Clone the project repository.](.docs/clone_bubblepop.png)
  - Once cloning is complete, we can **click the "show" button to navigate to our local project** via the OS file manager:
    - _**Windows button:**_ `Show in Explorer`
    - _**Mac button:**_ `Show / Reveal in Finder`
    - _**Linux button:**_ `Show in your File Manager`


### ![](../../.docs/folder.svg) File Manager

- Our operating system should launch its default _file manager_, opening to the project repository folder.
  - Open the **"Code"** folder.
  - _**Double-click:**_ "project.godot"

![Double-click: "project.godot"](.docs/project_godot.png)


> ## Object Oriented Design (OOD)
> There is a style of programming that works very well with games called **object oriented programming (OOP)**. The object oriented flow centralizes the concept of:
> - **Plans & Constructs.**
>
> Picture a vehicle designer who _**drafts up plans**_ for a new tricycle. Its form-factor is all curvy, and swooshy, and awesome! Those _**plans**_ then are sent to a manufacturer to _**construct**_ the trikes. Every trike the factory pumps out is a _**constructed instance**_ built from those original _**blueprints**_.
>
> ### Objects & Instances
> The games industry uses multiple terms for the **Plans & Constructs** model of organization. The most generic and common terms are:
> - Object & Instance.
>
> #### A **plan** may be called:
> - **object** (generic design)
> - **class** (any OOP capable language)
> - **scene** (Godot)(scene + script)
> - **blueprint** (Unreal)
> - **prefab** (Unity)
> - **symbol** (Adobe)
> - **lawform** (game design / board game design)
>
>
> #### A **construct** made from a plan may be called:
> - **instance** (C++, C#, python, Godot, Unreal, Unity, Adobe)
> - **Node** (Godot uses Scenes to generate Node instances.)
> - **Actor** (Unreal)
> - **gameObject** (Unity)


## ![](../../.docs/godot.svg) Godot Game Scenes: ".tscn"

**The Scene is main file format** that Godot uses to maintain its game data. The Scene file is a **_plain-text file_ with the extension ".tscn"** _(meaning Text Scene)_. Scene files are stored in the `FileSystem` dock **"res://" folder** _(resource folder)_. Scenes are where _game designers_, _level editors_, and _programmers_ congregate; bringing all the pieces together into a game project that might actually run... or may even be fun!


To open any existing Scene in a viewport tab, _**double-click:**_
- `File System` > `*.tscn`
- Double-clicking _any file_ with a **".tscn" extention**, will open _(or switch to)_ the **viewport tab** for that Scene.
- Use the `2D`, `3D`, and `Script` buttons to view the Scene in different ways.


## Nodes

**Nodes are the basic building blocks** that Godot uses to understand developer intentions. All Scenes in Godot are constructed from _**a hierarchy of nodes**_ that are displayed and may be manipulated from the: **`Scene` tree**

- Each Scene is required to contain _**at least one node**_ from which to build: **the _root node_**.
- The first node in the `Scene` tree hierarchy is the: **root node**
- Nodes are **able to be parented to one another to form complex _node trees_**.
- When our game runs, Godot uses **our node tree hierarchies to create the:** _**Game Tree**_.
- Game objects are **_spawned_ by attaching node trees to the Game Tree**.
- Game objects are **_despawned_ by detaching nodes from the Game Tree**.


### Parenting

By dragging the position of nodes around in the node tree, we have the ability to create complex _parent_ and _child_ connections between nodes. We will begin by exploring the following nodes:

- `Node`
- `Node2D`
- `MeshInstance2D`
- `Area2D`
- `CollisionShape2D`


## Our Object Classes

By splitting each _**major component**_ of our _**game system**_ into distinct _**object classes**_ _(object classifications)_ will allow us to **focus on one component at a time**.

 According to our GDD we must **construct the following classes:**

- `Main` _(gameplay)_
- `Bubble` _(game object)_
- `UI` _(reacts to gameplay)_


> ### OOP Organizing Standards
>
> In object oriented programming, it's good practice to **preserve identical naming** between the various files that make up our object. The most basic, yet functional, Scene in Godot would consists of **its root node** and **attached script**. Luckily Godot handles most of the naming conventions for us!
>
> We will be using the following the **naming convention**:
>
> - Scene: **"Bubble.tscn"**
>   - Root node: **`Bubble`**
>     - Attached script: **"Bubble.gd"**



## New Class: `Bubble`

Since the `Bubble` object is the main focal point of our game, we'll start by **defining _what a Bubble is!_** Our goal will be to **combine assets and logic into a single form** that the game engine will understand how to use.

We do this by organizing a _**new class Folder and new class Scene:**_


### New Folder...

- From the `FileSystem` dock, _**open the context menu:**_ `res://` + `RMB` > `New` > `Folder...`

![`New Scene...`](.docs/filesystem_new_folder.png)


- Within the `Create Folder` dialog box:
  - _**Name:**_ "Bubble"
  - _**Click:**_ `OK`


![Create Folder dialog box.](.docs/create_folder.png)


### New Scene...

- From the `FileSystem` dock _**open the context menu:**_ `res://` > `Bubble` + `RMB` > `New` > `Scene...`

![`RMB` > `New Folder...`](.docs/filesystem_new_scene.png)


- Within the `Create New Scene` dialog box:
  - _**Root Type:**_ `2D Scene`
  - _**Scene Name:**_ "Bubble" `.tscn`
  - _**Click:**_ `OK`


![Create a new scene called "Bubble".](.docs/create_new_scene.png)


### Graphics...

Let's pull some _visual assets_ into our `2D` viewport, so we can start **testing our game space**.

- From the `FileSystem` dock, _**drag:**_ `"res://` > `Textures` > `bubble.png` into the `2D` viewport.
This should create a new `Sprite2D` node **named "Bubble" in the** `Scene` **tree**.
- To avoid confusion, let's **rename the new node from "Bubble" to "Sprite"**.
- To center our `Sprite` graphics around the Scene's X & Y origin, _**reset the positional transforms:**_
  - `Sprite` > `Inspector` > `Node 2D` > `Transform` > `Position`
    - _**X:**_ `0.0`
    - _**Y:**_ `0.0`

![Rename the node to "Sprite".](.docs/bubble_sprite.png)


### Bubble Brains...

The _behavior_ of the `Bubble` class is dictated by its script. **Let's attach one to the `Bubble` _root node!_** This file will act as the "brain" of our `Bubble` object, making it an excellent place to store all the logic any `Bubble` _instance_ needs.



- From the `Scene` dock, _**open the context menu:**_ `Bubble` + `RMB` > `Attach Script`

![`Bubble` + `RMB` > "Attach Script".](.docs/bubble_attach_script.png)



- Within the `Attach Node Script` dialog box, **leave the auto-filled defaults for:**
  - _**Path:**_ "res://Bubble/**Bubble**.gd"
  - _**Click:**_ `Create`

![Path: "res://Bubble/Bubble.gd".](.docs/attach_node_script.png)


### Movement...

The `Bubble` doesn't do much, but **rise up the game screen**, and **pop**. Each Node2D has a `position` property that we can use to place the Bubble instance anywhere on the screen. We can reposition our `Bubble` instances upwards a little bit each frame, making it rise up the the game screen.


###### _"Bubble.gd"_
```gdscript
extends Node2D
class_name Bubble

var speed: float = 100.0

func _process(delta):
    position.y -= speed * delta
```

![`Bubble._process()` float code.](.docs/float_code.png)


## Add a `Bubble` instance to the `Main` scene.

- To open the `Main` Scene from the `FileSystem` dock, _**double-click:**_ `res://` > `Main` > `Main.tscn`
  - To **add a new** `Bubble` instance to the `Main` scene:
    - _**drag the Scene:**_ `res://` > `Bubble` > `Bubble.tscn` into the `2D` viewport.
    - When we drop `Bubble.tscn` onto the game stage, we have told Godot to **spawn an instance of the `Bubble` class**, within the `2D` viewport, at the location we dropped it.
    - _**Drop:**_ `Bubble.tscn` **inside of the Scene's blue rectangle**.
    The blue rectangle displays the **border our (DEBUG) game window**.
  - Notice that the `Scene` dock now contains a new node called "Bubble".
  - If we drag and drop another `Bubble.tscn`  onto the stage, the **node names will automatically increment**: "Bubble2", "Bubble3", etc.

![Adding a `Bubble` instance to the `Main` scene.](.docs/add_bubble_to_main.png)

### Play the Project: `F5`

Click the `▶` **play** button and make sure it works! What are our results? It's good practice to **test the results of our project as often as possible**. It will help us detect bugs we accidentally introduce. If we clean them up as we go, we will have a more stable code base.

- To run the project either:
  - _**Click the Play button:**_ `▶`
  -or _**tap the key:**_ `F5`
- We should see a bubble _(or bubbles, if one were overzealous with their dragging and dropping)_ climbing the screen at a rate of **100 pixels / second**, then disappear off the top of the _**(DEBUG) game window**_.

![Single bubble instance floating upwards.](.docs/end.png)


## ![](../../.docs/github.png) Commit & Push!

When satisfied with the results:

- Use **GitHub Desktop** to _**commit**_ and _**push**_ the project changes.
- **Should any issues arise**, please use **Discord** to send a message, and we'll sort it out together!


### Continued in Part 2

[Bubble Pop: 2. Tons o' Suds](https://gitlab.com/kirkja-leikjahonnunar/knowhow/-/blob/main/Tinker/Drills/Bubble%20Pop/2.%20Tons%20o'%20Suds/README.md)
