# Bubble Pop: 2. Tons o' Suds

![The end is the beginning.](.docs/end.png)


## Objectives

- **Add a** `Timer` **node** to the `Main` gameplay scene.
- **Spawn a new** `Bubble` instance **every 0.3 seconds**.
- Spawn random `Bubble` instances **along the bottom of the game window based on the window's width**.
- Despawn `Bubble` instances **once they reach the top of the game window**.



## ![](../../.docs/github.png) GitHub Desktop
### Perform the Opening Dev Ritual

Once again, we'll make sure we _**stay informed and content synced,**_ so we never lose more than one day's worth of progress!

Open **GitHub Desktop** and check the **3 main tool buttons** across the top:

1. Is the `Current repository` button **set to our "spacerocks-username" project?**
1. Is the `Current branch` button **set to "main"?**
1. Click the `Fetch origin` button for good luck!
1. _**Click the:**_ `"show"` **button** to navigate to our _local project repo_ via the OS file manager:
   - _**Windows button:**_ `Show in Explorer`
   - _**Mac button:**_ `Show / Reveal in Finder`
   - _**Linux button:**_ `Show in your File Manager`


![Open the project repository with GitHub Desktop.](../.docs/opening_dev_ritual.png)


## ![](../../.docs/folder.svg) File Manager

Our operating system should launch its default _file manager_, opening to the project repository folder.

- _**Open the folder:**_ "Code"
- _**Launch Godot, double-click:**_ "project.godot"


![Double-click: "project.godot"](.docs/project_godot.png)


## ![](../../.docs/godot.svg) The `Main` Scene: "Main.tscn"

Now that we can see some basic `Bubble` behavior in our game, we have **enough pieces to begin adding _gameplay_ to our** `Main` scene. To open the `Main` scene in the `2D` viewport:

- From the `FileSystem` dock, _**double-click:**_ `res://` > `Main` > `Main.tscn`
  - _**Click the viewport button:**_ `2D`
  - Notice that the `Scene` tree updates, showing us **a list of items** within the `2D` viewport.


### Main Brain: "Main.gd"

Let's create a new script for our main gameplay. This will be the core manager of all our `Bubble` instances. **"Main.gd"** script to the `Main` scene's _root node_:

- Over in the `Scene` tree, _**select the root node:**_ `Main`
  - _**Click the button:**_ `Attach Script` _(a script icon with a green plus sign)_
- Within the `Attach Node Script` dialog box:
  - _Leave the auto-filled_ defaults for, _**Path:**_ `res://` `Main/` `Main.gd`
  - _**Click:**_ `Create`

![`Main` node > `Attach Node Script` > Path: "res://Main/Main.gd".](.docs/attach_node_script.png)


### Adding a Countdown: `Timer`

In order to _**spawn**_ `Bubble` _**instances at regular intervals**_, we will make use of Godot's `Timer` node. A `Timer` instance is able to **count down from a time limit that we specify**.


- From the `Scene` tree, _**select the root node:**_ `Main`
  - _**Click the button:**_ `Add Child Node` _(the big_ `+` _button just beneath the_ `Scene` _tab)_
  - From the `Create New Node` popup, _**locate and select:**_ `Timer`
  - _**Click the button:**_ `Create`

![Add Timer node.](.docs/add_timer_node.png)


### Timer Parameters

Now that we have a `Timer` in the scene we should connect it to trigger an action when itwould like to set it up to _**emit a signal**_ every `0.3` seconds. We will also need to enable its **Autostart** parameter so `Timer` will begin counting down as soon as the `Main` scene is run: `▶️`

- From the `Scene` tree, _**select:**_ `Main` > `Timer`
- Over in the `Inspector` panel, _**set its properties:**_
  - _**Wait Time:**_ `0.3` seconds.
  - _**Autostart:**_ `true`

![Set the Timer's Wait time to 0.3. Set the Autostart to true.](.docs/timer_params.png)


### Tap: `F5`

If we launch the game now, we won't see anything new. It will still look blank, but the timer is firing every 0.3 seconds. We just haven't told it to do anything yet. So let's point it to some code **to prove that the Timer is working**.

- To run the project either:
  - _**Click the Play button:**_ `▶`
  -or _**tap the key:**_ `F5`




> ## Test Driven Development
>
> Instead of taking things on faith, developers prefer to make sure that once things are working, that they continue working, as we progress througn development, hooking up classes, scripts, and assets in-engine. Thus, we tend to create **quick little stress tests alongside the workflow** to make sure the code is still behaving the way we intend.
>
> We are looking for visible proof of the invisible timer countdown, so let's try to get some `print()` messages firing in the `Output` window every 0.3 seconds.


## Testing `Timer`


### Signal: `timeout`

Our `Timer` will _**emit a signal**_ called `timeout` once it has **finished its countdown** from the "Wait Time" parameter. In order for us to _**use the**_ `timeout` _signal_, we need to _**add 2 things to our "Main.gd" script first:**_

- _**Declare a function**_ that we want to run when the `Timer` _times out:_ `func OnTimedOut():`
- _**Connect the signal**_ to that new function:  `timer.timeout.connect(OnTimedOut)`


###### _"Main.gd"_

```gdscript
extends Node
class_name Main

@onready var timer = $Timer

func _ready():
    timer.timeout.connect(OnTimedOut)

func OnTimedOut():
    print("Timed out!")
```


### Tap `F5`

- `▶` **play** the game and make sure the `Timer` works!
-  We should see our `print()` text _"Timed out!"_ being spit out to the `Output` window every `0.3` seconds.

![Our timer prints "Timeout triggered!" to the Output window.](.docs/timed_out.png)


## Spawning Multiple Bubbles

Now that we've proven our `Timer` is functional, let's use it to **spawn multiple** `Bubble` **instances into the** `Main` **scene at run-time**. Earlier, we spawned a `Bubble` instance by dragging the `Bubble.tscn` file from the `FileSystem` dock into the `2D` viewport. Now let's **spawn a bubble with code!**

Spawning `Bubble` instances into the `Main` scene is a **4 step process:**


#### 1. Declare a constant.

- We start by _declaring_ a _**constant**_ so we can `preload`   **"Bubble.tscn"** _(a PackedScene file on our hard drive)_ for use in our script:
   - _**Using code:**_ `const BUBBLE: PackedScene = preload("res://Bubble/Bubble.tscn")`
   - _In-editor UI:_ equivalent to selecting the "Bubble.tscn" file in the `FileSystem` dock.
   - _Documentation:_ [PackedScene](https://docs.godotengine.org/en/latest/classes/class_packedscene.html)


#### 2. Declare a variable.

- Next we **_declare_ a variable** to hold a `Bubble` instance in memory:
  - _**Using code:**_ `var bubble_node = BUBBLE.instantiate()`
  - _In-editor UI:_ equivalent to dragging the "Bubble.tscn" file from the `FileSystem` dock, to the `2D` viewport.


#### 3. Position the instance.

- **Position the bubble instance** on the stage:
  - _**Using code:**_ `bubble_node.position.x = 100`
  - _**Using code:**_ `bubble_node.position.y = 200`
  - _In-editor UI:_ equivalent to **hovering "Bubble.tscn"** over the `2D` viewport at a particular position.
  - _Documentation:_ [Node2D](https://docs.godotengine.org/en/latest/classes/class_node2d.html)


#### 4. Add the node.

- And finally, we **attach  the** `Bubble` instance as a `Bubble` node in the scene tree:
  - _**Using code:**_ `add_child(bubble_node)`
  - _In-editor UI:_ equivalent to **dropping "Bubble.tscn"** on to the `2D` viewport stage.
  - _Documentation:_ [Nodes and Scenes](https://docs.godotengine.org/en/latest/tutorials/scripting/nodes_and_scene_instances.html)


###### _"Main.gd"_

```gdscript
extends Node
class_name Main

const BUBBLE: PackedScene = preload("res://Bubble/Bubble.tscn")
@onready var timer = $Timer

func _ready():
    timer.timeout.connect(OnTimedOut)

func OnTimedOut():
    var bubble_node = BUBBLE.instantiate()
    bubble_node.position.x = 100
    bubble_node.position.y = 200
    add_child(bubble_node)
```

![Spawning multiple bubbles based on the timer's countdown signal.](.docs/multiple_bubbles_spawn.png)


- Notice where new bubbles are being spawned: `100` pixels to the right along the **`x` axis**, and `200` pixels downwards along the **`y` axis**.
- We can now manipulate any `Bubble`'s spawning position to anywhere we want in **2D screen space**!
- We'll spawn all `Bubble` instances from the "Main.gd" script. So we should _**delete the original**_ `Bubble` _**instance**_ that we drug to the "Main.tscn" scene in our earlier test.
- The `Bubble` instances seem a little large for our game window. _**We should scale down the sprite graphics**_ so we can spawn more on the screen.


##  ![](../../.docs/godot.svg) The `Bubble` Scene: "Bubble.tscn"

### Scaling Graphics

- From the `Scene` tree, _**select the node:**_ `Sprite`
  - _**Scale the graphics:**_
    - Either, _**drag the little red handles**_ to scale the graphics.
    - Or, _**edit the scale parameter:**_ `Inspector` > `Node2D` > `Scale`

![Scale the `Sprite` node graphics.](.docs/scale_sprite.png)


## Random & Responsive

Spawning bubbles in a straight line isn't that engaging. If we add some **randomness to the gameplay**, we can keep the players guessing. Godot has a host of _**built-in functions**_ that provide us with [random numbers](https://docs.godotengine.org/en/latest/classes/class_randomnumbergenerator.html) in various formats. We will use one of these functions to plot the bubble's instance or `bubble.position.x` to a **randomly generated integer** before we spawn it to our scene. If we also look-up the **size** of our game window, we will be able to **spawn new bubbles according to the viewport dimensions**.


### Built-in Functions

#### `randi_range()`

We can use `randi_range(from_int, to_int)` to get a **random whole number** between **two given integers**.
- **Example code:** `var rando: int = randi_range(0, 100)`
- _**If we read the code like a sentence in English:**_
  - This line of code will create a **variable called "rando"** that only stores **integer values**.
  - It then runs the **random range** function to get a **random number between `0` and `100`**.
  - Then it **stores the random** number into the **"rando"** variable for later use.


#### `get_viewport()`

- **Example code:** `var window_size: Vector2 = get_viewport().size`
- _**If we read the code like sentences in English:**_
  - This line of code will create a **variable called "window_size"** that only stores **Vector2** values _(x, y)_.
  - It then runs the **get viewport** function.
  - Then we ask for the viewport's current `.size` parameter.
  - Then it **stores the Vector2** numbers into the **"window_size"** variable.
  - We can then access the `window_size.x` and `window_size.y` values at a later time.


### The Responsive Game Window

Now we just have to **put all the pieces together** in our script.


###### _"Main.gd"_

```gdscript
extends Node
class_name Main

const BUBBLE: PackedScene = preload("res://Bubble/Bubble.tscn")
@onready var timer = $Timer

func _ready():
    timer.timeout.connect(OnTimedOut)

func OnTimedOut():
    var rando: int = randi_range(0, get_viewport().size.x)
    var bubble_node = BUBBLE.instantiate()
    bubble_node.position.x = rando
    bubble_node.position.y = get_viewport().size.y
    add_child(bubble_node)
```

![We'll use a random integer to spawn bubbles across the bottom of the game screen.](.docs/rando_x_pos.png)


### Tap `F5`

- `▶` **Play** the game and make sure it works!
- We should see **multiple bubbles climb** the screen and disappear off the top.
- We should be able to resize the game window and the **bubbles should still fill the entire screen**.

![Tons o' Suds!](.docs/end.png)


## ![](../../.docs/github.png) Commit & Push!

When satisfied with the results:

- Use **GitHub Desktop** to _**commit**_ and _**push**_ the project changes.
- **Should any issues arise**, please use **Discord** to send a message, and we'll sort it out together!


### Continued in Part 3

[Bubble Pop: 3. Click & Pop()](https://gitlab.com/kirkja-leikjahonnunar/knowhow/-/blob/main/Tinker/Drills/Bubble%20Pop/3.%20Click%20%26%20Pop()/README.md)
