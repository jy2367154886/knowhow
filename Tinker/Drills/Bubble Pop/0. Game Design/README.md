# Bubble Pop: 0. Game Design Document


## Objectives

- Review the **Bubble Pop GDD**.
- Understand the importance of having a plan.


## What are Game Design Documents? _(GDD)_

_**"A good Game Design Document should be a 'living document' that is edited and maintained alongside the rest of the project's production. If a GDD becomes obsolete, it needs to be updated!"** - unblinky_

**Game designers are tasked with editing and maintaining the GDD.** When version controlled, **Game design documents become instructions, plans, and developer log**.


### GDD Prompts:

- What kind of game are we trying to make?
- **How should the player feel** when they experiennce certain points in the game?
- What **input controls** does the game support?
- What is the **target platform**, both hardware and software?
- What are the **steps needed** to complete the game?
- How should we **organize our work**?


### What's on the back of the box?

The game's final description will usually be written last. It's too hard to predict the final form of the media we are generating. Game development shifts and changes course rapidly. A general description is a fine placeholder until the project becomes playable. Yet, another reason it's beneficial to become comfortable with rapid prototyping and iteration techniques.


## Bubble Pop GDD

**The Bubble Pop GDD** should include:
- A list of **all the pieces** needed to play.
- **The rules** for the game, and **how the game mechanics work**.
- A list of possible **endgame conditions**.
- The intended **player experience**.

![Bubble Pop GDD.](.docs/bubblepop_gdd.png)

_(notice that one does not need incredible drawing skills to convey game mechanics. scribbles work just fine. a fair amount of game designers work on the back of napkins, or envelopes.)_


### Pieces of Bubble Pop

- Bubble
- Main
- UI
  - Score
  - Game Menu


### Rules

- `Bubble` _instances_ should spawn along the bottom of the `2D` _game window_.
- Each `Bubble` instance should:
  - Float up into view.
  - Continue rising upward.
  - Despawn once it reaches the top of the screen.
  - **Pop when the player clicks it!**
- Every bubble popped by the player grants a point value and should be added to the Score UI.
- A game menu should handle the pause and quit options.


### Player Experience

- Players are invited to play through a chill experience.
- Meditate, pop bubbles, and vibe.
- Is it fun?

_(Notice how our GDD serves a similar purpose to that of board game instructions. Often video games are played as board games first, before writing any code. It's a plan. It's a map. It's a journal.)_

