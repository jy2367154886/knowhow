# Bubble Pop: 4. Score + More

![The end is the beginning.](.docs/end.png)


## Objectives
- **Build a _user interface_** to hold the player score. Use the `Control` nodes, `PanelContainer`, and `Label` to count the **total number of bubbles the player has popped** during a game session.
- **Add a custom font** to the `Label`, and adjust the position, scale, etc.
- Create a function within "Main.gd" **to increment the score**.
- Allow the player to **quit the game by tapping the `Esc` key.**


## ![](../../.docs/github.png) GitHub Desktop
### Perform the Opening Dev Ritual

Once again, we'll make sure we _**stay informed and content synced,**_ so we never lose more than one day's worth of progress!

Open **GitHub Desktop** and check the **3 main tool buttons** across the top:

1. Is the `Current repository` button **set to our "spacerocks-username" project?**
1. Is the `Current branch` button **set to "main"?**
1. Click the `Fetch origin` button for good luck!
1. _**Click the:**_ `"show"` **button** to navigate to our _local project repo_ via the OS file manager:
   - _**Windows button:**_ `Show in Explorer`
   - _**Mac button:**_ `Show / Reveal in Finder`
   - _**Linux button:**_ `Show in your File Manager`


![Open the project repository with GitHub Desktop.](../.docs/opening_dev_ritual.png)


## ![](../../.docs/folder.svg) File Manager

Our operating system should launch its default _file manager_, opening to the project repository folder.

- _**Open the folder:**_ "Code"
- _**Launch Godot, double-click:**_ "project.godot"


![Double-click: "project.godot"](.docs/project_godot.png)


## ![](../../.docs/godot.svg) The `Main` Scene: "Main.tscn"


### Tap `F5`

- `▶` **Play** the game and _**take note of the current game state**_.
- _**Let's make a quick list of issues**_ we can find, and possible solutions.
  - There is no way for the player to exit the game.
  - We are now able to **pop bubbles** into the game, but we **are not scoring them anywhere.**
  - Let's _**create a user interface**_ to manage the numbers for us.


### Quitting the Game

**Let's allow the player to exit the game gracefully**, at any time, by hitting the `Esc` key on the keyboard. Godot has a _built-in_ function that tracks any unexpected input: `_unhandled_key_input()`

- We will use if statements to narrow down what the key the options to the key that we are looking for. In this case: `KEY_ESCAPE`


###### _"Main.gd"_

```gdscript
func _unhandled_key_input(event):
    if event is InputEventKey:
        if event.keycode == KEY_ESCAPE and event.pressed:
            get_tree().quit()
```


### The Score UI

We'll continue by adding a _user interface (UI)_ that we can anchor to the top-center of the `2D` viewport. Once we anchor our Control nodes to the viewport, the UI will _respond_ to any changes the player makes to the _runtime game window_.

- In the `Scene` tree:
  - _**Select the root node:**_ `Main`
  - _**Add a new child node:**_ `PanelContainer`
  - _**Rename the PanelContainer node to:**_ "UI"

![](../../.docs/create_new_node/panel_container.png)


- In the `2D` viewport:
  - _**Click the Anchors Preset:**_ `Center Top`

![Anchors preset.](.docs/anchors_preset.png)


- In the `Scene` tree:
  - _**Select the PanelContainer:**_ `UI`
  - _**Click the large button:**_ `+`
- In the `Create New Node` dialog:
  - _**Locate and select the node:**_ `Label`
  - _**Click the button:**_ `Create`

![Create New Node `Label` dialog box.](../../.docs/create_new_node/label.png)


- _**Enter the following parameters into:**_ `Inspector` > `Label`
  -  _**Text:**_ "1234567890"
  - _**Label Settings:**_ `New LabelSettings`
    - _**Font:**_ pick a fancy font from the `FileSystem` > `res://` > `Fonts` folder.
    - _**Size:**_ pick a fun text size.
    - _**Color:**_ choose a bubbly font color.
  - _**Horizontal Alignment:**_ `Center`
  - _**Vertical Alignment:**_ `Center`

![`Label` > `Inspector` parameters.](.docs/label_parameters.png)


### Flushing out the UI with some style!

- From the `Scene` tree, _**select the node:**_ `UI`
- Head over to the `Inspector` dock and _**create a new `StyleBox` by selecting the option:**_ `Control` > `Theme Overrides` > `Styles` > `Panel` > `New StyleBoxFlat`
  - Open the _resource parameters_ by _**clicking on the resource preview:**_ `StyleBoxFlat`
  - The following settings are the ones used in the image below:
    - _**BG Color:**_ `5b2899`
    - _**Border Width:**_
      - _**Left:**_ `3`
      - _**Top:**_ `3`
      - _**Right:**_ `3`
      - _**Bottom:**_ `3`
    - _**Border:**_
      - _**Color:**_ `dc84ff`
    - _**Corner Radius:**_
      - _**Top Left:**_ `33`
      - _**Top Right:**_ `33`
      - _**Bottom Right:**_ `33`
      - _**Bottom Left:**_ `33`
    - _**Content Margins:**_
      - _**Left:**_ `33`
      - _**Top:**_ `-1`
      - _**Right:**_ `33`
      - _**Bottom:**_ `10`

![`StyleBoxFlat`](.docs/style_box_flat.png)


### Scripting the Logic

The last thing we need to do is script the functionality of our scoring system. We'll update our score every time the player pops a bubble.


- _**Open the "Main.gd" script**_ in the `Script` viewport.
- From the `Scene` tree:
  - _**Drag the node:**_ `Label` into the `Script` viewport onto a blank line of the "Main.gd" script.
  - _**Drop the node while holding down the key:**_ `Ctrl`
  - Godot should **auto-generate:** `@onready var label = $UI/Label`
- To hold the score values, _**add a variable:**_ `var score: int = 0`

_**The UI will update if we**_ change the _string_ value of `label.text`. However, if we are storing our `score` as an `int`, and our UI `label.text` is a `string`, then _**we must first convert any numbers to letters with the function:**_ `str()`


### Creating the `IncreaseScore()` function.
- _**Increase the score value by one whole number:**_ `score += 1`
- _**Convert our `int` to a `string` with:**_ `str(score)`


###### _"Main.gd"_

```gdscript
@onready var label = $UI/Label
var score: int = 0

func IncreaseScore():
    score += 1
    label.text = str(score)
```


### Updating the `Bubble` scene.


###### _"Bubble.gd"_

```gdscript
var main: Main

func OnClicked(_viewport, event, _shape):
    if event is InputEventMouseButton:
        if event.button_index == MOUSE_BUTTON_LEFT:
            if event.pressed:
                Pop()
                main.IncreaseScore()
                print("Player clicked down.")
            else:
                print("Player clicked up.")
```


- Back in `Main`, _**we need to add the line**_ `bubble_node.main = self` to the `OnTimedOut()` function.


###### _"Main.gd"_

```gdscript
func OnTimedOut():
  var rando: int = randi_range(0, get_viewport().size.x)
  var bubble_node = BUBBLE.instantiate()
  bubble_node.main = self
  bubble_node.audio = audio
  bubble_node.position.x = rando
  bubble_node.position.y = get_viewport().size.y
  add_child(bubble_node)

```

### Tap `F5`

- `▶` **Play** the game and make sure it works!
- We should now have a fully playable prototype.
- The bubbles should be clickable.
- The user interface should keep score.

![Score! But so much more!](.docs/end.png)


## ![](../../.docs/github.png) Commit & Push!

When satisfied with the results:

- Use **GitHub Desktop** to _**commit**_ and _**push**_ the project changes.
- **Should any issues arise**, please use **Discord** to send a message, and we'll sort it out together!


### Congratulations on completing the Bubble Pop drill!

[Reference Code](.docs/code.md)
