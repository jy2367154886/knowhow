###### _"Main.gd" (full code)_

```gdscript
extends Node
class_name Main

const BUBBLE: PackedScene = preload("res://Bubble/Bubble.tscn")
@onready var timer = $Timer
@onready var audio = $Audio
@onready var label = $UI/Label

var score: int = 0

func _ready():
    timer.timeout.connect(OnTimedOut)

func IncreaseScore():
    score += 1
    label.text = str(score)

func OnTimedOut():
    var rando: int = randi_range(0, get_viewport().size.x)
    var bubble_node = BUBBLE.instantiate()
    bubble_node.main = self
    bubble_node.audio = audio
    bubble_node.position.x = rando
    bubble_node.position.y = get_viewport().size.y
    add_child(bubble_node)

func _unhandled_key_input(event):
    if event is InputEventKey:
        if event.keycode == KEY_ESCAPE and event.pressed:
            get_tree().quit()
```

###### _"Bubble.gd" (full code)_

```gdscript
extends Area2D
class_name Bubble

var main: Main
var audio: AudioStreamPlayer2D
var speed: float = 100.0

func _ready():
    input_event.connect(OnClicked)

func _process(delta):
    position.y -= speed * delta
    if position.y < 0:
        Pop()

func Pop():
    audio.position = position
    audio.pitch_scale = randf_range(0.9, 1.1)
    audio.play()
    queue_free()

func OnClicked(_viewport, event, _shape):
    if event is InputEventMouseButton:
        if event.button_index == MOUSE_BUTTON_LEFT:
            if event.pressed:
                Pop()
                main.IncreaseScore()
                print("Player clicked down.")
            else:
                print("Player clicked up.")
```
