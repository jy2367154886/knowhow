## Progression Objectives: K - 8

### Achievements
- Introduced: `I`
- Developing: `D`
- Mastered: `M`


### Laptop Hardware

| Hardware Flow | K | 1 | 2 | 3| 4 | 5 | 6 | 7 | 8 |
|---|---|---|---|---|---|---|---|---|---|
| Open / close the computer lid. | I | D | M | M | M | M | M | M | M |
| Turn on / off computer. | I | D | M | M | M | M | M | M | M |
| Turn on / off monitor. | I | D | M | M | M | M | M | M | M |
| Log on / off using username / password. | I | D | M | M | M | M | M | M | M |
| Identify and use the monitor. | I | D | M | M | M | M | M | M | M |
| Identify and use the trackpad. | I | D | M | M | M | M | M | M | M |
| Identify and use the keyboard. | I | D | M | M | M | M | M | M | M |
| Identify and use the USB ports. | I | D | M | M | M | M | M | M | M |
| Identify and use the mouse. | I | D | M | M | M | M | M | M | M |
| Identify and use the headphone jack. | I | D | M | M | M | M | M | M | M |
| Identify and use the microphone jack. | I | D | M | M | M | M | M | M | M |
| Identify and use the HDMI ports. | I | D | M | M | M | M | M | M | M |


### Operating System (OS)

| Mouse Flow | K | 1 | 2 | 3 | 4 | 5 | 6 | 7 | 8 |
|---|---|---|---|---|---|---|---|---|---|
| Demonstrate correct hand and finger placement. | I | D | D | M | M | M | M | M | M |
| Click. | I | M | M | M | M | M | M | M | M |
| Scroll. | I | M | M | M | M | M | M | M | M |
| Double-click. | I | D | M | M | M | M | M | M | M |
| Drag & drop. | I | D | M | M | M | M | M | M | M |
| Right-click to open context menus. | | I | D | M | M | M | M | M | M |
| Cursor control. | I | D | D | D | M | M | M | M | M |
| Single-click icons attached to the dock, to launch various applications. | I | D | M | M | M | M | M | M | M |
| Navigate drop down menus. | | I | D | M | M | M | M | M | M |


#### File Manager

| Mouse Flow | K | 1 | 2 | 3 | 4 | 5 | 6 | 7 | 8 |
|---|---|---|---|---|---|---|---|---|---|
| Open the file manager from the dock. | I | M | M | M | M | M | M | M | M |
| Locate the student home folder. | I | D | M | M | M | M | M | M | M |
| Open from and save to student folder. | I | D | M | M | M | M | M | M | M |
| Create new folder. | I | D | M | M | M | M | M | M | M |
| Name and rename folders and documents. | I | D | D | M | M | M | M | M | M |
| Double-click to launch an application from a folder or the desktop. | I | D | M | M | M | M | M | M | M |
| Create new document. | | I | D | D | M | M | M | M | M |
| Be able to collaborate with peers on digital projects. | | I | D | D | D | D | M | M | M |

| Keyboard Flow | K | 1 | 2 | 3 | 4 | 5 | 6 | 7 | 8 |
|---|---|---|---|---|---|---|---|---|---|
| Navigate with the `Tab` key. | | | I | D | D | D | M | M | M |
| Navigate with the arrow keys: `🡨` `🡩` `🡪` `🡫` | | | I | D | D | D | M | M | M |
| Open a folder with the `Enter` key.| | | I | D | D | D | M | M | M |


### Text Editor / Word Processor

| Keyboard Flow | K | 1 | 2 | 3 | 4 | 5 | 6 | 7 | 8 |
|---|---|---|---|---|---|---|---|---|---|
| Type full name. | I | D | M | M | M | M | M | M | M |
| Use `Enter` key. | I | D | M | M | M | M | M | M | M |
| Move the typing carrot by single characters with the `🡨` and `🡪` keys. | I | D | M | M | M | M | M | M | M |
| Move the typing carrot by line with the `🡩` and `🡫` keys. | I | D | M | M | M | M | M | M | M |
| Use `Shift` and `Caps Lock` keys. | | I | D | M | M | M | M | M | M |
| Use `Delete` and `Backspace` keys. | | I | D | M | M | M | M | M | M |
| Save with `Ctrl` + `S` keys. | | I | D | M | M | M | M | M | M |
| Undo with `Ctrl` + `Z` keys. | | I | D | M | M | M | M | M | M |
| Redo with `Ctrl` + `Shift` + `Z` keys. | | I | D | M | M | M | M | M | M |
| Demonstrate the correct keyboard Home Row finger placement. |  | I | D | M | M | M | M | M | M |
| Begin typing practice with the Home Row characters. | | I | D | M | M | M | M | M | M |
| Type simple sentences with a capital letter at the beginning. | | I | D | M | M | M | M | M | M |
| Type simple sentences with a `.` period at the end. | | I | D | M | M | M | M | M | M |
| Use `!` exclamation point and `?` question mark. | | I | D | M | M | M | M | M | M |
| Type multiple sentences with space after period. | | | I | D | M | M | M | M | M |
| Highlight text with: `Shift` + `🡪` , or `Shift` + `🡨` | | | I | D | M | M | M | M | M |
| Cut: `Ctrl` + `X` Copy: `Ctrl` + `C` Paste: `Ctrl` + `V` | | | I | D | M | M | M | M | M |


| Mouse Flow | K | 1 | 2 | 3 | 4 | 5 | 6 | 7 | 8 |
|---|---|---|---|---|---|---|---|---|---|
| Click once to place cursor for editing. | | I | D | M | M | M | M | M | M |
| Highlight text to move, delete, or edit. | | I | D | D | M | M | M | M | M |
| Change font, size, color of text: | | I | D | M | M | M | M | M | M |
| Use copy, cut, paste features: | | | I | D | M | M | M | M | M |
| Add graphic or image from file folder location: | | | | I | D | M | M | M | M |
| Create a numbered list and a bulleted list: | | | | | I | D | M | M | M |
| `Save` v. `Save As...` | | | | I | D | M | M | M | M |
| Use spell check: | | | | | | I | M | M | M |
| Independently produce multi-page documents: | | | | | | I | D | M | M |


### Digital Painting & Image Manipulation

| Krita Flow - [krita.org](https://krita.org/) | K | 1 | 2 | 3 | 4 | 5 | 6 | 7 | 8 |
|---|---|---|---|---|---|---|---|---|---|
| Create a new paint document with a width and height. | I | D | D | M | M | M | M | M | M |
| Pan the canvas by dragging with: `MMB` | I | D | D | D | M | M | M | M | M |
| Zoom the canvas by scrolling with: `MMB` | I | D | D | D | M | M | M | M | M |
| Click the menu selection: `File` > `Save` | I | D | M | M | M | M | M | M | M |
| Click the menu selection: `File` > `Export...` > `PNG` | I | D | D | M | M | M | M | M | M |
| Add / delete layers. | I | D | M | M | M | M | M | M | M |
| Select and reorder layers. | | I | D | M | M | M | M | M | M |
| Use the paint tool. | I | D | D | M | M | M | M | M | M |
| Use the color wheel. | I | D | D | M | M | M | M | M | M |
| Use the fill tool. | | I | D | M | M | M | M | M | M |
| Use the selection tool. | | I | D | M | M | M | M | M | M |
| Resize selection or layer. | | | | I | D | M | M | M | M |
| Use the RGBA / HSVA color sliders. | | | | I | D | M | M | M | M |
| Use transparency masks. | | | | I | D | M | M | M | M |
| Insert text / word art. | | | | I | D | M | M | M | M |
| Import images / clip art. | | | I | D | M | M | M | M | M |


| Hotkeys | K | 1 | 2 | 3 | 4 | 5 | 6 | 7 | 8 |
|---|---|---|---|---|---|---|---|---|---|
| Toggle Erase Mode: `E` | I | D | M | M | M | M | M | M | M |
| Increase brush size with square brackets: `]` | I | D | M | M | M | M | M | M | M |
| Decrease brush size with square brackets: `[` | I | D | M | M | M | M | M | M | M |
| Select All: `Ctrl` + `A` | | I | D | M | M | M | M | M | M |
| Deselect All: `Ctrl` + `Shift` + `A` | | I | D | M | M | M | M | M | M |
| Cut: `Ctrl` + `X` Copy: `Ctrl` + `C` Paste: `Ctrl` + `V` | | | I | D | M | M | M | M | M |


### Gaming Engine _(coding & scripting)_

| Godot Flow - [godotengine.org](https://godotengine.org/) | K | 1 | 2 | 3 | 4 | 5 | 6 | 7 | 8 |
|---|---|---|---|---|---|---|---|---|---|
| Viewport navigation, both 2D and 3D. | | I | D | D | D | D | D | M | M |
| Select different objects in the viewport. | | I | D | D | M | M | M | M | M |
| Multiple selections. | | | I | D | M | M | M | M | M |
| Move, rotate, and scale elements in the scene at will. | | I | D | D | M | M | M | M | M |
| Add new object. | | | I | D | M | M | M | M | M |
| Click and type in editor parameter fields. | | | I | D | M | M | M | M | M |
| Create various solid shapes with meshes.  | | | I | D | M | M | M | M | M |
| Plot, drag, and edit points on the stage. | | I | D | M | M | M | M | M | M |
| Drag a texture to the sprite parameter. | | | | I | D | M | M | M | M |
| Insert audio. | | | | I | D | M | M | M | M |
| Change color of graphic shapes.  | | | I | D | D | M | M | M | M |
| Export a game to play.| | | I | D | D | M | M | M | M |

| Hotkeys | K | 1 | 2 | 3 | 4 | 5 | 6 | 7 | 8 |
|---|---|---|---|---|---|---|---|---|---|
| Focus on selection with: `F` | | I | D | D | D | D | D | M | M |
| Orbit origin with: `O` | | I | D | D | D | D | D | M | M |
| Duplicate selection with: `Ctrl` + `D` | | I | D | D | D | D | D | M | M |


### Internet Browser

| Mouse Flow | K | 1 | 2 | 3 | 4 | 5 | 6 | 7 | 8 |
|---|---|---|---|---|---|---|---|---|---|
| Click links on web page as directed. | I | D | M | M | M | M | M | M | M |
| Click once to follow link to website. | I | D | M | M | M | M | M | M | M |
| Search for information using a search engine. | | | | I | D | M | M | M | M |
| Read search results before selecting link. | | | I | D | D | D | M | M | M |
| Navigate between multiple tabs: | | | | I | D | M | M | M | M |
| Use forward and back buttons: | | | | I | D | M | M | M | M |
| Download photo reference. | I | D | D | M | M | M | M | M | M |
| Download fonts. | | I | D | D | D | D | M | M | M |
| Right-click to copy image and paste in document: | | | | | I | D | M | M | M |
| Understand and follow copyright rules and guidelines. | | | I | D | D | D | M | M | M |
| Type simple web address in address text field: | | | I | D | M | M | M | M | M |
| Know how to leave a useful comment for a peer. | | | I | D | D | D | D | D | D |
| Practice good netiquette when commenting. | | | I | D | D | D | D | D | D |
| Know how to bookmark / save sites: | | | | I | D | D | M | M | M |
| Know how to evaluate websites for accuracy and relevance. | | | I | D | D | M | M | M | M |


#### Peripheral Workflows

| Scanner | K | 1 | 2 | 3| 4 | 5 | 6 | 7 | 8 |
|---|---|---|---|---|---|---|---|---|---|
| Plug the scanner in to the computer. | | | I | D | D | M | M | M | M |
| Open / close the lid. | | | I | D | D | D | M | M | M |
| Insert paper with the copy side down. | | | | I | D | D | M | M | M |
| Run the scanner software. | | | | I | D | D | M | M | M |
| Edit the scanning resolution properties for print or pixels. | | | | | I | D | M | M | M |

| Second Monitor | K | 1 | 2 | 3| 4 | 5 | 6 | 7 | 8 |
|---|---|---|---|---|---|---|---|---|---|
| Plug a second monitor into the computer's HDMI port. | | I | D | D | D | M | M | M | M |
| Open and edit the display settings to present a mirrored, or expanded desktop. | | I | D | D | D | M | M | M | M |
