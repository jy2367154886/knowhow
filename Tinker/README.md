# Welcome Tinkers!
The [Drills](./Drills/) folder contains exercises designed to demonstrate how one gets started in game development. We use [open-source apps](../Apps/) that anyone may download and use. 
