# Pop!_OS
Version: **22.04 LTS**
OS family: Linux < Debian < Ubuntu < Pop!_OS
https://www.omgubuntu.co.uk/2022/04/pop-os-22-04-lts-features-download


## Terminal Mighty
- Open Terminal: `Super` + `T`


### UX Command Loop
- The Terminal 2-step:
  1. Type a `command` to execute.
  1. Press `Enter` to run the command.


### Basics
- One may open a Terminal window by pressing the hotkeys: `Super` + `T` _(aka. `Win` + `T`)_
- The Terminal window should open with single command line.
- And a blinking rectangular caret for typing new commands
  (**`▮` `▯` `▮` `▯`**). It should display a line of text with the format: `username@computername:~$ ▮`


> _For example, if the line reads:_
`bilbo@bagend-pc:~$`
- The **user's name** is: `bilbo`
- The **computer name** is: `bagend-pc`
- The **folder location** is: `~` (aka. bilbo's Home folder: `/home/bilbo/` )



### Navigation
#### Directories / Folders
_Folders_ and _directories_ are two terms for the same thing. Our **current folder location** is displayed as a **path** between the `:` and `$` characters of our command line. A path consists of a `folder name` separated by `/` (slash) characters.

A few common paths are:
- `/` the computer's root folder. (an absolute path)
- `./` the current folder. (a relative path)
- `../` the parent folder. (a relative path)
- `~` the user's home folder.


#### Where are we?
We may use the **`pwd`** command to list our **current directory location** as an absolute path.
> `dirk@holistic-pi:~$` **`pwd`**
/home/dirk/

#### Looking Around
We may **list the contents** of the our current folder location with the **`ls`** command.
> `dirk@holistic-pi:~$` **`ls`**

- **List** the contents of our location with:
  - $ `ls`
  - $ `ls -l`
  - $ `ls -al`
  - $ `ll`

#### Moving Around
One may move to a new folder location by **changing directories** using the **`cd`** command, a `space` then the destination path.

A few starters:
- $ `cd ~` change directory to our home folder.
- $ `cd ..` change directory to the parent folder.
- $ `cd /` change directory to the computer's root folder.
- $ `cd /home/username/Big\ Project/"Big Folder"` if the folder name contains spaces:
  - We can use the `"` (double quote) character "around the folder name" so our spaces aren't used as _command delimiters_.
  - Alternatively, one may use the `\` (backslash) _escape character_, followed by the `space`.

> `dirk@holistic-pi:~$` **`cd Documents/Firefox/`**
`dirk@holistic-pi:~/Documents/Firefox$` **`cd ..`**
`dirk@holistic-pi:~/Documents$` **`cd ../Pictures/"Mrs. Tilde's Missing Dog"/`**
`dirk@holistic-pi:~/Pictures/Mrs. Tilde's Missing Dog$` `cd /home/dirk/`


### `ll`
- $ **`ifconfig`**
- $ **`lsusb`**
- $ **`lspci`**

### Symlinks
- $ **`ln -rs source_path link_path_and_name`** # relative symlink.

### Copy & Paste
- Select text then `MMB` to paste.


## Users and Groups
- View all Users: $ `compgen -u`
- View all Groups: $ `compgen -g`
- Add User:
- Add Group: $ `sudo groupadd groupname`
- Add User to Group: $ `sudo usermod -a -G groupname username`
- $ `whoami`: prints the **username**.
- $ `exit`: closes the Terminal window.

>`bilbo@bagend-pc:~$` **`whoami`**
bilbo
- Returns the current Terminal user: `bilbo`







## How to register new mimetypes. (like "project.godot" files)
![Associating Mimetype](.docs/godot_mimetype.png)

1. Find the locations of the app and the icon.
1. Edit the **"godot.desktop"** file to the absolute file path of the app:

##### godot.desktop
```
[Desktop Entry]
Name=Godot
GenericName=Game Engine
Comment=2D & 3D game engine
Keywords=3d;cg;modeling;animation;painting;texturing;rendering;render engine;game engine;cgscript;
Exec=/apps/godot/godot
Icon=/apps/godot/godot.svg
Terminal=false
Type=Application
Categories=Graphics;3DGraphics;
MimeType=application/x-godot-project;application/x-godot;
```
3. Create a **symlink** to "godot.desktop" in **"~/.local/share/applications/godot.desktop"**.
1. Create a plane-text XML file: **"~/.local/share/mime/packages/x-godot.xml"**.
1. Edit the contents of **"x-godot.xml"**:

##### x-godot.xml
```xml
<?xml version="1.0" encoding="UTF-8"?>
<mime-info xmlns="http://www.freedesktop.org/standards/shared-mime-info">
 <mime-type type="application/x-godot">
  <comment>Godot project</comment>
  <glob pattern="project.godot"/>
 </mime-type>
</mime-info>
```

6. Open the Terminal and refresh the OS mime database: $ **`update-mime-database ~/.local/share/mime`**
1. Test it out by **double clicking** a "project.godot" file.

---

# Troubleshooting

## Reinstalling Nvidia Drivers
Success: `2023-03-16`
Sometimes Pop!_OS video drivers start to dissociate and malfunction.
- $ `sudo apt purge ~nnvidia`
- $ `sudo apt autoremove`
- $ `sudo apt clean`
- $ `sudo apt update`
- $ `sudo apt install nvidia-driver-525`
- $ `sudo systemctl reboot`


### If the external monitor stops working.
Miscellaneous older commands:
- What drivers are installed? $ `dpkg -l *nvidia*`
- Info on an app package $ `apt-cache show nvidia-driver-520`
- Install the drivers $ `sudo apt install nvidia-driver-520`
- If Nvidia is running $ `lsmod |grep nvidia`

![lsmod](.docs/lsmod_results.png)



## Bash Scripting
In progress..
There are several scripts available to make installing frequently updated apps easier.

### tinker.sh
Tinker Tools install production ready open source applications, and set their OS .desktop and .xml mimetype files:
- Godot
- Blender
- Krita

### clone.sh
SSH clone from GitLab.

### pull.sh
### push.sh

## Installing Wine
Success: `2023-02`
https://linuxhint.com/install-wine-pop-os/
### Ubuntu Terminal
- $ `lscpu`
- $ `sudo dpkg --add-architecture i386`
- $ `sudo apt update`
- $ `sudo apt install wine32 wine64 -y`
- $ `sudo wine --version`


### Pop!_Shop
Search for **Lutris** then install the `Pop!_OS (deb)` version.

### Terminal
- $ `sudo apt install xdelta3`
- $ `sudo apt install xterm`
- $ `sudo apt install zenity`
