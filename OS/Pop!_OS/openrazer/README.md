# OpenRazer + Polychromatic
Trying to get the Razer Tartarus macro keypad working. With the Tartarusplugged in:

## Install OpenRazer
Parsed instructions from - https://openrazer.github.io/#download

### Ubuntu / Pop!_OS
#### From the PPA via the Terminal


1. To install the recommended stable builds: _**$**_ **`sudo add-apt-repository ppa:openrazer/stable`**

1. After adding the PPA: _**$**_ **`sudo apt update`**

1. Install the packages: _**$**_ **`sudo apt install openrazer-meta`**
> ```text
Reading package lists... Done
Building dependency tree... Done
Reading state information... Done
The following additional packages will be installed:
  libblas3 libgfortran5 liblapack3 openrazer-daemon openrazer-driver-dkms
  python3-daemonize python3-notify2 python3-numpy python3-openrazer
  python3-pyudev python3-setproctitle xautomation
Suggested packages:
  openrazer-doc python-daemonize-doc gfortran python-numpy-doc python3-dev
  python3-pytest
The following NEW packages will be installed:
  libblas3 libgfortran5 liblapack3 openrazer-daemon openrazer-driver-dkms
  openrazer-meta python3-daemonize python3-notify2 python3-numpy
  python3-openrazer python3-pyudev python3-setproctitle xautomation
0 upgraded, 13 newly installed, 0 to remove and 0 not upgraded.
Need to get 7,359 kB of archives.
After this operation, 32.3 MB of additional disk space will be used.
Do you want to continue? [Y/n] y
Get:1 http://apt.pop-os.org/ubuntu jammy/main amd64 libblas3 amd64 3.10.0-2ubuntu1 [228 kB]
Get:2 http://apt.pop-os.org/ubuntu jammy/main amd64 libgfortran5 amd64 12-20220319-1ubuntu1 [883 kB]
Get:3 http://apt.pop-os.org/ubuntu jammy/main amd64 liblapack3 amd64 3.10.0-2ubuntu1 [2,504 kB]
Get:4 http://apt.pop-os.org/ubuntu jammy/main amd64 python3-pyudev all 0.22.0-2 [33.2 kB]           
Get:5 http://apt.pop-os.org/ubuntu jammy/main amd64 python3-setproctitle amd64 1.2.2-2build1 [16.1 kB]  
Get:6 http://apt.pop-os.org/ubuntu jammy/universe amd64 python3-notify2 all 0.3-4 [11.0 kB]                 
Get:7 http://apt.pop-os.org/ubuntu jammy/universe amd64 python3-daemonize all 2.5.0-1 [6,656 B]           
Get:8 http://apt.pop-os.org/ubuntu jammy/universe amd64 xautomation amd64 1.09-4 [39.1 kB]                 
Get:9 http://apt.pop-os.org/ubuntu jammy/main amd64 python3-numpy amd64 1:1.21.5-1build2 [3,464 kB]
Get:10 https://ppa.launchpadcontent.net/openrazer/stable/ubuntu jammy/main amd64 openrazer-driver-dkms all 3.4.0~ubuntu22.04.1 [63.5 kB]
Get:11 https://ppa.launchpadcontent.net/openrazer/stable/ubuntu jammy/main amd64 openrazer-daemon all 3.4.0~ubuntu22.04.1 [67.9 kB]
Get:12 https://ppa.launchpadcontent.net/openrazer/stable/ubuntu jammy/main amd64 python3-openrazer all 3.4.0~ubuntu22.04.1 [27.2 kB]
Get:13 https://ppa.launchpadcontent.net/openrazer/stable/ubuntu jammy/main amd64 openrazer-meta all 3.4.0~ubuntu22.04.1 [14.4 kB]
Fetched 7,359 kB in 5s (1,531 kB/s)                             
Selecting previously unselected package libblas3:amd64.
(Reading database ... 316675 files and directories currently installed.)
Preparing to unpack .../00-libblas3_3.10.0-2ubuntu1_amd64.deb ...
Unpacking libblas3:amd64 (3.10.0-2ubuntu1) ...
Selecting previously unselected package libgfortran5:amd64.
Preparing to unpack .../01-libgfortran5_12-20220319-1ubuntu1_amd64.deb ...
Unpacking libgfortran5:amd64 (12-20220319-1ubuntu1) ...
Selecting previously unselected package liblapack3:amd64.
Preparing to unpack .../02-liblapack3_3.10.0-2ubuntu1_amd64.deb ...
Unpacking liblapack3:amd64 (3.10.0-2ubuntu1) ...
Selecting previously unselected package openrazer-driver-dkms.
Preparing to unpack .../03-openrazer-driver-dkms_3.4.0~ubuntu22.04.1_all.deb ...
Unpacking openrazer-driver-dkms (3.4.0~ubuntu22.04.1) ...
Selecting previously unselected package python3-pyudev.
Preparing to unpack .../04-python3-pyudev_0.22.0-2_all.deb ...
Unpacking python3-pyudev (0.22.0-2) ...
Selecting previously unselected package python3-setproctitle:amd64.
Preparing to unpack .../05-python3-setproctitle_1.2.2-2build1_amd64.deb ...
Unpacking python3-setproctitle:amd64 (1.2.2-2build1) ...
Selecting previously unselected package python3-notify2.
Preparing to unpack .../06-python3-notify2_0.3-4_all.deb ...
Unpacking python3-notify2 (0.3-4) ...
Selecting previously unselected package python3-daemonize.
Preparing to unpack .../07-python3-daemonize_2.5.0-1_all.deb ...
Unpacking python3-daemonize (2.5.0-1) ...
Selecting previously unselected package xautomation.
Preparing to unpack .../08-xautomation_1.09-4_amd64.deb ...
Unpacking xautomation (1.09-4) ...
Selecting previously unselected package openrazer-daemon.
Preparing to unpack .../09-openrazer-daemon_3.4.0~ubuntu22.04.1_all.deb ...
Unpacking openrazer-daemon (3.4.0~ubuntu22.04.1) ...
Selecting previously unselected package python3-numpy.
Preparing to unpack .../10-python3-numpy_1%3a1.21.5-1build2_amd64.deb ...
Unpacking python3-numpy (1:1.21.5-1build2) ...
Selecting previously unselected package python3-openrazer.
Preparing to unpack .../11-python3-openrazer_3.4.0~ubuntu22.04.1_all.deb ...
Unpacking python3-openrazer (3.4.0~ubuntu22.04.1) ...
Selecting previously unselected package openrazer-meta.
Preparing to unpack .../12-openrazer-meta_3.4.0~ubuntu22.04.1_all.deb ...
Unpacking openrazer-meta (3.4.0~ubuntu22.04.1) ...
Setting up libblas3:amd64 (3.10.0-2ubuntu1) ...
update-alternatives: using /usr/lib/x86_64-linux-gnu/blas/libblas.so.3 to provide /usr/lib/x86_64-linux-gnu/l
ibblas.so.3 (libblas.so.3-x86_64-linux-gnu) in auto mode
Setting up python3-setproctitle:amd64 (1.2.2-2build1) ...
Setting up libgfortran5:amd64 (12-20220319-1ubuntu1) ...
Setting up python3-notify2 (0.3-4) ...
Setting up xautomation (1.09-4) ...
Setting up python3-pyudev (0.22.0-2) ...
Setting up openrazer-driver-dkms (3.4.0~ubuntu22.04.1) ...
Loading new openrazer-driver-3.4.0 DKMS files...
Building for 5.19.0-76051900-generic
Building initial module for 5.19.0-76051900-generic
Done.
>
> razerkbd.ko:
Running module version sanity check.
 - Original module
   - No original module exists within this kernel
 - Installation
   - Installing to /lib/modules/5.19.0-76051900-generic/kernel/drivers/hid/
>
> razermouse.ko:
Running module version sanity check.
 - Original module
   - No original module exists within this kernel
 - Installation
   - Installing to /lib/modules/5.19.0-76051900-generic/kernel/drivers/hid/
>
> razerkraken.ko:
Running module version sanity check.
 - Original module
   - No original module exists within this kernel
 - Installation
   - Installing to /lib/modules/5.19.0-76051900-generic/kernel/drivers/hid/
>
> razeraccessory.ko:
Running module version sanity check.
 - Original module
   - No original module exists within this kernel
 - Installation
   - Installing to /lib/modules/5.19.0-76051900-generic/kernel/drivers/hid/
>
> depmod..........
>
> ADD YOUR USER TO THE PLUGDEV GROUP: 'sudo gpasswd -a $USER plugdev'
Setting up python3-daemonize (2.5.0-1) ...
Setting up liblapack3:amd64 (3.10.0-2ubuntu1) ...
update-alternatives: using /usr/lib/x86_64-linux-gnu/lapack/liblapack.so.3 to provide /usr/lib/x86_64-linux-g
nu/liblapack.so.3 (liblapack.so.3-x86_64-linux-gnu) in auto mode
Setting up openrazer-daemon (3.4.0~ubuntu22.04.1) ...
Setting up python3-numpy (1:1.21.5-1build2) ...
Setting up python3-openrazer (3.4.0~ubuntu22.04.1) ...
Setting up openrazer-meta (3.4.0~ubuntu22.04.1) ...
Processing triggers for man-db (2.10.2-1) ...
Processing triggers for libc-bin (2.35-0ubuntu3.1) ...```

1. Add user to the group: _**$**_ **`sudo gpasswd -a $USER plugdev`**
> Adding user unblinky to group plugdev

1. Restart. REQUIRED!



After the restart one should be able to type into a text editor with the Tartarus                                          Macro Pad.

## Install Polychromatic
Parsed from - https://polychromatic.app/download/ubuntu/

> For the tried & tested experience.
>
> - _**$**_ **`sudo add-apt-repository ppa:polychromatic/stable`**
> - _**$**_ **`sudo apt update`**
>
> Full installation
> - _**$**_ **`sudo apt install polychromatic`**

### Log Dump
```
$ sudo apt install polychromatic
Reading package lists... Done
Building dependency tree... Done
Reading state information... Done
The following additional packages will be installed:
  gir1.2-ayatanaappindicator3-0.1 libevent-2.1-7 libminizip1 libqt5designer5 libqt5help5 libqt5positioning5
  libqt5printsupport5 libqt5qml5 libqt5qmlmodels5 libqt5quick5 libqt5quickwidgets5 libqt5sql5
  libqt5sql5-sqlite libqt5svg5 libqt5test5 libqt5webchannel5 libqt5webengine-data libqt5webengine5
  libqt5webenginecore5 libqt5webenginewidgets5 libqt5xml5 libre2-9 polychromatic-cli polychromatic-common
  polychromatic-controller polychromatic-tray-applet python3-colour python3-pyqt5 python3-pyqt5.qtsvg
  python3-pyqt5.qtwebchannel python3-pyqt5.qtwebengine python3-pyqt5.sip
Suggested packages:
  qt5-qmltooling-plugins
The following NEW packages will be installed:
  gir1.2-ayatanaappindicator3-0.1 libevent-2.1-7 libminizip1 libqt5designer5 libqt5help5 libqt5positioning5
  libqt5printsupport5 libqt5qml5 libqt5qmlmodels5 libqt5quick5 libqt5quickwidgets5 libqt5sql5
  libqt5sql5-sqlite libqt5svg5 libqt5test5 libqt5webchannel5 libqt5webengine-data libqt5webengine5
  libqt5webenginecore5 libqt5webenginewidgets5 libqt5xml5 libre2-9 polychromatic polychromatic-cli
  polychromatic-common polychromatic-controller polychromatic-tray-applet python3-colour python3-pyqt5
  python3-pyqt5.qtsvg python3-pyqt5.qtwebchannel python3-pyqt5.qtwebengine python3-pyqt5.sip
0 upgraded, 33 newly installed, 0 to remove and 0 not upgraded.
Need to get 62.2 MB/62.4 MB of archives.
After this operation, 205 MB of additional disk space will be used.
Do you want to continue? [Y/n] y
Get:1 http://apt.pop-os.org/ubuntu jammy/main amd64 gir1.2-ayatanaappindicator3-0.1 amd64 0.5.90-7ubuntu2 [5,822 B]
Get:2 http://apt.pop-os.org/ubuntu jammy/main amd64 libevent-2.1-7 amd64 2.1.12-stable-1build3 [148 kB]                 
Get:3 http://apt.pop-os.org/ubuntu jammy/universe amd64 libminizip1 amd64 1.1-8build1 [20.2 kB]      
Get:4 http://apt.pop-os.org/ubuntu jammy-updates/universe amd64 libqt5xml5 amd64 5.15.3+dfsg-2ubuntu0.1 [124 kB]
Get:5 http://apt.pop-os.org/ubuntu jammy/universe amd64 libqt5designer5 amd64 5.15.3-1 [2,832 kB]
Get:6 http://apt.pop-os.org/ubuntu jammy-updates/universe amd64 libqt5sql5 amd64 5.15.3+dfsg-2ubuntu0.1 [123 kB]
Get:7 http://apt.pop-os.org/ubuntu jammy/universe amd64 libqt5help5 amd64 5.15.3-1 [162 kB]      
Get:8 http://apt.pop-os.org/ubuntu jammy/universe amd64 libqt5positioning5 amd64 5.15.3+dfsg-3 [223 kB]
Get:9 http://apt.pop-os.org/ubuntu jammy-updates/universe amd64 libqt5printsupport5 amd64 5.15.3+dfsg-2ubuntu0.1 [215 kB]
Get:10 http://apt.pop-os.org/ubuntu jammy/universe amd64 libqt5qml5 amd64 5.15.3+dfsg-1 [1,472 kB]         
Get:11 http://apt.pop-os.org/ubuntu jammy/universe amd64 libqt5qmlmodels5 amd64 5.15.3+dfsg-1 [205 kB]
Get:12 http://apt.pop-os.org/ubuntu jammy/universe amd64 libqt5quick5 amd64 5.15.3+dfsg-1 [1,748 kB]    
Get:13 http://apt.pop-os.org/ubuntu jammy/universe amd64 libqt5quickwidgets5 amd64 5.15.3+dfsg-1 [39.9 kB]
Get:14 http://apt.pop-os.org/ubuntu jammy-updates/universe amd64 libqt5sql5-sqlite amd64 5.15.3+dfsg-2ubuntu0.1 [53.0 kB]
Get:15 http://apt.pop-os.org/ubuntu jammy-updates/universe amd64 libqt5test5 amd64 5.15.3+dfsg-2ubuntu0.1 [152 kB]
Get:16 http://apt.pop-os.org/ubuntu jammy/universe amd64 libqt5webchannel5 amd64 5.15.3-1 [62.9 kB]
Get:17 http://apt.pop-os.org/ubuntu jammy/universe amd64 libqt5webengine-data all 5.15.9+dfsg-1 [8,571 kB]
Get:18 https://ppa.launchpadcontent.net/polychromatic/stable/ubuntu jammy/main amd64 polychromatic-common all 0.7.3 [294 kB]
Get:19 http://apt.pop-os.org/ubuntu jammy/main amd64 libre2-9 amd64 20220201+dfsg-1 [160 kB]                  
Get:20 http://apt.pop-os.org/ubuntu jammy/universe amd64 libqt5webenginecore5 amd64 5.15.9+dfsg-1 [41.9 MB]
Get:21 https://ppa.launchpadcontent.net/polychromatic/stable/ubuntu jammy/main amd64 polychromatic-controller all 0.7.3 [237 kB]
Get:22 http://apt.pop-os.org/ubuntu jammy/universe amd64 libqt5webengine5 amd64 5.15.9+dfsg-1 [176 kB]        
Get:23 http://apt.pop-os.org/ubuntu jammy/universe amd64 libqt5webenginewidgets5 amd64 5.15.9+dfsg-1 [125 kB]
Get:24 http://apt.pop-os.org/ubuntu jammy/universe amd64 python3-colour all 0.1.5-2 [22.3 kB]      
Get:25 http://apt.pop-os.org/ubuntu jammy/universe amd64 python3-pyqt5.sip amd64 12.9.1-1build1 [61.1 kB]
Get:26 http://apt.pop-os.org/ubuntu jammy/universe amd64 python3-pyqt5 amd64 5.15.6+dfsg-1ubuntu3 [2,822 kB]
Get:27 http://apt.pop-os.org/ubuntu jammy/universe amd64 python3-pyqt5.qtsvg amd64 5.15.6+dfsg-1ubuntu3 [31.4 kB]
Get:28 http://apt.pop-os.org/ubuntu jammy/universe amd64 python3-pyqt5.qtwebchannel amd64 5.15.6+dfsg-1ubuntu3 [15.3 kB]
Get:29 http://apt.pop-os.org/ubuntu jammy/universe amd64 python3-pyqt5.qtwebengine amd64 5.15.5-1 [122 kB]
Get:30 https://ppa.launchpadcontent.net/polychromatic/stable/ubuntu jammy/main amd64 polychromatic-tray-applet all 0.7.3 [22.3 kB]
Get:31 https://ppa.launchpadcontent.net/polychromatic/stable/ubuntu jammy/main amd64 polychromatic-cli all 0.7.3 [20.6 kB]          
Get:32 https://ppa.launchpadcontent.net/polychromatic/stable/ubuntu jammy/main amd64 polychromatic all 0.7.3 [13.4 kB]              
Fetched 62.2 MB in 8s (8,099 kB/s)                                                                                                  
Extracting templates from packages: 100%
Selecting previously unselected package libqt5svg5:amd64.
(Reading database ... 317664 files and directories currently installed.)
Preparing to unpack .../00-libqt5svg5_5.15.3-1_amd64.deb ...
Unpacking libqt5svg5:amd64 (5.15.3-1) ...
Selecting previously unselected package gir1.2-ayatanaappindicator3-0.1.
Preparing to unpack .../01-gir1.2-ayatanaappindicator3-0.1_0.5.90-7ubuntu2_amd64.deb ...
Unpacking gir1.2-ayatanaappindicator3-0.1 (0.5.90-7ubuntu2) ...
Selecting previously unselected package libevent-2.1-7:amd64.
Preparing to unpack .../02-libevent-2.1-7_2.1.12-stable-1build3_amd64.deb ...
Unpacking libevent-2.1-7:amd64 (2.1.12-stable-1build3) ...
Selecting previously unselected package libminizip1:amd64.
Preparing to unpack .../03-libminizip1_1.1-8build1_amd64.deb ...
Unpacking libminizip1:amd64 (1.1-8build1) ...
Selecting previously unselected package libqt5xml5:amd64.
Preparing to unpack .../04-libqt5xml5_5.15.3+dfsg-2ubuntu0.1_amd64.deb ...
Unpacking libqt5xml5:amd64 (5.15.3+dfsg-2ubuntu0.1) ...
Selecting previously unselected package libqt5designer5:amd64.
Preparing to unpack .../05-libqt5designer5_5.15.3-1_amd64.deb ...
Unpacking libqt5designer5:amd64 (5.15.3-1) ...
Selecting previously unselected package libqt5sql5:amd64.
Preparing to unpack .../06-libqt5sql5_5.15.3+dfsg-2ubuntu0.1_amd64.deb ...
Unpacking libqt5sql5:amd64 (5.15.3+dfsg-2ubuntu0.1) ...
Selecting previously unselected package libqt5help5:amd64.
Preparing to unpack .../07-libqt5help5_5.15.3-1_amd64.deb ...
Unpacking libqt5help5:amd64 (5.15.3-1) ...
Selecting previously unselected package libqt5positioning5:amd64.
Preparing to unpack .../08-libqt5positioning5_5.15.3+dfsg-3_amd64.deb ...
Unpacking libqt5positioning5:amd64 (5.15.3+dfsg-3) ...
Selecting previously unselected package libqt5printsupport5:amd64.
Preparing to unpack .../09-libqt5printsupport5_5.15.3+dfsg-2ubuntu0.1_amd64.deb ...
Unpacking libqt5printsupport5:amd64 (5.15.3+dfsg-2ubuntu0.1) ...
Selecting previously unselected package libqt5qml5:amd64.
Preparing to unpack .../10-libqt5qml5_5.15.3+dfsg-1_amd64.deb ...
Unpacking libqt5qml5:amd64 (5.15.3+dfsg-1) ...
Selecting previously unselected package libqt5qmlmodels5:amd64.
Preparing to unpack .../11-libqt5qmlmodels5_5.15.3+dfsg-1_amd64.deb ...
Unpacking libqt5qmlmodels5:amd64 (5.15.3+dfsg-1) ...
Selecting previously unselected package libqt5quick5:amd64.
Preparing to unpack .../12-libqt5quick5_5.15.3+dfsg-1_amd64.deb ...
Unpacking libqt5quick5:amd64 (5.15.3+dfsg-1) ...
Selecting previously unselected package libqt5quickwidgets5:amd64.
Preparing to unpack .../13-libqt5quickwidgets5_5.15.3+dfsg-1_amd64.deb ...
Unpacking libqt5quickwidgets5:amd64 (5.15.3+dfsg-1) ...
Selecting previously unselected package libqt5sql5-sqlite:amd64.
Preparing to unpack .../14-libqt5sql5-sqlite_5.15.3+dfsg-2ubuntu0.1_amd64.deb ...
Unpacking libqt5sql5-sqlite:amd64 (5.15.3+dfsg-2ubuntu0.1) ...
Selecting previously unselected package libqt5test5:amd64.
Preparing to unpack .../15-libqt5test5_5.15.3+dfsg-2ubuntu0.1_amd64.deb ...
Unpacking libqt5test5:amd64 (5.15.3+dfsg-2ubuntu0.1) ...
Selecting previously unselected package libqt5webchannel5:amd64.
Preparing to unpack .../16-libqt5webchannel5_5.15.3-1_amd64.deb ...
Unpacking libqt5webchannel5:amd64 (5.15.3-1) ...
Selecting previously unselected package libqt5webengine-data.
Preparing to unpack .../17-libqt5webengine-data_5.15.9+dfsg-1_all.deb ...
Unpacking libqt5webengine-data (5.15.9+dfsg-1) ...
Selecting previously unselected package libre2-9:amd64.
Preparing to unpack .../18-libre2-9_20220201+dfsg-1_amd64.deb ...
Unpacking libre2-9:amd64 (20220201+dfsg-1) ...
Selecting previously unselected package libqt5webenginecore5:amd64.
Preparing to unpack .../19-libqt5webenginecore5_5.15.9+dfsg-1_amd64.deb ...
Unpacking libqt5webenginecore5:amd64 (5.15.9+dfsg-1) ...
Selecting previously unselected package libqt5webengine5:amd64.
Preparing to unpack .../20-libqt5webengine5_5.15.9+dfsg-1_amd64.deb ...
Unpacking libqt5webengine5:amd64 (5.15.9+dfsg-1) ...
Selecting previously unselected package libqt5webenginewidgets5:amd64.
Preparing to unpack .../21-libqt5webenginewidgets5_5.15.9+dfsg-1_amd64.deb ...
Unpacking libqt5webenginewidgets5:amd64 (5.15.9+dfsg-1) ...
Selecting previously unselected package python3-colour.
Preparing to unpack .../22-python3-colour_0.1.5-2_all.deb ...
Unpacking python3-colour (0.1.5-2) ...
Selecting previously unselected package polychromatic-common.
Preparing to unpack .../23-polychromatic-common_0.7.3_all.deb ...
Unpacking polychromatic-common (0.7.3) ...
Selecting previously unselected package python3-pyqt5.sip.
Preparing to unpack .../24-python3-pyqt5.sip_12.9.1-1build1_amd64.deb ...
Unpacking python3-pyqt5.sip (12.9.1-1build1) ...
Selecting previously unselected package python3-pyqt5.
Preparing to unpack .../25-python3-pyqt5_5.15.6+dfsg-1ubuntu3_amd64.deb ...
Unpacking python3-pyqt5 (5.15.6+dfsg-1ubuntu3) ...
Selecting previously unselected package python3-pyqt5.qtsvg.
Preparing to unpack .../26-python3-pyqt5.qtsvg_5.15.6+dfsg-1ubuntu3_amd64.deb ...
Unpacking python3-pyqt5.qtsvg (5.15.6+dfsg-1ubuntu3) ...
Selecting previously unselected package python3-pyqt5.qtwebchannel.
Preparing to unpack .../27-python3-pyqt5.qtwebchannel_5.15.6+dfsg-1ubuntu3_amd64.deb ...
Unpacking python3-pyqt5.qtwebchannel (5.15.6+dfsg-1ubuntu3) ...
Selecting previously unselected package python3-pyqt5.qtwebengine.
Preparing to unpack .../28-python3-pyqt5.qtwebengine_5.15.5-1_amd64.deb ...
Unpacking python3-pyqt5.qtwebengine (5.15.5-1) ...
Selecting previously unselected package polychromatic-controller.
Preparing to unpack .../29-polychromatic-controller_0.7.3_all.deb ...
Unpacking polychromatic-controller (0.7.3) ...
Selecting previously unselected package polychromatic-tray-applet.
Preparing to unpack .../30-polychromatic-tray-applet_0.7.3_all.deb ...
Unpacking polychromatic-tray-applet (0.7.3) ...
Selecting previously unselected package polychromatic-cli.
Preparing to unpack .../31-polychromatic-cli_0.7.3_all.deb ...
Unpacking polychromatic-cli (0.7.3) ...
Selecting previously unselected package polychromatic.
Preparing to unpack .../32-polychromatic_0.7.3_all.deb ...
Unpacking polychromatic (0.7.3) ...
Setting up libre2-9:amd64 (20220201+dfsg-1) ...
Setting up libqt5webengine-data (5.15.9+dfsg-1) ...
Setting up libminizip1:amd64 (1.1-8build1) ...

Setting up libqt5positioning5:amd64 (5.15.3+dfsg-3) ...
Setting up libqt5svg5:amd64 (5.15.3-1) ...
Setting up libqt5sql5:amd64 (5.15.3+dfsg-2ubuntu0.1) ...
Setting up libqt5printsupport5:amd64 (5.15.3+dfsg-2ubuntu0.1) ...
Setting up libqt5xml5:amd64 (5.15.3+dfsg-2ubuntu0.1) ...
Setting up libqt5qml5:amd64 (5.15.3+dfsg-1) ...
Setting up libevent-2.1-7:amd64 (2.1.12-stable-1build3) ...
Setting up libqt5webchannel5:amd64 (5.15.3-1) ...
Setting up python3-colour (0.1.5-2) ...
Setting up python3-pyqt5.sip (12.9.1-1build1) ...
Setting up libqt5test5:amd64 (5.15.3+dfsg-2ubuntu0.1) ...
Setting up gir1.2-ayatanaappindicator3-0.1 (0.5.90-7ubuntu2) ...
Setting up polychromatic-common (0.7.3) ...
Setting up libqt5qmlmodels5:amd64 (5.15.3+dfsg-1) ...
Setting up libqt5sql5-sqlite:amd64 (5.15.3+dfsg-2ubuntu0.1) ...
Setting up libqt5designer5:amd64 (5.15.3-1) ...
Setting up polychromatic-tray-applet (0.7.3) ...
Setting up polychromatic-cli (0.7.3) ...
Setting up libqt5quick5:amd64 (5.15.3+dfsg-1) ...
Setting up python3-pyqt5 (5.15.6+dfsg-1ubuntu3) ...
Setting up python3-pyqt5.qtsvg (5.15.6+dfsg-1ubuntu3) ...
Setting up libqt5quickwidgets5:amd64 (5.15.3+dfsg-1) ...
Setting up python3-pyqt5.qtwebchannel (5.15.6+dfsg-1ubuntu3) ...
Setting up libqt5webenginecore5:amd64 (5.15.9+dfsg-1) ...
Setting up libqt5webengine5:amd64 (5.15.9+dfsg-1) ...
Setting up libqt5webenginewidgets5:amd64 (5.15.9+dfsg-1) ...
Setting up python3-pyqt5.qtwebengine (5.15.5-1) ...
Setting up polychromatic-controller (0.7.3) ...
Setting up polychromatic (0.7.3) ...
Processing triggers for man-db (2.10.2-1) ...
Processing triggers for desktop-file-utils (0.26-1ubuntu3) ...
Processing triggers for hicolor-icon-theme (0.17-2) ...
Processing triggers for gnome-menus (3.36.0-1ubuntu3) ...
Processing triggers for libc-bin (2.35-0ubuntu3.1) ...
```
Trooubleshooting: it didn't work without errors on the first try.

Restarted to no effect, but unplugging the USB wire then back in again reset the buttons in text editor working.
