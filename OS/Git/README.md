# Terminal & Git

## Make an SSH key pair: (private and public)
Using SSH keys helps ease the git user workflow. Users do not have to remember passwords once the SSH keys are setup on the user device and GitLab account. The following steps generate a

1. _**Create an SSH key pair:**_ $ `ssh-keygen -t ed25519 -C "tinker"`
  - _**Use default SSH path "~/.ssh":**_ `Enter`
  - _**Override previous keys:**_ `y`, `Enter`
  - _**Leave the passphrase blank:**_ `Enter`
  - _**Re-enter the blank passphrase:**_ `Enter`
1. _**Copy the public key to the paste buffer: $**_ `xclip -sel clip < .ssh/id_ed25519.pub`

![SSH Terminal Example](.docs/ssh_example.png)


3. _**Assign the public key with a GitLab account:**_
  - _**Log in to:**_ https://gitlab.com/
  - _**Click on the:**_ `User Account Icon`
  - _**From the drop-down menu, select:**_ `Edit Profile`
  - _**From the left-side user panel, select:**_ `SHH Keys`
  - Paste the public key into the large `Key` input field.
  - Delete the default `Expiration date`.
  - _**Press the button:**_ `Add Key`

### Testing
- _**Test the keys: $**_ `ssh -T git@gitlab.com`


## Basic Git Comands
Open the Terminal in the folder to Git:
$ **`git fetch`**
$ **`git pull`**
$ **`git commit -m "Commit message."`**
$ **`git push`**


## Local Repo -> GitLab Repo
### GitLab Projects
GitLab projects contain just 1 repository, issues, members, etc. We created a project for each of **our 6 main repos** under the group:

#### [`Kirkja Leikjahönnunar`](https://gitlab.com/kirkja-leikjahonnunar/)

- https://gitlab.com/kirkja-leikjahonnunar/kirkja
- https://gitlab.com/kirkja-leikjahonnunar/kirkja-2d
- https://gitlab.com/kirkja-leikjahonnunar/kirkja-3d
- https://gitlab.com/kirkja-leikjahonnunar/kirkja-assets
- https://gitlab.com/kirkja-leikjahonnunar/kirkja-audio
- https://gitlab.com/kirkja-leikjahonnunar/kirkja-tools


### Update the Origin
Open Terminal _(using "kirkja-2d" as example)_
- `cd ~/kirkja/repos/kirkja-2d`


- `git lfs install`
- `git lfs track "*.kra"`
- `git lfs track "*.png"`
- `git lfs track "*.jpg"`


- `git remote rename origin old-origin`
- `git remote add origin https://gitlab.com/kirkja-leikjahonnunar/kirkja-2d.git`
- `git push -u origin --all`
  - Username for 'https://gitlab.com': `username`
  - Password for 'https://username@gitlab.com': `▯▯▯▯▯▯`
  - Gitlab has us verify our username and password multiple times.
  > Locking support detected on remote "origin". Consider enabling it with:
  $ `git config lfs.https://gitlab.com/kirkja-leikjahonnunar/kirkja-assets.git/info/lfs.locksverify true`
  >
  > Branch 'main' set up to track remote branch 'main' from 'origin'.


- `git push -u origin --tags`
  - Username for 'https://gitlab.com': `username`
  - Password for 'https://username@gitlab.com': `▯▯▯▯▯▯`
  - Gitlab has us verify our username and password multiple times.
  > Everything up-to-date






  Login as the super user "bob".
  `$ su bob`
