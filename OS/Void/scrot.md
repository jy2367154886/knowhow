# scrot _(SCReenshOT)_


## Keyboard Combos

**No cursor:**
  - `PrtSc`: snap a shot of the entire screen.
    - **$:** `scrot -F 'Pictures/scrot/%Y-%m-%d-%H%M%S.png'`
  - `Alt` + `PrtSc`: snap a shot of the current window.
    - **$:** `scrot -ub -F 'Pictures/scrot/%Y-%m-%d_%H%M%S.png'`
  - `Super` + `PrtSc`: drag a selection before snapping the screenshot.
    - **$:** `scrot -f -s -F 'Pictures/scrot/%Y-%m-%d_%H%M%S.png'`

**Show cursor:**
  - `Ctrl` + `PrtSc`: snap a shot of the entire screen with the mouse cursor rendered.
  - `Ctrl` + `Alt` + `PrtSc`: snap a shot of the current window.



### Snap a screenshot of the entire screen.

- _**Press the key:**_ `PrtSc`
  - **Terminal command:**_ $ `scrot ~/Pictures/scrot/foo.png`


### Snap a screenshot of the entire screen.

- _**Snap a screenshot of the currently focused window:**_ `Alt` + `PrtSc`
  - **Terminal command:**_ $ `scrot -ub -F 'Pictures/scrot/%Y-%m-%d_%H-%M-%S.png'`

   -f, --freeze
      Freeze the screen when -s is used.

- Drag a selection