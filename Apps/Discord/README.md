# Discord Markdown Tips

A simple **Markdown formatting legend** with examples that work in Discord chat windows. Discord has **limited support** for Markdown. The examples contained below should cover the most of the options.

## Typing MD within Discord
Typing within Discord chat windows can be a pain when first getting started. Wether one is posting to a Discord channel, or one is sending a personal _Direct Message_ _(DM)_ is relativley straigh forward.


- Interface basics: [Discord Interface Walk-through]()

### New Lines
B
- Only press `Enter` when the message is ready to send.
- _**Use:**_ `Shift` + `Enter` to start a **new line in the same message**.

### Send Button
If one desires a "Send" button, to  There is a send button that one may enable:
- `User Settings` > `APP SETTINGS` > `Accessibility` > `CHAT INPUT` > `Show Send Message button`: `🗹`


- bool = true: **`🗹`**
- bool = false: **`🞎`**

---


# Headings

Discord **supports 3 levels of heading**. Headings are useful when trying to organize sections of text underneath a helpful title.

- _**Create title headings using:**_
  - `# Heading 1`
  - `## Heading 2`
  - `### Heading 3`


---


## Headings Example:

# Next Game GDD: `#`

A legendary game of niche appeal, with a small following of dedicated players that make life worth living! Remember you will

## Gameplay: `##`

What are the _design pillars_ for our game? Who is it for? What _game mechanics_ make the game fun?

### Controls: `###`
- _**Player controls:**_ `🠉`, `🠋`, `🠈`, `🠊` _(arrow keys)_
- _**Jump:**_ `Space`

## Look and Feel: `##`
What is the mood of the game?

### Visual Art: `###`
Robust color palette to draw the players eye.

### Audio Art: `###`
- **Music** should bring the emotional connection to the in-game world.
- **Sound effects** should be unique, yet familiar. The dialogue and _voice over_ should compliment the settings and characters.


---


# Inline Formatting
Markdown supports `<span>` style inline formatting. The character used


---


### `_italic_`
- We use _italic formatting_ for new _vocabulary_, dev industry _jargon_, and trailing _(Note: meta comments.)_

```md
### `_italic_`
- We use _italic formatting_ for new _vocabulary_, dev industry _jargon_, and trailing _(Note: meta comments.)_
```


---


### `**bold**`
- We use **bold formatting** to drawing the reader's attention to the **important parts** so they can **skim the docs quickly**.

```md
### `**bold**`
- We use **bold formatting** to drawing the reader's attention to the **important parts** so they can **skim the docs quickly**.
```


---


### `_**italic + bold**_`
- We use _**italic + bold formatting**_ when the two previous formats overlap.

```md
### `_**italic + bold**_`
- We use _**italic + bold formatting**_ when the two previous formats overlap.
```

---


### `~~Strikethru~~`
- We use ~~strikethru formatting~~ when we want to preserve a previous statement or idea, but is no longer relevant. _(One should probably leave a note as to why it's important to keep, instead of just deleting it.)_

```md
### `~~Strikethru~~`
- We use ~~strikethru formatting~~ when we want to preserve a previous statement or idea, but is no longer relevant. _(One should probably leave a note as to why it's important to keep, instead of just deleting it.)_
```

---


## Blockquotes

We are able to quote source material with the `>` + `Space` characters.

**PAGE ONE**
> :sun_with_face: Far out in the uncharted backwaters of the unfashionable end of the western spiral arm of the Galaxy lies a small unregarded yellow sun.
>
> :earth_asia: Orbiting this at a distance of roughly ninety-two million miles is an utterly insignificant little blue green planet whose ape-descended *life forms are so amazingly primitive that they still think digital watches are a pretty neat idea*.
>
> :money_with_wings: This planet has - or rather had - a problem, which was this: most of the people on it were unhappy for pretty much of the time. Many solutions were suggested for this problem, but most of these were largely concerned with the movements of *small green pieces of paper*, which is odd because ***on the whole it wasn't the small green pieces of paper that were unhappy***.
>
> :watch: And so the problem remained; lots of the people were mean, and most of them were miserable, even the ones with digital watches.
>
> :evergreen_tree: Many were increasingly of the opinion that they'd all made a big mistake in coming down from the trees in the first place. And some said that even the trees had been a bad move, and that no one should ever have left the oceans.
>
> :coffee: And then, one Thursday, nearly two thousand years after one man had been nailed to a tree for saying how great it would be to be nice to people for a change, one girl sitting on her own in a small café in Rickmansworth suddenly realized what it was that had been going wrong all this time, and she finally knew how the world could be made a good and happy place. This time it was right, it would work, and no one would have to get nailed to anything.
>
> :sob: Sadly, however, before she could get to a phone to tell anyone-about it, a terribly stupid catastrophe occurred, and the idea was lost forever.
>
> :no_entry_sign: This is not her story.
>
> :whale: But it is the story of that terrible stupid catastrophe and some of its consequences.



---
---
---
---

### Headings

# Heading 1
## Heading 2
### Heading 3


### Text Formatting

#### `_italic_` or `*italic*`

- We use *italic* formatting for new *vocabulary*, dev industry *jargon*, and trailing *(Note: GitHub.com seems to also supports the single asterisk formatting.)*


#### `**bold**`

- pass


#### `_**italic + bold**_` or `***italic + bold***`

- pass


---


#### `~~strikethru~~`

- We use ~~strikethru formatting~~ when we want to preserve a previous statement, but is no longer relevant.


#### `__underline__`

- We use underlines Not sure why the __underline__ isn't supported by git host websites.


---


#### `Inline Code`

- Surround the desired `inline code` with single backticks: `Shift` + `~`
- _**Use inline code when documenting:**_
  - Keyboard Keys: `W`, `S`, `A`, `D`.
  - Mouse Buttons: `LMB`, `RMB`, `MMB`, `MMW`.
    - In-app UI:
      - UI Buttons: `Continue`, `Quit`.
      - Menu Navigation: `File` > `Preferences` > `Fullscreen`
  - And of course for: `code`, `variables`, `functions`, and `classes`.


---


#### `|| Spoiler ||`

- Surround the desired || spoilers || between `||` _(double pipes)_. The spoilers will be hidden under a solid box, but are revealed if the user clicks the box.


---


#### `[Website Link](url address)`

- Here is a website link to the [Official Kirkja Website](https://kirkja.org/)

```md
#### `[Website Link](url address)`
- Here is a website link to the [Official Kirkja Website](https://kirkja.org/)
```


### Manual Lists

Making the lists one desires may require manual work in Discord:

1. `•` | `‣` | `▪` : Copy a bullet character to use in our list.
1. Paste it to the front of each new line item.
1. Press `Shift` + `Enter` to start a new line item.





---
---
---



## GitHub Syntax

![GitHub Logo Tooltip](https://github.githubassets.com/favicons/favicon-dark.png)

[Official Markdown Documentation](https://docs.github.com/en/get-started/writing-on-github/getting-started-with-writing-and-formatting-on-github/basic-writing-and-formatting-syntax)


### Headings

- We use headings to describe the hierarchy or structure of the subject. Headings on GitHub.com range from biggest `#` to smallest `######`.


# Heading 1
## Heading 2
### Heading 3
#### Heading 4
##### Heading 5
###### Heading 6


### Text Formatting


#### `_italic_`

- We use _italic formatting_ for new _vocabulary_, dev industry _jargon_, and trailing _(Note: meta comments.)_


#### `**bold**`

- We use **bold formatting** when drawing the user's attention to the **important parts** so the user can **skim the documents quickly**.


#### `_**italic + bold**_`

- We use _**italic + bold formatting**_ when the two previous formats overlap.


#### `Inline Code`

- We use `inline code` with single backticks surrounding the word to designate key, or mouse buttons, and in-editor menu and button navigation. And for `code` `variables` and `classes`.


#### `~~Strikethru~~`

- We use ~~strikethru formatting~~ when we want to preserve a previous statement or idea, but is no longer relevant. _(Should probably leave a note as to why it's important to keep.)_


### Images

Are there any ways to make this image smaller? Not with pure Markdown.
![Kirkja Logo](http://kirkja.org/favicon.png)


### URL Links

Use `[link_name](url_address)` to add web links.
- [Kirkja Official Website](https://kirkja.org/)




### Lists

#### Unordered Lists

We are able to create bulleted lists by adding `-` + `Space` before each line item.
- bool = true: **`🗹`**
- bool = false: **`🞎`**
- We use quotes around paths ""


#### Ordered lists

Add `1.` + `Space` before each new line.

1. exist.
1. behave true.
1. make mistakes.
1. atone + grow.

or we may skip ahead by starting the list with a number of our choice.

38. life
1. the
1. universe
1. and
1. everything


#### Indented Lists

We can indent our list for different lines using 4 spaces + the before using the `-` character, then another space.

- Indent level: 0
    - Indent level: 1
        - Indent level: 2
        - Indent level: 2
    - Indent level: 1
        - Indent level: 2
            - Indent level: 3
                    # Code can be indented using 8 spaces beyond the current indent level.
                    extends Area2D
                    class_name Bubble
                - Indent level: 4
            - Indent level: 3


#### Task Lists

Task lists are more useful within GitHub.com Issues and Projects. The Github.com web UI tracks the tasks as a part of their project management
Add `- [ ]` before each new line.
- [x] exist.
- [x] behave true.
- [x] make mistakes.
- [ ] atone + grow.


#### Code Block

```gdscript
var dog_legs: int = 4

func _ready():
  print("Our dog has ", dog_legs, " legs.")
```


### Tables

#### Table Syntax

- Use vertical pipes **`|`** to separate table columns.
- The first row of text is displayed as table headers.
- The second row contains the column data justification syntax `---`.
- Rows 3+ are all table data cells.


| Occasion | Unicode Glyphs | ASCII Style |
| :-:| :-:  | --: |
| Justify Left: `:--` | Justify Center: `:-:` | Justify Right: `--:` |
| Alphanumeric Keys | `W` `S` `A` `D` | [W] [S] [A] [D] |
| Modifier Keys | `Shift` `Ctrl` `Alt` | [Shift] [Ctrl] [Alt] |
| Arrow Keys | `🠉` `🠋` `🠈` `🠊` | [Up] [Down] [Left] [Right] |


#### Tested emojis and glyphs.

| Occasion | Glyph / Emoji | Reason |
| --- | --- | --- |
| UI Instructions | Within Atom, click `Edit` > `Bookmark` > `View All`. |
| Boolean UI | **`🞎`** **`🗹`** | [false] [true] |
| There is no real way... | ... to merge cells together. | Það er rist eða ekkert. |


### Hard Rule

---
