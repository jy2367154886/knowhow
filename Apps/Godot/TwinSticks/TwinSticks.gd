extends Node
class_name TwinSticks

# 2D + 3D Graphics Nodes.
@onready var graphics = $Widget/Graphics
@onready var dood = $Dood
@onready var dood_body = $Dood/DoodBody

# UI Nodes.
@onready var target_line_r = $Widget/TargetLineR
@onready var thumb_vector_r = $Widget/ThumbVectorR
@onready var target_line_l = $Widget/TargetLineL
@onready var thumb_vector_l = $Widget/ThumbVectorL
@onready var look_see_label = $Widget/ThumbVectorR/LookSeeLabel
@onready var look_deg_label = $Widget/ThumbVectorR/LookDegLabel
@onready var move_see_label = $Widget/ThumbVectorL/MoveSeeLabel
@onready var move_deg_label = $Widget/ThumbVectorL/MoveDegLabel
@onready var current_angle_label = $Widget/CurrentAngleLabel
@onready var distance_label = $Widget/DistanceLabel

# Not really speed: 180 is pretty fast, 1000 is slow.
@export_range(180, 1000) var turning_time: float = 300
var move_multiplier: float = 0.5

var current_angle: float
var target_angle: float
var turn_interval: float


static func NearestAngle(angle_rad_a: float, angle_rad_b: float):
	var distance: float = fmod(angle_rad_b - angle_rad_a, 2 * PI) # 360°
	if distance < -PI:  # -180°
		distance += 2 * PI  # 360°
	elif distance > PI: # 180°
		distance -= 2 * PI  # 360°
	return distance


func _process(delta):
	# Looking Stick.
	# The resulting deadzone will have a circular shape.
	var look_see: Vector2 = Input.get_vector("twin_look_left", "twin_look_right", "twin_look_forward", "twin_look_back")
	if look_see != Vector2.ZERO:
		thumb_vector_r.show()
		target_line_r.show()
		target_angle = look_see.angle()
		target_line_r.rotation = target_angle
		thumb_vector_r.position = look_see * 100
		look_see_label.text = "x: [" + str(look_see.x).pad_decimals(1) + "]\ny: [" + str(look_see.y).pad_decimals(1) + "]"
		look_deg_label.text = str(rad_to_deg(look_see.angle())).pad_decimals(2) + "°"
		
		# Turn speed interval logic.
		var radians_to_target: float = NearestAngle(current_angle, target_angle)
		# TODO: Make this more curvy!
		turn_interval = radians_to_target / deg_to_rad(turning_time)
		current_angle += turn_interval

		# Unwind as we go over the bounds.
		if current_angle < -PI:
			current_angle += 2 * PI
		elif current_angle > PI:
			current_angle -= 2 * PI
	else:
		thumb_vector_r.hide()
		target_line_r.hide()
	
	
	# Movement Stick.
	# The resulting deadzone will have a circular shape.
	var move_see: Vector2 = Input.get_vector("twin_move_left", "twin_move_right", "twin_move_forward", "twin_move_back")
	if move_see != Vector2.ZERO:
		thumb_vector_l.show()
		target_line_l.show()
		thumb_vector_l.position = move_see * 100
		target_line_l.rotation = move_see.angle()
		var sticks_angle_distance: float = NearestAngle(target_line_l.rotation, target_line_r.rotation)
		move_see_label.text = "x: [" + str(move_see.x).pad_decimals(1) + "]\ny: [" + str(move_see.y).pad_decimals(1) + "]"
		move_deg_label.text = str(rad_to_deg(move_see.angle())).pad_decimals(2) + "°"
		
		if look_see != Vector2.ZERO:
			distance_label.show()
			distance_label.text = "Distance to blue line: " + str(rad_to_deg(sticks_angle_distance)).pad_decimals(2) + "°"
	else:
		thumb_vector_l.hide()
		target_line_l.hide()
		distance_label.hide()
	
	current_angle_label.text = "Player angle: " + str(rad_to_deg(current_angle)).pad_decimals(4) + "°"
	graphics.rotation = current_angle
	dood_body.rotation.y = -current_angle
	dood.position.x += move_see.x * move_multiplier
	dood.position.z += move_see.y * move_multiplier
