# The Cheatsheet Project

Trying to make a simple GDScript document with the most common code patterns. The Code folder contains a Godot project that holds a working example of the following code:


###### _"Example.gd"_

```gdscript
var hello: String = "goodbye."
```
