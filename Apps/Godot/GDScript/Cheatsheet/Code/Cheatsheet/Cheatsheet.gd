# "Cheatsheet.gd": A quick reminder of GDScrit patterns & syntax.
# Use tabs to indent! Spaces and tabs don't mix in Godot.

# Inherit from the super class.
extends Node
class_name Cheatsheet

# Scene classes to instance.
const CAT: PackedScene = preload("res://Cat/Cat.tscn")

# References to existing Scene nodes with the `$` character.
# Try dragging the `Timer` node from the `Scene` tab into the
# `Script` view, then hold `Ctrl` when releasing the button.
@onready var pixel_space := $PixelSpace
@onready var vertex_space := $VertexSpace
@onready var timer: Timer = $Timer


# Variables are containers that hold different values.
var true_or_false: bool = true
var whole_number: int = 2023
var decimal_number: float = 3.45
var string_of_characters: String = "make mistakes\n" + "atone + grow"
var pixel: Vector2i = Vector2i(100, 200)
var pixels: Array = [pixel, Vector2i(30, 70)]
var vertex: Vector3 = Vector3(105.7, 34.0, 333.679)
var vertices: PackedVector3Array


# Built-in functions are provided by the game engine.
func _ready():
	print(string_of_characters, "\nThis only happens one time, just before the first frame draws.")
	timer.timeout.connect(OnTimesUp)
	decimal_number = PI
	
	# Array manipulation.
	vertices.append(vertex)
	vertices.append(Vector3(sin(PI), cos(PI), tan(PI)))
	vertices.append(vertex)
	vertices.remove_at(2)
	
	# Spawn instance from PackedScene.
	var cat = CAT.instantiate()
	cat.position = get_viewport().size / 2
	pixel_space.add_child(cat)


func _process(delta):
#	print("This will happen every frame.So comment out print() ASAP!")
#	print("The delta: ", delta, ".\nis the amount of time in seconds passed, since the last frame was drawn to screen.")
	pass


# Class functions are made by us!
func MyAwesomeFunction(column_x: int, row_y: float, username: String):
	var combined: String = "Ég heiti: " + username + str(column_x) + str(row_y) + "."
	return combined


func OnTimesUp():
	print("yo")




# The following text will introduce you to all the features of this text editor.

# This line is a comment. Comments start with the `#` character.
# Comments are colored gray in the `Script` viewport,
# and are neither recognized, nor executed, by the game engine.
# They are for human eyes only!
# HACK: Defeat AI hordes through comments!


# Comments don't have to take up the whole line either;
# they may be placed after a line of code.
#extends Node # This is a line of code.

# The `extends` keyword let's us steal all the powers of
# the listed class that follows it.
# In this case, all  the powers of `Node`! (which basically means it exists)


#------------------------------------------------------------------------------
# 
#------------------------------------------------------------------------------

# There are only 4 main concepts one needs to understand before becoming a
# functional progammer:
# 	- Varables.
# 	- Functions.
# 	- Conditions.
# 	- Loops.


#------------------------------
# Navigating with the keyboard.
#------------------------------
# Move the Carrot with the arrow keys on the keyboard.
#
#Use the up and down arrows.
# `Shift` + `Left`, or `Shift` + `Right` to grab individual letters.
# `Shift` + 
# Use the `Ctrl` + `K` keys to toggle komments over multiple lines at a time.


#------------------------------------------------------------------------------
# Variables:
# Variables are containers of computer memory.
# We can use the `var` keyword to ask the computer for
# some memory to use in our game.
#------------------------------------------------------------------------------
#
#var my_name
#

# See these line numbers off to the left?
# Tried clicking one yet? It should select the entire line; plus the `newline`.

# <---------------------- Press the `LMB` down here, then drag down...

# Don't forget that we will be overriding about 90% of our work as we itterate
# through our game dev workflow.





# <---------------------- ... and release here.

# Declare member variables here.
# Sometimes 
# var a = 2
# var b = "text"

#
## Called when the node enters the scene tree for the first time.
#func _ready():
#	pass # Replace with function body.


# Called every frame. 'delta' is the elapsed time since the previous frame.
#func _process(delta):
#	pass


#------------------------------------------------------------------------------
# Some programmers like to title their functions with dashes or ascii art,
# but those ghosts are just plane ecto.
#------------------------------------------------------------------------------
