# GDScript Basics
## How to Program with Grace and Style


### The Fundamental Four
- Variables
- Functions
- Conditions
- Loops


### Variables with: `var`
Variables are the nouns of the coding world. We use the _keyword_  `var` to ask the computer for memory.

```gdscript
# Explicit declaration.
# Best pattern.
var my_float: float = 5.6789

# Implicit declaration.
var my_container
my_container = "Bilbo"
my_container = 89

my_float = 89 # This works. It just converts the int 89 to a float 89.0
my_float = "Bilbo" # Throws an error: not a String. We declared as a float above.
```


### Functions with: `func`
Functions _(aka. Methods)_ are the verbs of our coding _syntax_.


### Conditions with: `if:`, `elif:`, `else:`
Conditions are the main way logic flow.... brain overworked....


### Loops with: `for:`
Loops are important.... for some reason.... infinite loop shutting down....
