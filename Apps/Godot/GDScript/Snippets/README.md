# GDScript Recipes


## Lerping
```gdscript
# Basic lerp()
var value_a: float = 50.0
var value_b: float = 100.0

var result = lerp(value_a, value_b, 0.5) # 75.0

# Helpful
var point_a: Vector2 = Vector2(100, 100)
var point_b: Vector2 = Vector2(200, 200)
var weight: float = 0.0

func _process(delta):
  weight += 0.1 * delta
  point_a.lerp(point_b, weight)
```


### Accessing a Node's Theme Constants

```gdscript
$GridContainer.get_theme_constant("h_separation") = 8
$Label.add_theme_font_size_override("font_size", size.x * 0.4)
```


### Reparenting

```gdscript
func _ready():
    call_deferred("Reparent", new_parent)

func Reparent(node):
    var gt = global_transform
    get_parent().remove_child(self)
    node.add_child(self)
    global_transform = gt
```

### Deleting All Children of a Node

```gdscript
static func remove_children_of(node):
    for n in node.get_children():
    node.remove_child(n)
    n.queue_free()
```

### Where to Save?

Saving persistent data to file is highly dependent on the task at hand. There are some general

#### User Data
```gdscript
func save(content):
var file = FileAccess.open("user://save_game.dat", FileAccess.WRITE)
file.store_string(content)

func load():
var file = FileAccess.open("user://save_game.dat", FileAccess.READ)
var content = file.get_as_text()
return content
```

### Time
```gdscript
func _ready():
var date = Time.get_date_from_unix_time(????)
```







## Godot Game Tree
When we press `▶️` the play button Godot opens a new window and plays the Scene indicated by:
- `Project` > `Project Settings...` > Run



#### Common 3D Scene Tree Nodes
- Node3D (has a 3D position, rotation, and scale)
- Area (has the ability to be run into)
- CollisionShape (the hitbox)
- MeshInstance (3D graphics)
- Timer (run code at time intervals)


- OOD verbs (actions, behaviors); code that “does” something.
- _ready():
- _process(delta):


- OOD nouns (properties, parameters); things in and code that “are” something.
- bool (boolean): true, or false.
- int (integer): -12
- float (floating point number): 12.34
- String: "A string of characters."
- Vector3 ()



- Intro to gdscript:
- CaPiTaLiSaTiOn: Yes it matters!
- Indenting Code Blocks.
- Don’t mix whitespace characters; use ‘Tab’.
- Nesting blocks.
- White space characters: Indenting with [Tab], and [Shift] + [Tab].
- ‘#’ Commenting Code: [Ctrl] + [K].


- from Godot Editor: Drag scenes from `FileSystem` to the viewport.
- from code:
1. Preload scene: `const BUNNY = preload("res://Bunny/Bunny.tscn")`
2. Instance scene: `var new_bunny = BUNNY.instance()`
3. Add instance to Game Tree: `add_child(new_bunny)`
