# Godot
We love Godot! We are always excited when a new version comes out!

- Go grab it at the [Official Godot Website](https://godotengine.org) and start playing!

![](https://godotengine.org/assets/error_logo.svg)

## Important Project Settings
- `Project` > `Project Settings...` > **`General`**
  - `Application` > _**`Config`**_ > _**Name:**_ `Rename Project`
  - `Application` > _**`Run`**_ > _**Main Scene:**_ `Starting Scene`
  - `Display` > `Window` > **`Size`**
    - _**Viewport Width:**_ `1152`
    - _**Viewport Height:**_ `648`
    - _**Viewport Mode:**_ `Windowed`
    - _**Resizable:**_ `true`
    - _**Borderless:**_ `false`
  - `Rendering` > `Textures` > **`Canvas Textures`**
    - _**Default Texture Filter:**_ `Nearest`
    - _**Default Texture Repeat:**_ `Disable`
- `Project` > `Project Settings...` > **`Input Map`**
  - _**Add New Action:**_ `jump_action` > `Enter`
  - _**Action:**_ > `jump_action` > `+` > `Event Configuration` > **`Listening for input...`**
    - Tap the desired input key to bind to the jump_action: `Space`
    - Continue using the `+` _(Add Event)_ button to support multiple options: `Middle Mouse Button`
- Project: > `Project Settings...` > **`Autoload`**
  - Use the `Folder` button to select a _**singleton script:**_
    - _**Name:**_ `Persistent`
    - _**Path:**_ `res://Main/Persistent.gd`
    - _**Global Variable:**_ `true`


## Script Organizing Standards
In an attempt to keep things organized amongst collaborators: **please try to follow these code block patterns** within individual scripts. _(so everyone will know where to look)_

1. `extends` - inherit from super class.
1. `class_name` - name of our class.
1. `const` - preload game resource files.
1. `@export var` - expose member variables for use in-editor.
1. `@onready var` - references to existing nodes in scene tree.
1. `var` - our class member variables.
1. `func _ready():` - built-in functions provided by Godot.
 - `signal.connect(OnSignaled)` - our signal hooks, and callback declaration. _(declared without parentheses)_
1. `func DoSomething():` - our class functions.
1. `func OnSignaled():` - our signal callbacks. _(past tense)_

_Example_

```gdscript
extends Node
class_name Village

const HOUSE: PackedScene = preload("res://House/House.tscn")
@onready var timer := $VillageClock/Timer

# Member variables may be set before `_ready()` executes.
var my_boolean: bool = true
var my_integer: int = 123
var my_float: float = -4.5678
var my_string: String = "I am but a simple earthling; on a complex journey."
var my_point: Vector2i = Vector2i(7, 6)
var my_plot: Vector3

func _ready():
	timer.timeout.connect(OnHourStruck)

func _process(delta):
	if Input.is_action_just_pressed("spawn_it"):
		SpawnHouse(get_viewport().get_mouse_position().y)

func SpawnHouse(row_offset: float):
	my_plot = Vector3(my_point.x, my_point.y + row_offset, 54.32)
	var house = HOUSE.instantiate()
	house.livingroom.couch.color = Color.HOT_PINK
	house.position = my_plot
	self.add_child(house)
	print(my_string)

func OnHourStruck():
	var time: Dictionary = Time.get_datetime_dict_from_system()
	my_string += ".. who is still awake at: " + time.hour + " o'clock; spawning houses in my village!"
```
