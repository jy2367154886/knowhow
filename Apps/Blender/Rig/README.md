# 3D Rigging Overview
Bungling the notes.


## Questions
- What are our control standards?
- Naming conventions?
- Visual communication?


## Prepare the Mesh
- Scale the mesh to game scale before applying the transforms.
- After prepping our mesh we'll want to make sure all transforms are applied.
- Apply Modifiers: `Mirror`, `SubSurf`, etc.

## Construct the Skeleton

### Adding an Armature

#### `Object Mode`
- Add an Armature to the scene with: **`Shift` + `A` > `Armature`**.
- An Armature object is created with 1 starting bone.

![Add an `Armature` to the scene.](.docs/add_armature.png)


### Helpful Armature Views
- **`Object Data Properties` tab > `Viewport Display` >**
  - Display As: **`Envelope`**.
  - Show: **`Names`**.
  - Show: **`In Front`**.

![Skeleton complete.](.docs/envelope_radius.png)


#### `Edit Mode`
- Switch to the Front Orthographic view: **`View` > `Viewpoint` > `Front`, or `Numpad 1`**.
- Extrude bones from the tail of the selected bone with: **`E` + `LMB` click**.

![Extruding bones.](.docs/extruding_bones.png)


- Duplicate the selected done with: **`Shift` + `D`**.

![Duplicating bones.](.docs/duplicate_bone.png)


### Naming Bones
Since a bone is always a part of an Armature data we'll use snake casing.


#### Deformation Bones
- **"def_name_00"**
- **"def_name_00.r"**
- **"def_name_00.l"**


#### Target Bones
- **"tgt_name_00.l"**


#### Control Bones
- **"ctl_name_00.l"**


### Left and Right Symmetry
- Build out one side of the character.
- Rename the bones with the **suffix**:
  - Left side bones: **"bone_00.l"**.
  - Right side bones: **"bone_00.r"**.

![Symmetrize the right bone hierarchy.](.docs/symmetrize.png)


## Parenting the Mesh to the Armature
The bones that deform the skin mesh.

#### `3D Viewport` > `Object Mode`

1. **`LMB`** select the **`Mesh Prepped` first.**
1.  Select **`Armature` second** _(with `Shift` + `LMB` select)_.
1. **`Ctrl` + `P` > `Set Parent To` > `Armature Deform` > `With Automatic Weights`.**


![](.docs/armature_deform.png)



## Target Bones
Target bones are a part of the control rig?
- Detach but keep parent.
- Select target bone then deformation bone.
- Assign the Constraint: **`Ctrl` + `Shift` + `C` > `Copy Transforms`**.

![Copy Transforms](.docs/copy_transforms_constraint.png)


## Control Bones
Bones used to control the rig.
For now these are the Target Bones.

Edit Mode parenting vs. Pose Mode parenting

Parenting bones must be done in `Edit Mode`?
