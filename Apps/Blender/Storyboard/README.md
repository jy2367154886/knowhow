# Storyboard
There is a "Storyboard.blend" template file that is already setup to draw.

## Drawing with Grease Pencil Strokes

### Add Stroke
**`Object Mode`**
1. Viewport: `Shift` + `A`
1. Add: `Grease Pencil` > `Stroke`

![Add > Grease Pencil > Stroke](.docs/add_stroke.png)

## Drawing
**`Draw Mode`**
- Draw: `LMB` drag
- Erase: `Ctrl` + `LMB` drag
