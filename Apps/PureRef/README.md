# PureRef
- [Official Download Site](https://www.pureref.com/download.php)
- Store any asset reference images into a PrueRef file within the asset's `"docs"` folder.

**PurRef** is an awesome reference image layout application that streamlines the image gathering and sorting process for digital artists of any kind. The app may be used for free, but please consider supporting the developers of this wonderful tool!

## Collecting Reference
Reference can be any type of media that communicates the **next steps the asset needs to proceed**:

- Text descriptions.
- Image search.
- Concept art.
- Photos.
- Maquettes.
- Drawings on napkins.
- Wireframe drawings.
- Lego bricks.
- A cut-up paper bag with stickers.
- Any; or all of the above!
